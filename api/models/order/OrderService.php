<?php

namespace app\models\order;

use api\common\models\Order;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class OrderService extends Object
{

    public static function getOrder($tenant_id, $order_id)
    {
        $order_data = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenant_id, $order_id]);
        $order_data = unserialize($order_data);

        if ($order_data) {
            if (self::isOrderProcessedThroughTheExternalExchange($order_data)) {
                $worker = [
                    'phone'          => ArrayHelper::getValue($order_data, 'exchange_info.order.worker.phone', ''),
                    'tenant_company' => [
                        'name'  => '',
                        'phone' => '',
                    ],
                ];
                $car = [
                    'name'       => ArrayHelper::getValue($order_data, 'exchange_info.order.car.name', ''),
                    'color'      => ArrayHelper::getValue($order_data, 'exchange_info.order.car.color', ''),
                    'gos_number' => ArrayHelper::getValue($order_data, 'exchange_info.order.car.gos_number', ''),
                    'brand'      => ArrayHelper::getValue($order_data, 'exchange_info.order.car.name', ''),
                    'model'      => '',
                ];
            } else {
                $worker = [
                    'phone'          => ArrayHelper::getValue($order_data, 'worker.phone', ''),
                    'tenant_company' => [
                        'name'  => ArrayHelper::getValue($order_data, 'worker.tenant_company.name', ''),
                        'phone' => ArrayHelper::getValue($order_data, 'worker.tenant_company.phone', ''),
                    ],
                ];

                $car = [
                    'name'       => ArrayHelper::getValue($order_data, 'car.name', ''),
                    'color'      => ArrayHelper::getValue($order_data, 'car.color', ''),
                    'gos_number' => ArrayHelper::getValue($order_data, 'car.gos_number', ''),
                    'brand'      => ArrayHelper::getValue($order_data, 'car.brand', ''),
                    'model'      => ArrayHelper::getValue($order_data, 'car.model', ''),
                ];
            }
            $result = [
                'order_id'       => (int)ArrayHelper::getValue($order_data, 'order_id'),
                'tenant_id'      => (int)ArrayHelper::getValue($order_data, 'tenant_id'),
                'city_id'        => (int)ArrayHelper::getValue($order_data, 'city_id'),
                'address'        => (string)ArrayHelper::getValue($order_data, 'address'),
                'time_to_client' => (int)ArrayHelper::getValue($order_data, 'time_to_client'),
                'currency_id'    => (int)ArrayHelper::getValue($order_data, 'currency_id'),
                'order_time'     => (int)ArrayHelper::getValue($order_data, 'order_time'),
                'position_id'    => (int)ArrayHelper::getValue($order_data, 'position_id'),
                'client'         => [
                    'lang'  => ArrayHelper::getValue($order_data, 'client.lang', 'en'),
                    'phone' => ArrayHelper::getValue($order_data, 'phone'),
                ],
                'passenger' => [
                    'lang'  => ArrayHelper::getValue($order_data, 'clientPassenger.lang', 'en'),
                    'phone' => ArrayHelper::getValue($order_data, 'client_passenger_phone'),
                ],
                'tariff'         => [
                    'class_id' => !empty($order_data['tariff']['class_id']) ? (int)$order_data['tariff']['class_id'] : '',
                ],
                'worker'         => $worker,
                'car'            => $car
            ];
        } else {
            $order_data = Order::find()
                ->where([
                    'order_id'  => $order_id,
                    'tenant_id' => $tenant_id,
                ])
                ->with('client', 'tariff', 'car', 'car.color', 'worker', 'tenantCompany')
                ->asArray()
                ->one();

            $result = [
                'order_id'       => (int)ArrayHelper::getValue($order_data, 'order_id'),
                'tenant_id'      => (int)ArrayHelper::getValue($order_data, 'tenant_id'),
                'city_id'        => (int)ArrayHelper::getValue($order_data, 'city_id'),
                'address'        => (string)ArrayHelper::getValue($order_data, 'address'),
                'time_to_client' => (int)ArrayHelper::getValue($order_data, 'time_to_client'),
                'currency_id'    => (int)ArrayHelper::getValue($order_data, 'currency_id'),
                'order_time'     => (int)ArrayHelper::getValue($order_data, 'order_time'),
                'position_id'    => (int)ArrayHelper::getValue($order_data, 'position_id'),
                'client'         => [
                    'lang' => (string)ArrayHelper::getValue($order_data, 'client.lang', 'en'),
                    'phone' => (string)ArrayHelper::getValue($order_data, 'phone'),
                ],
                'passenger'      => [
                    'lang' => (string)ArrayHelper::getValue($order_data, 'client.lang', 'en'),
                    'phone' => (string)ArrayHelper::getValue($order_data, 'client_passenger_phone'),
                ],
                'tariff'         => [
                    'class_id' => (int)ArrayHelper::getValue($order_data, 'tariff.class_id', 'en'),
                ],
                'worker'         => [
                    'phone'          => (int)ArrayHelper::getValue($order_data, 'worker.phone', ''),
                    'tenant_company' => [
                        'name'  => ArrayHelper::getValue($order_data, 'tenantCompany.name', ''),
                        'phone' => ArrayHelper::getValue($order_data, 'tenantCompany.phone', ''),
                    ],
                ],
                'car'            => [
                    'name'       => ArrayHelper::getValue($order_data, 'car.name', ''),
                    'color'      => ArrayHelper::getValue($order_data, 'car.color.name', ''),
                    'gos_number' => ArrayHelper::getValue($order_data, 'car.gos_number', ''),
                    'brand'      => ArrayHelper::getValue($order_data, 'car.brand', ''),
                    'model'      => ArrayHelper::getValue($order_data, 'car.model', ''),
                ],
            ];
        }

        if ($order_data) {
            return $result;
        }

        return null;
    }

    /**
     * Is order processed through the external exchange
     * @param $activeOrder
     * @return bool
     */
    protected static function isOrderProcessedThroughTheExternalExchange($activeOrder)
    {
        return ArrayHelper::getValue($activeOrder, 'exchange_info.order', false) ? true : false;
    }
}