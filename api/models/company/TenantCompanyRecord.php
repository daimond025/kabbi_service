<?php

namespace app\models\company;

use yii\db\ActiveRecord;

/**
 * Class TenantCompanyRecord
 * @package app\models\company
 *
 * @property integer $tenant_company_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $phone
 * @property integer $block
 * @property string  $logo
 * @property string $use_logo_company
 */
class TenantCompanyRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tenant_company}}';
    }
}