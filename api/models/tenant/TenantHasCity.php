<?php

namespace app\models\tenant;


use yii\db\ActiveRecord;

/**
 * Class Worker
 * @package app\models\tenant
 *
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $sort
 * @property integer $block
 */
class TenantHasCity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}
