<?php

namespace app\models\transport\car;

use api\common\models\CarColor;
use Yii;

class Car extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(CarColor::className(), ['color_id' => 'color']);
    }
}
