<?php
namespace app\models\worker;

/**
 * This is the model class for table "{{%call}}".
 *
 * @property integer   $id
 * @property integer   $worker_id
 * @property string    $note
 * @property integer   $created_at
 */
class WorkerShiftNote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_shift_note}}';
    }

    public function beforeSave($insert)
    {
        $this->created_at = time();
        return parent::beforeSave($insert);
    }
}