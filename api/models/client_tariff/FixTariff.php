<?php
namespace app\models\client_tariff;

use yii\db\ActiveRecord;

/**
 * Class FixTariff
 * @package app\models\client_tariff
 *
 * @property int $fix_id
 * @property int $from
 * @property int $to
 */
class FixTariff extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%fix_tariff}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOption()
    {
        return $this->hasOne(FixHasOption::className(), ['option_id' => 'option_id']);
    }
}
