<?php

namespace app\models\client_tariff;

use yii\db\ActiveRecord;

/**
 * Class TaxiTariffOption
 * @package app\models\client_tariff
 *
 * @property int $tariff_id
 * @property int $option_id
 */
class TaxiTariffOption extends ActiveRecord
{

    const TYPE_CURRENT = 'CURRENT';
    const TYPE_HOLIDAYS = 'HOLIDAYS';
    const TYPE_EXCEPTIONS = 'EXCEPTIONS';

    const ACCRUAL_FIX = 'FIX';
    const ACCRUAL_TIME = 'TIME';
    const ACCRUAL_DISTANCE = 'DISTANCE';
    const ACCRUAL_MIXED = 'MIXED';
    const ACCRUAL_INTERVAL = 'INTERVAL';

    const AREA_CITY = 'CITY';
    const AREA_TRACK = 'TRACK';
    const AREA_AIRPORT = 'AIRPORT';
    const AREA_RAILWAY = 'RAILWAY';

    const COST_UNIT_1_MINUTE = '1_minute';
    const COST_UNIT_30_MINUTE = '30_minute';
    const COST_UNIT_1_HOUR = '1_hour';

    const DEFAULT_ROUNDING = 0.01;
    const ROUNDING_TYPE_ROUND = 'ROUND';
    const ROUNDING_TYPE_CEIL = 'CEIL';
    const ROUNDING_TYPE_FLOOR = 'FLOOR';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_tariff}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOption()
    {
        return $this->hasMany(FixHasOption::className(), ['option_id' => 'option_id']);
    }
}