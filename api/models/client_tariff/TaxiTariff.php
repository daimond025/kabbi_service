<?php

namespace app\models\client_tariff;

use yii\db\ActiveRecord;

/**
 * Class TaxiTariff
 * @package app\models\client_tariff
 *
 * @property TaxiTariffOption $currentAreaCityOption
 * @property TaxiTariffOption $holidaysAreaCityOption
 * @property TaxiTariffOption $exceptionAreaCityOption
 *
 * @property TaxiTariffOption $currentAreaTrackOption
 * @property TaxiTariffOption $holidaysAreaTrackOption
 * @property TaxiTariffOption $exceptionAreaTrackOption
 *
 * @property TaxiTariffOption $currentAreaAirportOption
 * @property TaxiTariffOption $holidaysAreaAirportOption
 * @property TaxiTariffOption $exceptionAreaAirportOption
 *
 * @property TaxiTariffOption $currentAreaStationOption
 * @property TaxiTariffOption $holidaysAreaStationOption
 * @property TaxiTariffOption $exceptionAreaStationOption
 */
class TaxiTariff extends ActiveRecord
{
    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAreaCityOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_CURRENT,
                'area'        => TaxiTariffOption::AREA_CITY,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAreaTrackOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_CURRENT,
                'area'        => TaxiTariffOption::AREA_TRACK,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolidaysAreaCityOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_HOLIDAYS,
                'area'        => TaxiTariffOption::AREA_CITY,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolidaysAreaTrackOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_HOLIDAYS,
                'area'        => TaxiTariffOption::AREA_TRACK,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExceptionAreaCityOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_EXCEPTIONS,
                'area'        => TaxiTariffOption::AREA_CITY,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExceptionAreaTrackOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_EXCEPTIONS,
                'area'        => TaxiTariffOption::AREA_TRACK,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAreaAirportOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_CURRENT,
                'area'        => TaxiTariffOption::AREA_AIRPORT,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolidaysAreaAirportOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_HOLIDAYS,
                'area'        => TaxiTariffOption::AREA_AIRPORT,
            ]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExceptionAreaAirportOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_EXCEPTIONS,
                'area'        => TaxiTariffOption::AREA_AIRPORT,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAreaStationOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_CURRENT,
                'area'        => TaxiTariffOption::AREA_RAILWAY,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolidaysAreaStationOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_HOLIDAYS,
                'area'        => TaxiTariffOption::AREA_RAILWAY,
            ]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExceptionAreaStationOption()
    {
        return $this->hasOne(TaxiTariffOption::className(), ['tariff_id' => 'tariff_id'])
            ->onCondition([
                'tariff_type' => TaxiTariffOption::TYPE_EXCEPTIONS,
                'area'        => TaxiTariffOption::AREA_RAILWAY,
            ]);
    }




}
