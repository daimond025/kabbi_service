<?php
namespace app\models\client_tariff;

use yii\db\ActiveRecord;

/**
 * Class OptionActiveDate
 * @package app\models\client_tariff
 *
 * @property int $date_id
 * @property int $tariff_id
 * @property string $active_date
 * @property string $tariff_type
 *
 */
class OptionActiveDate extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%option_active_date}}';
    }


}
