<?php
namespace app\models\client_tariff;

use yii\db\ActiveRecord;

/**
 * Class FixHasOption
 * @package app\models\client_tariff
 *
 * @property int $option_id
 * @property int $fix_id
 * @property float $price_to
 * @property float $price_from
 */
class FixHasOption extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%fix_has_option}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFix()
    {
        return $this->hasOne(FixTariff::className(), ['fix_id' => 'fix_id']);
    }
}
