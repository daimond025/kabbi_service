<?php
namespace app\models\client_tariff;

use yii\db\ActiveRecord;


class TaxiTariffHasCity extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }
}
