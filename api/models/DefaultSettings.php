<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%default_settings}}".
 *
 * @property string $name
 * @property string $value
 * @property string $type
 */
class DefaultSettings extends \yii\db\ActiveRecord
{

    const GEOSERVICE_API_KEY = 'GEOSERVICE_API_KEY';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value', 'type'], 'string'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'value' => 'Value',
            'type' => 'Type',
        ];
    }
    

    public static function getSetting($name)
    {
        return self::find()->where(['name' => $name])->select('value')->scalar();
    }
}
