<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
return [
    'id'                  => 'api_service_console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\commands',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],
        'db'    => $db['dbMain'],
        'log'   => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],

    'params' => $params,
];
