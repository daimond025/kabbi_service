<?php

use app\models\DefaultSettings;

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/params.php')
);

return [
    'id'         => 'api',
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => ['log'],
    'components' => [
        'parkingService' => [
            'class' => 'app\modules\v1\components\ParkingService',
        ],
        'taxiTariffService' => [
            'class' => 'app\modules\v1\components\TaxiTariffService',
        ],
        'geoservice' => [
            'class' => 'app\modules\v1\components\geoservice\Geoservice',
            'host' => 'https://geo.kabbi.eu',
//            'port' => '3100',
            'version' => '1',
            'apiKey' => function () {
                return DefaultSettings::getSetting(DefaultSettings::GEOSERVICE_API_KEY);
            },
        ],
        'gearman_v1'                  => [
            'class' => 'app\modules\v1\components\gearman\Gearman',
            'host'  => '192.168.81.20'
        ],
        'sms_v1'                      => [
            'class' => 'app\modules\v1\components\taxiSmsComponent\TaxiSmsComponent',
        ],
        'i18n'                        => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],
        'user'                        => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'mailer'                      => [
            'class'            => 'yii\swiftmailer\Mailer',
            // it doesn't matter if it will write to file or actually send email, right?
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.yandex.ru',
                'port'       => 465,
                'username'   => 'api_service@gootax.pro',
                'password'   => 'try',
                'encryption' => 'ssl',
            ],
        ],
        'log'                         => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error', 'warning'],
                    'message' => [
                        'from'    => ['api_service@gootax.pro'],
                        'to'      => ['api_service@gootax.pro'],
                        'subject' => 'Error in KABBI',
                    ],
                ],
            ],
        ],
        'errorHandler'                => [
            'errorAction' => '/v1/route-analyzer/error',
        ],
        'urlManager'                  => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'version' => 'v1/site/version',
            ],
        ],
        'assetManager'                => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets'
        ],
        'request'                     => [
            'baseUrl'              => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'  => 'ymB2hl6GmkDJkoB_TnQrz_RzzsfoCISp',
            'enableCsrfValidation' => false
        ],
        'redis_drivers'               => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 0,
        ],
        'redis_orders_active'         => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 1,
        ],
        'redis_phone_scenario'        => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 2,
        ],
        'redis_active_call'           => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 3,
        ],
        'redis_queue_call'            => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 4,
        ],
        'redis_active_dispetcher'     => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 5,
        ],
        'redis_free_dispetcher_queue' => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 6,
        ],
    ],
    'modules'    => [
        'debug' => [
            'class'      => 'yii\debug\Module',
            'allowedIPs' => ['192.168.1.*']
        ],
        'v1'    => [
            'basePath' => '@app/modules/v1',
            'class'    => 'app\modules\v1\Module'   // here is our v1 modules
        ],
        'v2'    => [
            'basePath' => '@app/modules/v2',
            'class'    => 'app\modules\v2\Module'   // here is our v1 modules
        ]
    ],
    'params'     => $params,
];

