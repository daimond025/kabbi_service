<?php

return [
    'adminEmail'              => 'api_service@gootax.pro',
    'gootaxUrl'               => "https://kabbi.eu",
    'iosDriverPushTypeServer' => 'production', //sandbox   -тип сервера для ios водительского приложения
    'iosClientPushTypeServer' => 'production',
    'az_geocoder_accuracy'    => 0.0006,
    'version'                 => require __DIR__ . '/version.php',
];
