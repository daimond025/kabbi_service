<?php

$db = [
    'dbMain' => [
        'class'               => 'yii\db\Connection',
        'charset'             => 'utf8',
        'tablePrefix'         => 'tbl_',
        'dsn'                 => getenv('DB_MAIN_DSN'),
        'username'            => getenv('DB_MAIN_USERNAME'),
        'password'            => getenv('DB_MAIN_PASSWORD'),
        'enableSchemaCache'   => getenv('DB_MAIN_SCHEMA_CACHE_ENABLE') ? true : false,
        'schemaCacheDuration' => (int)getenv('DB_MAIN_SCHEMA_CACHE_DURATION'),
    ],

    'redisCache' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_MAIN'),
    ],

    'redisCacheCheckStatus' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_CHECK_STATUS'),
    ],

    'redisMainOrdersActive' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ORDERS_ACTIVE'),
    ],
];

if ((bool)getenv('DB_MAIN_SLAVE_ENABLE')) {
    $db['dbMain']['slaveConfig'] = [
        'username' => getenv('DB_MAIN_SLAVE_USERNAME'),
        'password' => getenv('DB_MAIN_SLAVE_PASSWORD'),
        'charset'  => 'utf8',
    ];

    $db['dbMain']['slaves'] = [
        ['dsn' => getenv('DB_MAIN_SLAVE_DSN')],
    ];
}

return $db;