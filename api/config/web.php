<?php

$db = require __DIR__ . '/db.php';

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$params = require __DIR__ . '/params.php';

$config = [
    'id'                  => 'api_service',
    'basePath'            => dirname(__DIR__),
    'vendorPath'          => dirname(dirname(__DIR__) . '/vendor'),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [

        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],

        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'db'                       => $db['dbMain'],
        'redis_cache_check_status' => $db['redisCacheCheckStatus'],
        'redis_orders_active'      => $db['redisMainOrdersActive'],

        'errorHandler' => [
            'class' => \api\modules\v1\components\ErrorHandler::className(),
            'errorAction' => '/v1/route-analyzer/error',
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'user' => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => false,
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'request' => [
            'baseUrl'              => '',
            'cookieValidationKey'  => getenv('COOKIE_VALIDATION_KEY'),
            'enableCsrfValidation' => false,
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/routes.php',
        ],

        'parkingService' => [
            'class' => 'app\modules\v1\components\ParkingService',
        ],

        'taxiTariffService' => [
            'class' => 'app\modules\v1\components\TaxiTariffService',
        ],

        'promoCodeService' => [
            'class' => 'api\modules\v1\components\PromoCodeService',
        ],

        'geoservice' => [
            'class'   => 'app\modules\v1\components\geoservice\Geoservice',
            'baseUrl' => getenv('API_GEO_URL'),
            'version' => 1,
            'apiKey'  => function () {
                return \app\models\DefaultSettings::getSetting(\app\models\DefaultSettings::GEOSERVICE_API_KEY);
            },
        ],

        'apiOrder' => [
            'class'   => 'app\modules\v1\components\apiOrder\ApiOrder',
            'baseUrl' => getenv('API_ORDER_URL'),
            'timeout' => getenv('API_ORDER_TIMEOUT'),
        ],

        'gearman' => [
            'class' => 'app\modules\v1\components\gearman\Gearman',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],

        'sms_v1' => [
            'class' => 'app\modules\v1\components\taxiSmsComponent\TaxiSmsComponent',
        ],

        'logger' => [
            'class'  => 'api\components\logger\Logger',
            'targetClass'    => 'api\components\logger\target\SyslogTarget',
            'targetIdentity' => getenv('SYSLOG_IDENTITY') . '-' . $params['version'],
        ],

    ],

    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class'    => 'app\modules\v1\Module',
        ],
    ],

    'params' => $params,
];

return $config;