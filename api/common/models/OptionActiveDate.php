<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_option_active_date".
 *
 * @property integer $date_id
 * @property integer $tariff_id
 * @property string $active_date
 * @property string $tariff_type
 *
 * @property TaxiTariff $tariff
 */
class OptionActiveDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_option_active_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'active_date', 'tariff_type'], 'required'],
            [['tariff_id'], 'integer'],
            [['tariff_type'], 'string'],
            [['active_date'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date_id' => 'Date ID',
            'tariff_id' => 'Tariff ID',
            'active_date' => 'Active Date',
            'tariff_type' => 'Tariff Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
