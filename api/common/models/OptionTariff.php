<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_option_tariff".
 *
 * @property integer $option_id
 * @property integer $tariff_id
 * @property string $accrual
 * @property string $area
 * @property double $planting_price_day
 * @property double $planting_price_night
 * @property integer $planting_include_day
 * @property integer $planting_include_night
 * @property double $next_km_price_day
 * @property double $next_km_price_night
 * @property integer $min_price_day
 * @property integer $min_price_night
 * @property double $supply_price_day
 * @property double $supply_price_night
 * @property integer $wait_time_day
 * @property integer $wait_time_night
 * @property integer $wait_driving_time_day
 * @property integer $wait_driving_time_night
 * @property double $wait_price_day
 * @property double $wait_price_night
 * @property double $speed_downtime_day
 * @property double $speed_downtime_night
 * @property double $rounding_day
 * @property double $rounding_night
 * @property string $tariff_type
 * @property integer $allow_day_night
 * @property string $start_day
 * @property string $end_day
 * @property integer $active
 *
 * @property FixHasOption[] $fixHasOptions
 * @property FixTariff[] $fixes
 * @property TaxiTariff $tariff
 */
class OptionTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_option_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'area', 'tariff_type'], 'required'],
            [['tariff_id', 'planting_include_day', 'planting_include_night', 'min_price_day', 'min_price_night', 'wait_time_day', 'wait_time_night', 'wait_driving_time_day', 'wait_driving_time_night', 'allow_day_night', 'active'], 'integer'],
            [['accrual', 'area', 'tariff_type'], 'string'],
            [['planting_price_day', 'planting_price_night', 'next_km_price_day', 'next_km_price_night', 'supply_price_day', 'supply_price_night', 'wait_price_day', 'wait_price_night', 'speed_downtime_day', 'speed_downtime_night', 'rounding_day', 'rounding_night'], 'number'],
            [['start_day', 'end_day'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'tariff_id' => 'Tariff ID',
            'accrual' => 'Accrual',
            'area' => 'Area',
            'planting_price_day' => 'Planting Price Day',
            'planting_price_night' => 'Planting Price Night',
            'planting_include_day' => 'Planting Include Day',
            'planting_include_night' => 'Planting Include Night',
            'next_km_price_day' => 'Next Km Price Day',
            'next_km_price_night' => 'Next Km Price Night',
            'min_price_day' => 'Min Price Day',
            'min_price_night' => 'Min Price Night',
            'supply_price_day' => 'Supply Price Day',
            'supply_price_night' => 'Supply Price Night',
            'wait_time_day' => 'Wait Time Day',
            'wait_time_night' => 'Wait Time Night',
            'wait_driving_time_day' => 'Wait Driving Time Day',
            'wait_driving_time_night' => 'Wait Driving Time Night',
            'wait_price_day' => 'Wait Price Day',
            'wait_price_night' => 'Wait Price Night',
            'speed_downtime_day' => 'Speed Downtime Day',
            'speed_downtime_night' => 'Speed Downtime Night',
            'rounding_day' => 'Rounding Day',
            'rounding_night' => 'Rounding Night',
            'tariff_type' => 'Tariff Type',
            'allow_day_night' => 'Allow Day Night',
            'start_day' => 'Start Day',
            'end_day' => 'End Day',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOptions()
    {
        return $this->hasMany(FixHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixes()
    {
        return $this->hasMany(FixTariff::className(), ['fix_id' => 'fix_id'])->viaTable('tbl_fix_has_option', ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
