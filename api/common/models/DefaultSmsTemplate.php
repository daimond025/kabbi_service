<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_default_sms_template".
 *
 * @property integer $template_id
 * @property string $type
 * @property string $text
 * @property string $params
 * @property string $description
 */
class DefaultSmsTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_default_sms_template';
    }
}
