<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_fix_tariff".
 *
 * @property integer $fix_id
 * @property integer $from
 * @property integer $to
 *
 * @property FixHasOption[] $fixHasOptions
 * @property OptionTariff[] $options
 * @property Parking $from0
 * @property Parking $to0
 */
class FixTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_fix_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fix_id' => 'Fix ID',
            'from' => 'From',
            'to' => 'To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOptions()
    {
        return $this->hasMany(FixHasOption::className(), ['fix_id' => 'fix_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(OptionTariff::className(), ['option_id' => 'option_id'])->viaTable('tbl_fix_has_option', ['fix_id' => 'fix_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom0()
    {
        return $this->hasOne(Parking::className(), ['parking_id' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo0()
    {
        return $this->hasOne(Parking::className(), ['parking_id' => 'to']);
    }
}
