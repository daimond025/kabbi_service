<?php

namespace api\common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_client".
 *
 * @property string        $client_id
 * @property string        $tenant_id
 * @property string        $city_id
 * @property string        $photo
 * @property string        $last_name
 * @property string        $name
 * @property string        $second_name
 * @property string        $email
 * @property integer       $black_list
 * @property integer       $priority
 * @property string        $create_time
 * @property integer       $active
 * @property string        $success_order
 * @property string        $fail_driver_order
 * @property string        $fail_client_order
 * @property string        $birth
 * @property integer       $password
 *
 * @property Tenant        $tenant
 * @property ClientPhone[] $clientPhones
 */
class Client extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['success_order', 'fail_worker_order', 'fail_client_order', 'fail_dispatcher_order'], 'integer'],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    public static function findByClientId($client_id, $select = [])
    {
        $clientPhone = ClientPhone::find()
            ->where(['client_id' => $client_id])
            ->with([
                'client' => function ($query) use ($select) {
                    $query->select($select);
                },
            ])
            ->one();

        return $clientPhone->client;
    }

    /**
     * Обновление счетчиков клиента
     *
     * @param type $clientId
     * @param type $status_id
     */
    public static function updateClientOrderCounters($clientId, $status_id)
    {
        $client = Client::find()
            ->where(['client_id' => $clientId])
            ->one();
        if (!$client) {
            return false;
        }
        if (in_array($status_id, OrderStatus::getCompletedStatusId())) {
            $client->success_order++;

            return $client->save();
        }

        if (in_array($status_id, OrderStatus::getRejectedStatusIdByClient())) {
            $client->fail_client_order++;

            return $client->save();
        }

        if (in_array($status_id, OrderStatus::getRejectedStatusIdByWorker())) {
            $client->fail_worker_order++;

            return $client->save();
        }

        if (in_array($status_id, OrderStatus::getRejectedStatusIdByDispatcher())) {
            $client->fail_dispatcher_order++;

            return $client->save();
        }

        return false;
    }


    private static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t($category, $message, $params, $language);
    }

}
