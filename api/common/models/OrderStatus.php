<?php

namespace api\common\models;

use Yii;
use api\common\models\OrderViews;

/**
 * This is the model class for table "{{%order_status}}".
 *
 * @property integer $status_id
 * @property string $name
 * @property string $status_group
 * @property integer $dispatcher_sees
 *
 * @property Order[] $orders
 * @property OrderStatusTime[] $orderStatusTimes
 */
class OrderStatus extends \yii\db\ActiveRecord
{

    const FILTER_STATUS_1 = 'executing';
    const FILTER_STATUS_2 = 'completed';
    const FILTER_STATUS_3 = 'rejected';
    const STATUS_GROUP_0 = 'new';
    const STATUS_GROUP_1 = 'car_assigned';
    const STATUS_GROUP_2 = 'car_at_place';
    const STATUS_GROUP_3 = 'executing';
    const STATUS_GROUP_4 = 'completed';
    const STATUS_GROUP_5 = 'rejected';
    const STATUS_GROUP_6 = 'pre_order';
    const STATUS_GROUP_7 = 'warning';
    const STATUS_GROUP_8 = 'works';
    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_DRIVER = 17;
    const STATUS_DRIVER_WAITING = 26;
    const STATUS_REJECTED = 39;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_PAYMENT_CONFIRM = 106;
    const CACHE_KEY = 'tbl_order_status';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dispatcher_sees'], 'integer'],
            [['name', 'status_group'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id'       => 'Status ID',
            'name'            => 'Name',
            'status_group'    => 'Status Group',
            'dispatcher_sees' => 'Dispatcher Sees',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTimes()
    {
        return $this->hasMany(OrderStatusTime::className(), ['status_id' => 'status_id']);
    }

    public static function getFilterStatusList($filter_group)
    {
        switch ($filter_group) {
            case self::FILTER_STATUS_1:
                $filterStatusList = [
                    '17' => t('order', 'Driving to client'),
                    '26' => t('order', 'Arrived'),
                    '33' => t('order', 'Not out'),
                    '36' => t('order', 'Execution'),
                ];
                break;
            case self::FILTER_STATUS_2:
                $filterStatusList = [
                    '37' => t('order', 'Paid'),
                    '38' => t('order', 'Not paid'),
                ];
                break;
            case self::FILTER_STATUS_3:
                $filterStatusList = [
                    '39' => t('order', 'Refusal of the client'),
                    '40' => t('order', 'No cars'),
                    '41' => t('order', 'Сonflict with the driver'),
                    '42' => t('order', 'Сonflict with the dispatcher'),
                    '43' => t('order', 'Not out'),
                    '44' => t('order', 'Not answer the phone'),
                    '45' => t('order', 'Late driver'),
                    '46' => t('order', 'Accident'),
                    '47' => t('order', 'Car crash'),
                    '48' => t('order', 'Сonflict with the client'),
                ];
                break;
            default :
                $filterStatusList = [];
        }

        return $filterStatusList;
    }

    public static function getNewStatusId()
    {
        return [1, 2, 3, 4, 5, 52, 108, 109, 113, 114, 115];
    }

    public static function getWorksStatusId()
    {
        return [17, 26, 27, 29, 30, 36, 54, 55, 106, 110];
    }

    public static function getWarningStatusId()
    {
        return [5, 16, 27, 30, 38, 45, 46, 47, 48, 52, 54];
    }

    public static function getPreOrderStatusId()
    {
        return [6, 7, 16, 111, 112, 116, 117, 118, 119];
    }

    public static function getCompletedStatusId()
    {
        return [37, 38];
    }

    public static function getRejectedStatusId()
    {
        return [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 107, 120];
    }

    public static function getRejectedStatusIdByClient()
    {
        return [39, 40, 41, 42, 43, 44];
    }

    public static function getRejectedStatusIdByWorker()
    {
        return [45, 46, 47, 48, 50, 51, 120];
    }

    public static function getRejectedStatusIdByDispatcher()
    {
        return [107];
    }

    public static function getStatusWithPickUpTime()
    {
        return [1, 5];
    }

    public static function getBlockEditStatuses()
    {
        return [1, 2, 3];
    }

    /**
     * For order update card.
     * @return array
     */
    public static function getCountdownSatusList()
    {
        return [1, 4, 17, 26, 54];
    }

    /**
     * Allow to know order status group.
     * @param integer $status_id
     * @return array Statuses
     */
    public static function getStatusGroup($status_id)
    {
        $arStatus = [];

        if (in_array($status_id, self::getWarningStatusId())) $arStatus[] = 'warning';
        if (in_array($status_id, self::getNewStatusId())) $arStatus[] = 'new';
        if (in_array($status_id, self::getWorksStatusId())) $arStatus[] = 'works';
        if (in_array($status_id, self::getPreOrderStatusId())) $arStatus[] = 'pre_order';
        if (in_array($status_id, self::getCompletedStatusId())) $arStatus[] = 'completed';
        if (in_array($status_id, self::getRejectedStatusId())) $arStatus[] = 'rejected';

        return $arStatus;
    }

    /**
     * Группы, которые входят в раздел "В работе"
     * @return array
     */
    public static function getSubWorkGroups()
    {
        return [
            self::STATUS_GROUP_1,
            self::STATUS_GROUP_2,
            self::STATUS_GROUP_3
        ];
    }

    /**
     * Группы, у которых необходимо выводить счетчик новых заказов
     * @return array
     */
    public static function getGroupsForCalculating()
    {
        return [
            self::STATUS_GROUP_0,
            self::STATUS_GROUP_8,
            self::STATUS_GROUP_7,
            self::STATUS_GROUP_6,
        ];
    }

    /**
     * Группы, которые хранятся в редисе
     * @return array
     */
    public static function getGroupsFromRedis()
    {
        return [
            self::STATUS_GROUP_0,
            self::STATUS_GROUP_8,
            self::STATUS_GROUP_7,
            self::STATUS_GROUP_6,
        ];
    }

    /**
     * Список статусов из БД (Кешируется).
     * @return array
     */
    public static function getStatusData()
    {
        $cache = Yii::$app->cache;
        $data = $cache->get(self::CACHE_KEY);

        if ($data === false) {
            $data = self::find()->asArray()->all();
            $cache->set(self::CACHE_KEY, $data);
        }

        return $data;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //При обновлении чистим кеш
        $cache = Yii::$app->cache;
        $cache->delete(self::CACHE_KEY);
    }

    /**
     * Группы статусов при которых на карте отображается водитель.
     * @return array
     */
    public static function getStatusGroupsForMapDriverType()
    {
        return ['car_assigned', 'car_at_place', 'executing'];
    }

    /**
     * Группы статусов при которых на карте отображается маршрут заказа.
     * @return array
     */
    public static function getStatusGroupsForMapRouteType()
    {
        return ['completed'];
    }

    /**
     * Обновление счетчика заказов у диспетчеров
     * @param type $statusId
     * @param type $tenantId
     * @param type $ciyId
     */
    public static function updateDispatcherOrderCounters($statusId, $tenantId, $ciyId, $inc)
    {
        //Обновление счетчика заказов
        $status_group = self::getStatusGroup($statusId);
        $calculating_group = array_intersect($status_group, self::getGroupsForCalculating());

        if (!empty($calculating_group)) {
            foreach ($calculating_group as $group) {
                OrderViews::updateAllCounters([$group => $inc], 'tenant_id = ' . $tenantId . ' AND city_id = ' . $ciyId);
            }

        }
    }

}
