<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_car_option".
 *
 * @property integer $option_id
 * @property string $name
 *
 * @property AdditionalOption[] $additionalOptions
 * @property CarHasOption[] $carHasOptions
 * @property Car[] $cars
 * @property DriverOptionDiscount[] $driverOptionDiscounts
 * @property TenantHasCarOption[] $tenantHasCarOptions
 * @property Tenant[] $tenants
 */
class CarOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['additional_option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('tbl_car_has_option', ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverOptionDiscounts()
    {
        return $this->hasMany(DriverOptionDiscount::className(), ['car_option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(Tenant::className(), ['tenant_id' => 'tenant_id'])->viaTable('tbl_tenant_has_car_option', ['option_id' => 'option_id']);
    }
}
