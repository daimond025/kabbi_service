<?php

namespace api\common\models;

use app\models\client_tariff\TaxiTariff;
use app\models\company\TenantCompanyRecord;
use app\models\transport\car\Car;
use app\models\worker\Worker;

class Order extends \yii\db\ActiveRecord
{

    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_1 = 'IOS';
    const DEVICE_2 = 'ANDROID';
    const PRE_ODRER_TIME = 1800;
    const PICK_UP_TIME = 300;
    const STATUS_NEW_FREE = 4;
    const STATUS_NEW_EXPIRED = 52;
    const STATUS_NEW_PRDV = 6;
    const STATUS_NEW_PRDV_FREE = 15;
    const STATUS_NEW_PRDV_NO_PARKING = 16;

    public $additional_option = [];
    // public $phone;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $status_reject;
    private $pickUp;
    private $orderOffset;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'status_id'], 'required'],
            [['tenant_id', 'driver_id', 'city_id', 'tariff_id', 'client_id', 'user_create', 'status_id', 'user_modifed', 'company_id', 'parking_id', 'order_number', 'show_phone', 'create_time', 'order_time'], 'integer'],
            [['address', 'comment', 'payment', 'phone'], 'string'],
            [['show_phone', 'additional_option', 'order_date', 'order_hours', 'order_minutes', 'status_reject'], 'safe'],
            [['predv_price'], 'number'],
            [['device'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'         => 'Order ID',
            'tenant_id'        => 'Tenant ID',
            'driver_id'        => 'Driver ID',
            'city_id'          => 'City ID',
            'tariff_id'        => 'Tariff',
            'client_id'        => 'Client ID',
            'user_create'      => 'User Create',
            'user_modifed'     => 'User Modifed',
            'status_id'        => 'Status',
            'address'          => 'Address',
            'comment'          => 'Comment',
            'predv_price'      => 'Predv Price',
            'create_time'      => 'Create Time',
            'device'           => 'Device',
            'payment'          => 'Payment',
            'show_phone'       => 'Show client phone to driver',
            'parking_id'       => 'Parking',
            'preliminary_calc' => 'Preliminary calculation',
            'phone'            => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasOne(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompanyRecord::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    public function getOrderDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id']);
    }

    /**
     * Получить заказа из редиса по Ид
     * @param type $orderId
     */
    public static function getOrderForId($orderId, $tenantId)
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenantId, $orderId]);
        $order = unserialize($order);
        if (empty($order)) {
            return null;
        }
        return $order;
    }

}
