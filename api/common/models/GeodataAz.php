<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_geodata_az".
 *
 * @property integer $id
 * @property string $city_az
 * @property string $city_en
 * @property string $city_ru
 * @property string $street_az
 * @property string $street_en
 * @property string $street_ru
 * @property string $label_az
 * @property string $label_en
 * @property string $label_ru
 * @property string $type
 * @property double $lat
 * @property double $lon
 * @property integer $weight
 * @property string $house
 */
class GeodataAz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_geodata_az';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_az', 'city_en', 'city_ru', 'street_az', 'street_en', 'street_ru', 'label_az', 'label_en', 'label_ru', 'type', 'lat', 'lon'], 'string'],
            [['weight'], 'integer'],
            [['house'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_az' => 'City Az',
            'city_en' => 'City En',
            'city_ru' => 'City Ru',
            'street_az' => 'Street Az',
            'street_en' => 'Street En',
            'street_ru' => 'Street Ru',
            'label_az' => 'Label Az',
            'label_en' => 'Label En',
            'label_ru' => 'Label Ru',
            'type' => 'Type',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'weight' => 'Weight',
            'house' => 'House',
        ];
    }
}
