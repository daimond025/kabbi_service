<?php

namespace api\common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_client_temp".
 *
 * @property integer $client_temp_id
 * @property integer $tenant_id
 * @property string $phone
 * @property string $password
 */
class ClientTemp extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_temp}}';
    }
}
