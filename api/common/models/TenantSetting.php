<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_tenant_setting".
 *
 * @property int    $setting_id
 * @property int    $tenant_id
 * @property string $name
 * @property string $value
 * @property int    $city_id
 * @property int    $position_id
 *
 * @property Tenant $tenant
 */
class TenantSetting extends \yii\db\ActiveRecord
{
    const CACHE_TIME = 3600;
    const CACHE_ON = true;

    const SETTING_LANGUAGE = 'LANGUAGE';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'value'], 'required'],
            [['tenant_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'value'      => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Getting setting value
     *
     * @param int    $tenantId
     * @param string $name
     * @param int    $cityId
     * @param int    $positionId
     *
     * @return string
     */
    public static function getSettingValue($tenantId, $name, $cityId = null, $positionId = null)
    {
        $value = self::find()
            ->select('value')
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $name,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->scalar();

        return !empty($value) ? $value : null;
    }

}
