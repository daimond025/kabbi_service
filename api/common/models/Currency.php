<?php

namespace api\common\models;

class Currency extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    public static function getSymbol($currency_id)
    {
        return (string)self::find()
            ->select('symbol')
            ->where(['currency_id' => $currency_id])
            ->scalar();
    }
}
