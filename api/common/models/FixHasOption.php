<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_fix_has_option".
 *
 * @property integer $option_id
 * @property integer $fix_id
 * @property string $price_to
 * @property string $price_back
 *
 * @property OptionTariff $option
 * @property FixTariff $fix
 */
class FixHasOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_fix_has_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'fix_id', 'price_to', 'price_back'], 'required'],
            [['option_id', 'fix_id'], 'integer'],
            [['price_to', 'price_back'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'fix_id' => 'Fix ID',
            'price_to' => 'Price To',
            'price_back' => 'Price Back',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(OptionTariff::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFix()
    {
        return $this->hasOne(FixTariff::className(), ['fix_id' => 'fix_id']);
    }
}
