<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_parking".
 *
 * @property integer        $parking_id
 * @property integer        $tenant_id
 * @property integer        $city_id
 * @property string         $name
 * @property string         $polygon
 * @property integer        $sort
 * @property string         $type
 * @property integer        $is_active
 *
 * @property FixTariff[]    $fixTariffs
 * @property Tenant         $tenant
 * @property ParkingOrder[] $parkingOrders
 */
class Parking extends \yii\db\ActiveRecord
{

    const TYPE_AIRPORT = 'airport';
    const TYPE_STATION = 'station';
    const TYPE_BASE_POLYGON = 'basePolygon';
    const TYPE_CITY = 'city';
    const TYPE_TRACK = 'track';
    const TYPE_RECEPTION_AREA = 'reseptionArea';

    const DISTRICT_TYPE_PERCENT = 'PERCENT';
    const DISTRICT_TYPE_MONEY = 'MONEY';

    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_parking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'name', 'polygon', 'type'], 'required'],
            [['tenant_id', 'city_id', 'sort', 'is_active'], 'integer'],
            [['polygon'], 'string'],
            [['name', 'type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parking_id' => 'Parking ID',
            'tenant_id'  => 'Tenant ID',
            'city_id'    => 'City ID',
            'name'       => 'Name',
            'polygon'    => 'Polygon',
            'sort'       => 'Sort',
            'type'       => 'Type',
            'is_active'  => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixTariffs()
    {
        return $this->hasMany(FixTariff::className(), ['to' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkingOrders()
    {
        return $this->hasMany(ParkingOrder::className(), ['parking_id' => 'parking_id']);
    }

    public static function getParkingArrAll($tenant_id, $city_id)
    {
        return Parking::find()
            ->where(['tenant_id' => $tenant_id])
            ->andWhere(['city_id' => $city_id])
            ->andWhere('type <> :type', [':type' => 'reseptionArea'])
            ->andWhere(['is_active' => 1])
            ->orderBy('sort DESC')
            ->asArray()
            ->all();
    }

}
