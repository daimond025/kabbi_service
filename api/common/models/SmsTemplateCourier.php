<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "tbl_sms_template".
 *
 * @property integer $template_id
 * @property integer $tenant_id
 * @property string $type
 * @property string $text
 *
 * @property Tenant $tenant
 */
class SmsTemplateCourier extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sms_template_courier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

}
