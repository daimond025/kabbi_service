<?php

namespace api\common\models;

use Yii;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $account_id
 * @property integer $acc_kind_id
 * @property integer $acc_type_id
 * @property integer $owner_id
 * @property integer $currency_id
 * @property integer $tenant_id
 * @property double $balance
 *
 * @property AccountKind $accKind
 * @property AccountType $accType
 * @property Currency $currency
 * @property Tenant $tenant
 * @property Operation[] $operations
 * @property Transaction[] $transactions
 */
class Account extends \yii\db\ActiveRecord
{

    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;
    const TENANT_KIND = 1;
    const DRIVER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;
    const DEFAULT_CURRENCY = 1;

    public $sum;
    public $transaction_comment;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            ['transaction_comment', 'string'],
            [['balance', 'sum'], 'number'],
            [['balance'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccKind()
    {
        return $this->hasOne(AccountKind::className(), ['kind_id' => 'acc_kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccType()
    {
        return $this->hasOne(AccountType::className(), ['type_id' => 'acc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['sender_acc_id' => 'account_id']);
    }

    /**
     * Создание счета
     * @param integer $kind_id Тип счета
     * @param integer $owner_id Собственник счета
     * @param integer $currency_id Валюта
     * @param integer $tenant_id Ид заказчика
     * @return bool
     */
    public function create($kind_id, $owner_id, $currency_id, $tenant_id)
    {
        $this->acc_kind_id = $kind_id;
        $this->acc_type_id = self::PASSIVE_TYPE;
        $this->owner_id = $owner_id;
        $this->currency_id = $currency_id;
        $this->tenant_id = $tenant_id;
        return $this->save();
    }

}
