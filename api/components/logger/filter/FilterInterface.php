<?php

namespace api\components\logger\filter;

interface FilterInterface
{
    public static function filter($data);
}