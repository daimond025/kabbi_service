<?php

namespace api\components\logger\filter;

class Filter implements FilterInterface
{

    public static function filter($data)
    {
        foreach ($data as $key => $value) {
            switch ($key) {

                case 'address_array':
                    $value      = urldecode($value);
                    $value      = json_decode($value, true);
                    $value      = array_values($value);
                    $value      = self::filterAddress($value);
                    $data[$key] = json_encode($value);
                    break;

                case 'routeLine':
                    $data[$key] = 'array[' . count($value) . ']';
                    break;

                case 'tariffInfo':
                    //                            $data[$key] = self::filteredTariffInfo($value);
                    break;

                case 'tariff_id':
                    $data[$key] = is_array($value) ? implode(',', $value) : $value;
            }
        }

        return $data;

    }

    private static function filterAddress($data)
    {
        $safeKeys = ['city_id', 'lat', 'lon', 'parking_id'];
        $result   = [];

        foreach ($data as $address) {
            $filteredAddress = [];
            foreach ($address as $key => $value) {
                if (in_array($key, $safeKeys)) {
                    $filteredAddress[$key] = $value;
                }
            }
            $result[] = $filteredAddress;
        }

        return $result;
    }

    private static function filteredTariffInfo($data)
    {
        $data['tariffDataCity']  = 'array';
        $data['tariffDataTrack'] = 'array';

        return $data;
    }
}