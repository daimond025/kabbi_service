<?php

namespace api\components\logger\target;

use yii\base\Component;

class FakeTarget extends Component implements LogTargetInterface
{
    /**
     * @var string
     */
    public $identity;

    public function export($message, $severity)
    {

    }
}