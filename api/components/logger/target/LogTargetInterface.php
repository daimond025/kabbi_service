<?php

namespace api\components\logger\target;

interface LogTargetInterface
{
    public function export($message, $severity);
}