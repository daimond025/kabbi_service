<?php

namespace api\components\logger\target;

use yii\base\Object;

class SyslogTarget extends Object implements LogTargetInterface
{
    /**
     * @var string
     */
    public $identity;

    public function export($message, $severity)
    {
        openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
        syslog($severity, $message);
        closelog();
    }
}