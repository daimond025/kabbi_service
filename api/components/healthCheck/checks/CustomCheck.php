<?php

namespace healthCheck\checks;

use healthCheck\exceptions\HealthCheckException;

/**
 * Class CustomCheck
 * @package healthCheck\checks
 */
class CustomCheck extends BaseCheck
{
    /**
     * @var callable
     */
    private $checkFunction;

    /**
     * CustomCheck constructor.
     *
     * @param string   $name
     * @param callable $checkFunction
     */
    public function __construct($name, callable $checkFunction)
    {
        $this->checkFunction = $checkFunction;

        parent::__construct($name);
    }

    public function run()
    {
        try {
            call_user_func($this->checkFunction);
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Custom check exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }

}