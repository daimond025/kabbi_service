<?php

return [
    'Order rejected' => 'Заказ отменен',
    'From'           => 'Откуда',
    'New order'      => 'Новый заказ',
    'income'         => 'Внесено',
    'expenses'       => 'Списано',
    'Your balance'   => 'Ваш баланс',
];

