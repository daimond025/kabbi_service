<?php

return[
    'Conditioner'                       => 'Кондиционер',
    'wi-fi'                             => 'Wi - fi',
    'Big luggage carrier'               => 'Большой багажник',
    'Child safety chair'                => 'Детское кресло',
    'Universal'                         => 'Универсал',
    'Card payment'                      => 'Платеж картой',
    'Fresh news papers and magazines'   => 'Свежие журналы и газеты',
    'Pet transport'                     => 'Перевозка животных',
    'Strict report at payment'          => 'Строгий отчет при платеже',
    'Taxi checkers'                     => 'Шашки',
    'Yellow color'                      => 'Желтый цвет',
    'Advertisement on car'              => 'Реклама на машине',
];
