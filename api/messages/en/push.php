<?php

return [
    'Order rejected' => 'Order rejected',
    'From'           => 'From',
    'New order'      => 'New order',
    'income'         => 'income',
    'expenses'       => 'expenses',
    'Your balance'   => 'Your balance',
];

