<?php

namespace app\modules\v2\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class RouteAnalyzerController extends Controller
{

    public $headers;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'ping' => ['get'],
                ],
            ]
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [
            "code" => "404",
            "info" => "METHOD_NOT_FOUND"
        ];
        return $result;
    }

    public function actionPing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [
            "code"   => "0",
            "info"   => "OK",
            "result" => "PONG",
        ];
        return $result;
    }

}
