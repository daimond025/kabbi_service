<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 *
 * @var string        $lang
 * @var string        $siteAddress
 * @var string        $user_name
 * @var string        $company_name
 */
?>

<!-- START TITLE + TEXT -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="title+text">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#daf6ef">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#daf6ef">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="center" class="mobile" style="font-family:arial, sans-serif; font-size:20px; line-height:26px; font-weight:bold;" st-title="title+text">
                                    <?= Yii::t('email', 'Hello {user}!', [
                                            'user' => Html::encode($user_name)
                                            ], $lang) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left" style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #4d4d4d; line-height:18px; padding:0 20px;" st-content="title+text">
                                    <?= Yii::t('email', 'Received a chat message', [], $lang) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"> </td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END TITLE + TEXT -->
<!-- START 1 IMAGE + TEXT COLUMN -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#ffffff">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left" style="font-family:arial, sans-serif; font-size:16px; line-height:20px; padding:0 20px;" st-title="1-image+text-column">
                                    <b><?= Html::encode($worker_name) ?></b>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <font color="#696969">
                                        <?= Html::encode($sender_id) ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?= Html::encode($sender_role) ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?= Html::encode($city) ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?= Html::encode($send_time) ?>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" style="line-height:10px; font-size:10px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left" style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000; line-height:18px; padding:0 20px;" st-content="1-image+text-column">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="footer">
                                        <tr>
                                            <td width="100%" valign="top" cellpadding="0" cellspacing="0" border="0">
                                                <!-- Start Wrapper  -->
                                                <table width="100%">
                                                    <tr>
                                                        <td  align="left" style="font-family:arial, sans-serif; font-size:16px; line-height:20px;">
                                                            <?= Html::encode($message) ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- End Wrapper  -->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                            <tr>
                                <td height="40" style="line-height:40px; font-size:40px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td class="mobile" style="font-size:14px; line-height:20px;" align="center">
                                    <!-- Start Button -->
                                    <table width="190" cellpadding="0" cellspacing="0" align="center" border="0" bgcolor="#2cbc92" st-button="2-images+text-columns">
                                        <tr>
                                            <td width="190" height="40" align="center" valign="middle" style="font-family:arial, sans-serif; font-size: 20px; color: #ffffff; line-height:22px; border-radius:3px;" st-content="2-images+text-columns">
                                                <a href="<?= $siteAddress ?>" target="_blank" alias="" style="font-family:arial, sans-serif; text-decoration: none; color: #ffffff;">
                                                    <?= Yii::t('email', 'Go to site', [], $lang) ?>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Button -->
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"> </td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;">
                                    <hr style="border-style: solid; border-width: 1px 0 0 0; border-color: #cccccc;"/>
                                </td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END 1 IMAGE + TEXT COLUMN -->