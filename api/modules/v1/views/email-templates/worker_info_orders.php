<?php

use api\common\models\ClientPhone;
use app\models\worker\Worker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use \api\common\models\Order;
use \app\models\worker\WorkerShiftNote;


/**
 * @var \yii\base\View $this
 *
 * @var string         $worker_id
 * @var integer        $time_start
 * @var integer        $time_end
 * */

function getShortName($last_name, $name = '', $second_name = '')
{
    $result = $last_name;

    if (!empty($name)) {
        $result .= ' ' . mb_substr($name, 0, 1, "UTF-8") . '. ';
    }

    if (!empty($second_name)) {
        $result .= mb_substr($second_name, 0, 1, "UTF-8") . '.';
    }

    return $result;
}

function getFullClient($last_name, $name = '', $second_name = ''){
    $last_ = ucfirst($last_name);
    $name_ = ucfirst($name);
    $second_ = ucfirst($second_name);

    return $last_. ' ' . $name_;
}

function getWorkerOrders($timeStart, $timeEnd, $workerId) {
    return  Order::find()
        ->alias('o')
       // ->select(['o.*' , 'c.last_name', 'c.name' ])
        //->leftJoin('{{%client}} c', 'c.client_id = o.client_id')
        ->where(['o.worker_id' => (int)$workerId])
        ->andWhere("o.order_time >= $timeStart AND o.order_time <= $timeEnd")
        ->joinWith('client c')
        ->joinWith('clientPhones cp')
        ->orderBy([
            'o.order_time' => SORT_ASC
        ])
        //->asArray()
        ->all();
}

function getWorkerNote($workerId){
    $note =  WorkerShiftNote::find()
        ->where([
            'worker_id' => (int)$workerId
        ])
        ->one();

    return ($note) ?  $note->note : null;
}

function getWorker($worker_id){
    return Worker::find()
        ->where(['worker_id' =>$worker_id])
        ->asArray()
        ->one();

}
function getClientPhone($client_id){
    $clientPhone = ClientPhone::find()
        ->where(['client_id' => $client_id])
        ->one();
}

$worker = getWorker($worker_id);
$worker_orders = getWorkerOrders($time_start, $time_end, $worker_id);

$workerNote = getWorkerNote($worker_id);


$date_from = date('Y-m-d H:i', $time_start);
$date_end  = date('Y-m-d H:i', $time_end  );
$date  = date('d.m.Y', $time_start  );

/*foreach($worker_orders as $order){
    $client = $order->client->client_id;

    var_dump( $order->clientPhones);
    exit();
}
var_dump(123);
exit();*/

?>


<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="title+text">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper"
                   bgcolor="#daf6ef">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#daf6ef">

                        <!-- Start Container -->
                        <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="center" colspan="3" class="mobile"
                                    style="font-family:arial, sans-serif; font-size:20px; line-height:26px; font-weight:bold;"
                                    st-title="title+text">
                                    Guten Tag, <?= ucfirst($worker['name'])  . ' '. ucfirst ($worker['last_name']) ?>!
                                </td>
                            </tr>
                            <tr>
                                <td height="20" colspan="3" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left" colspan="3"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 15px; color: #4d4d4d; line-height:18px; padding:0 20px;"
                                    st-content="title+text">
                                    <?= Html::encode('Ihr vorläufiger Fahrplan für den '. $date ) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" colspan="3" style="line-height:25px; font-size:25px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr style="display: table-row">
                                <td align="left" width="10%"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px;padding:0 10px; text-align: center"
                                    st-content="title+text">
                                    <b>Zeit</b>
                                </td>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; text-align: center"
                                    st-content="title+text">
                                    <b>Adresse</b>
                                </td>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; text-align: center"
                                    st-content="title+text">
                                    <b>Kunde</b>
                                </td>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; text-align: center"
                                    st-content="title+text">
                                    <b>Bemerkung</b>
                                </td>
                            </tr>
                            <?php foreach($worker_orders as $order): ?>

                                <tr style="height: 65px; display: table-row">
                                    <td align="left" width="10%"
                                        style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; text-align: center; border-bottom: 3px solid"
                                        st-content="title+text">
                                        <?= date('H:i', ArrayHelper::getValue($order,'order_time', '')); ?>
                                    </td>
                                    <td align="left"
                                        style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px;  border-bottom: 3px solid; width: 40%"
                                        st-content="title+text">

                                        <?
                                        $address = unserialize(ArrayHelper::getValue($order, 'address', []));
                                        ?>
                                        <? foreach($address as $key => $value): ?>

                                        <p style="margin-bottom: 2px; margin-top: 0px;">
                                                <span class="od_d">(<?= Html::encode($key); ?>)</span>
                                            <span style="line-height: initial">
                <?
                $direction = [];
                $direction[] = $value['city'];
                $direction[] = empty($value['house'])
                    ? $value['street'] : $value['street'].' '.$value['house'];
                $direction[] = empty($value['housing'])
                    ? null : t('order', 'b. ').$value['housing'];

                echo Html::encode(implode(', ', array_filter($direction, function($item){
                    return !empty(($item));
                })));
                ?>
                                       </span>
                                        </p>


                                        <? endforeach; ?>
                                    </td>
                                    <td align="left"
                                        style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; border-bottom: 3px solid; text-align: center;"
                                        st-content="title+text">
                                        <span style="line-height: initial"><?= getFullClient(  ArrayHelper::getValue($order->client,'last_name', '') , ArrayHelper::getValue($order->client,'name', '')  ) ;?> </span>
                                        <span style="line-height: initial"> <?=ArrayHelper::getValue($order->clientPhones,'value', '') ;?> </span>

                                    </td>
                                    <td align="left"
                                        style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 10px; border-bottom: 3px solid; text-align: center;"
                                        st-content="title+text">
                                        <?=ArrayHelper::getValue($order,'comment', '') ;?>

                                    </td>
                                </tr>

                            <?php endforeach; ?>


                            <tr>
                                <td height="30" colspan="3" style="line-height:30px; font-size:30px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <? if($workerNote) :?>
                                <tr>
                                    <td align="left" colspan="3"
                                        style="
                                         font-family:Verdana, Arial, sans serif;
                                         font-size: 15px;
                                          color: #4d4d4d;
                                          line-height:18px;
                                          padding:0 20px;"
                                          st-content="title+text">
                                        <b> Notizen ändern : </b>
                                        <?= Html::encode( $workerNote ) ?>
                                    </td>
                                </tr>
                            <? endif;?>

                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
