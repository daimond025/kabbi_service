<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 *
 * @var string        $order_number
 * @var string        $order_date
 * @var string        $address_points
 * @var string        $tariff_name
 * @var string        $order_time
 * @var string        $order_cost
 * @var string        $currency_symbol
 * @var string        $company_name
 * @var string        $company_phone
 * @var string        $company_email
 * @var string        $company_site
 * @var string        $worker_name
 * @var string        $worker_phone
 * @var string        $car_model
 * @var string        $car_brand
 * @var string        $car_color
 * @var string        $car_gos_number
 * @var string        $currency_symbol
 * @var string        $supply_cost
 * @var string        $included_supply_time
 * @var string        $included_supply_distance
 * @var string        $included_supply_cost
 * @var string        $supply_outside_city_tariff
 * @var string        $supply_outside_city_meter
 * @var string        $supply_outside_city_cost
 * @var string        $before_boarding_cost
 * @var string        $city_distance_tariff
 * @var string        $city_distance_meter
 * @var string        $city_distance_cost
 * @var string        $city_time_tariff
 * @var string        $city_time_meter
 * @var string        $city_time_cost
 * @var string        $outside_city_distance_tariff
 * @var string        $outside_city_distance_meter
 * @var string        $outside_city_distance_cost
 * @var string        $outside_city_time_tariff
 * @var string        $outside_city_time_meter
 * @var string        $outside_city_time_cost
 * @var string        $waiting_city_tariff
 * @var string        $waiting_city_meter
 * @var string        $waiting_city_cost
 * @var string        $waiting_outside_city_tariff
 * @var string        $waiting_outside_city_meter
 * @var string        $waiting_outside_city_cost
 * @var string        $additional_cost
 * @var string        $summary_distance
 * @var string        $summary_time
 * @var string        $bonus_replenishment
 * @var string        $promo_bonus_replenishment
 * @var string        $summary_cost_no_discount
 * @var string        $summary_cost
 * @var string        $lang
 */

?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" st-sortable="full-text">
    <tr>
        <td width="100%" valign="top" align="left">
            <!-- Start Wrapper  -->
            <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                   bgcolor="#ffffff">
                <tr>
                    <td colspan="2" align="left" style="padding-left: 25px;">
                        <h1 style="font-size: 28px; font-weight: 400; margin-bottom: 0;"><?= Yii::t('order', 'Report on the trip', [], $lang) ?></h1>
                        <p style="color: #898989; margin-top: 8px;"><?= Yii::t('order', 'Order', [], $lang) ?> №<?= Html::encode($order_number) ?>
                            от <?= Html::encode($order_date) ?></p>
                    </td>
                </tr>
                <tr>
                    <td height="20"
                        style="font-size:10px; line-height:10px;"></td>
                </tr>

                <?php
                $delimiter = <<<TXT
                                        <tr>
                                            <td colspan="2" align="left" style="padding-left: 40px;">
                                                <font color="909090">|</font>
                                            </td>
                                        </tr>
TXT;

                $addresses      = explode(';', $address_points);
                $addressContent = [];
                foreach ($addresses as $address) {
                    $addressContent[] = '
                                        <tr>
                                                <td align="left" style="padding-left: 35px; width: 25px;">
                                                    <img src="http://foto.gootax.ru/img/dot.png"
                                                         width="14" height="14"
                                                         style="margin:0; padding:0; border:none;"
                                                         border="0" class="centerClass" alt=""
                                                         st-image="image"/>
                                                </td>
                                                <td align="left" style="padding-right: 35px; width: 400px;">
                                                    ' . Html::encode($address) . '
                                                </td>
                                            </tr>
                                        ';

                } ?>

                <?= implode($delimiter, $addressContent) ?>

                <tr>
                    <td height="30"
                        style="font-size:10px; line-height:10px;"></td>
                    <!-- Spacer -->
                </tr>
            </table>

            <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                   bgcolor="#ffffff">
                <tr>
                    <td align="left" style="padding-left: 25px; padding-right: 25px;">
                        <p style="color: #898989;"><?= Yii::t('order', 'Tariff', [], $lang) ?></p>
                        <?= Html::encode($tariff_name) ?>
                    </td>
                    <td align="left" style="padding-left: 25px; padding-right: 25px;">
                        <p style="color: #898989;"><?= Yii::t('order', 'Trip time', [], $lang) ?></p>
                        <?= Html::encode($order_time) ?> <?= Yii::t('order', 'm.', [], $lang) ?>
                    </td>
                    <td align="left" style="padding-left: 25px; padding-right: 25px;">
                        <p style="color: #898989;"><?= Yii::t('order', 'Cost', [], $lang) ?></p>
                        <?= Html::encode($order_cost) ?> <?= Html::encode($currency_symbol) ?>
                    </td>
                </tr>
                <tr>
                    <td height="30"
                        style="font-size:10px; line-height:10px;"></td>
                    <!-- Spacer -->
                </tr>
                <tr>
                    <td align="left" colspan="3" style="padding-left: 12px; padding-right: 12px;">
                        <table width="616" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                               bgcolor="#efefef">
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td colspan="3" align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <b><?= Yii::t('order', 'Trip Detail', [], $lang) ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Submission car', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($supply_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>

                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order',
                                        'Price includes {distance} km and {time} min',
                                        [
                                            'distance' => Html::encode($included_supply_distance),
                                            'time'     => Html::encode($included_supply_time),
                                        ], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($included_supply_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Distance for submission car', [], $lang); ?>
                                    <?= Html::encode($supply_outside_city_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('order', 'm.', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($supply_outside_city_meter) ?>
                                        <?= Yii::t('app', 'km', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($supply_outside_city_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Time waiting before landing in the car', [], $lang); ?>
                                    <?= Html::encode($before_boarding_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('order', 'm.', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($before_boarding_meter) ?>
                                        <?= Yii::t('app', 'sec.', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($before_boarding_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'City', [], $lang); ?>
                                    <?= Html::encode($city_distance_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('app', 'km', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($city_distance_meter) ?>
                                        <?= Yii::t('app', 'km', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($city_distance_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'City', [], $lang); ?>
                                    <?= Html::encode($city_time_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('order', 'm.', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($city_time_meter) ?>
                                        <?= Yii::t('app', 'sec.', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($city_time_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Out of the city', [], $lang); ?>
                                    <?= Html::encode($outside_city_distance_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('app', 'km', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($outside_city_distance_meter) ?>
                                        <?= Yii::t('app', 'km', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($outside_city_distance_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Out of the city', [], $lang); ?>
                                    <?= Html::encode($outside_city_time_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('app', 'km', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($outside_city_time_meter) ?>
                                        <?= Yii::t('app', 'sec.', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($outside_city_time_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Downtime in city', [], $lang); ?>
                                    <?= Html::encode($waiting_city_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('order', 'm.', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($waiting_city_meter) ?>
                                        <?= Yii::t('app', 'sec.', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($waiting_city_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Downtime in outcity', [], $lang); ?>
                                    <?= Html::encode($waiting_outside_city_tariff) ?>
                                    <?= Html::encode($currency_symbol) ?>/<?= Yii::t('order', 'm.', [], $lang) ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e">
                                        <?= Html::encode($waiting_outside_city_meter) ?>
                                        <?= Yii::t('app', 'sec.', [], $lang) ?>
                                    </font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($waiting_outside_city_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left"
                                    style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Additional options', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($additional_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Summary time', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($summary_time) ?> <?= Yii::t('app', 'sec.', [], $lang) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Refilled bonus', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($bonus_replenishment) ?> <?= Yii::t('currency', 'B', [], $lang) ?>
                                    (<?= Html::encode($currency_symbol) ?>)
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Refilled promo bonus', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($promo_bonus_replenishment) ?> <?= Yii::t('currency', 'B', [],
                                        $lang) ?>(<?= Html::encode($currency_symbol) ?>)
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Cost without discount', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($summary_cost_no_discount) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                            <tr>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Yii::t('order', 'Cost with discount', [], $lang); ?>
                                </td>
                                <td align="left" style="padding-left: 15px; padding-right: 15px;">
                                    <font color="6e6e6e"></font>
                                </td>
                                <td align="right" style="padding-left: 15px; padding-right: 15px;">
                                    <?= Html::encode($summary_cost) ?> <?= Html::encode($currency_symbol) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"
                                    style="font-size:10px; line-height:10px;"></td>
                                <!-- Spacer -->
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="20"
                        style="font-size:10px; line-height:10px;"></td>
                    <!-- Spacer -->
                </tr>
                <tr>
                    <td align="left" colspan="3">
                        <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                               bgcolor="#ffffff">
                            <tr>
                                <td align="left" style="padding-left: 25px; padding-right: 25px;" width="33.33%"
                                    valign="top">
                                    <p style="color: #898989;"> <?=\Yii::t('car', 'Company', [], $lang) ?></p>
                                    <p style="line-height: 1.7; margin-top: 0;">
                                        <?= Html::encode($company_name) ?><br>
                                        <?= Html::encode($company_phone) ?><br>
                                        <a href="mailto:<?= Html::encode($company_email) ?>">
                                            <?= Html::encode($company_email) ?>
                                        </a><br>
                                        <a href="<?= Html::encode($company_site) ?>">
                                            <?= Html::encode($company_site) ?>
                                        </a>
                                    </p>
                                </td>
                                <td align="left" style="padding-left: 25px; padding-right: 25px;" width="33.33%"
                                    valign="top">
                                    <p style="color: #898989;"><?=  \Yii::t('order', 'Driver', [], $lang)?></p>
                                    <p style="line-height: 1.7; margin-top: 0;">
                                        <?= Html::encode($worker_name) ?><br>
                                        <?= Html::encode($worker_phone) ?>
                                    </p>
                                </td>
                                <td align="left" style="padding-left: 25px; padding-right: 25px;" width="33.33%"
                                    valign="top">
                                    <p style="color: #898989;"><?= \Yii::t('reports', 'Car', [], $lang)?></p>
                                    <p style="line-height: 1.7; margin-top: 0;">
                                        <?= Html::encode("$car_brand $car_model,") ?><br>
                                        <?= Html::encode("$car_gos_number, $car_color"); ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
