<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string        $lang
 * @var string        $user_name
 * @var string        $user_email
 * @var string        $user_password
 * @var string        $url
 *
 */
?>
<p><?= \Yii::t('email', 'Hello', [], $lang) ?>, <?= Html::encode($user_name) ?>!</p>
<p><?= \Yii::t('email', 'Thank you for your registration in Gootax', [], $lang) ?>.</p>
<p><?= \Yii::t('email', 'Your gootax', [], $lang) ?> - <?= Html::a($url, $url) ?></p>
<p><?= \Yii::t('email', 'Login', [], $lang) ?>: <?= Html::encode($user_email) ?></p>
<p><?= \Yii::t('email', 'Password', [], $lang) ?>: <?= Html::encode($user_password) ?></p>