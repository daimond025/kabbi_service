<?php
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $lang
 * @var string $user_name
 * @var string $url
 */

$text = 'Dear {name},<br>'
 . 'You recently have requested a password reset. Please use the link below to change your password.<br>'
 . '<a href="{url}">{url}</a><br>';

echo \Yii::t('email',$text,[
    'name' => Html::encode($user_name),
    'url' => Html::encode($url),
], $lang);