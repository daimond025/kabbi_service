<?php

namespace app\modules\v1\services;

use app\modules\v1\helpers\NotificationHelper;
use app\modules\v1\repositories\dto\orders\OrderAddressPointData;
use app\modules\v1\repositories\dto\orders\OrderData;
use app\modules\v1\repositories\dto\orders\OrderRecipientData;
use app\modules\v1\repositories\OrderRepository;
use app\modules\v1\repositories\RecipientRepository;
use yii\helpers\ArrayHelper;

class RecipientService
{
    public $recipientRepository;
    public $orderRepository;


    public function __construct(RecipientRepository $recipientRepository, OrderRepository $orderRepository)
    {
        $this->recipientRepository = $recipientRepository;
        $this->orderRepository     = $orderRepository;
    }

    /**
     * @param $tenantId
     * @param $orderId
     * @param $type
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function sendNotificationForRecipients($tenantId, $orderId, $type)
    {
        $order    = $this->orderRepository->getOrder($tenantId, $orderId);
        $template = $this->recipientRepository->getTemplate($tenantId, $order->cityId, $order->positionId, $type);

        $smsComponent = \Yii::$app->get('sms_v1');

        /** @var OrderAddressPointData $point */
        foreach ($order->address as $key => $point) {
            if ($key === 0) {
                continue;
            }
            $paramsMap = $this->getParamsMap($template->params, $order, $point->recipient);
            if (!empty($point->recipient->phone)) {
                $message = str_replace(array_keys($paramsMap), $paramsMap, $template->text);
                $smsComponent->sendSms($tenantId, $order->cityId, $point->recipient->phone, $message);
            }
        }
    }

    private function getParamsMap($params, OrderData $orderData, OrderRecipientData $recipientData)
    {
        $paramsMap = [];

        if (ArrayHelper::isIn(NotificationHelper::PARAM_WORKER_LAST_NAME, $params)) {
            $paramsMap[NotificationHelper::PARAM_WORKER_LAST_NAME] = $orderData->worker->lastName;
        }

        if (ArrayHelper::isIn(NotificationHelper::PARAM_WORKER_NAME, $params)) {
            $paramsMap[NotificationHelper::PARAM_WORKER_NAME] = $orderData->worker->name;
        }

        if (ArrayHelper::isIn(NotificationHelper::PARAM_WORKER_SECOND_NAME, $params)) {
            $paramsMap[NotificationHelper::PARAM_WORKER_SECOND_NAME] = $orderData->worker->secondName;
        }

        if (ArrayHelper::isIn(NotificationHelper::PARAM_WORKER_PHONE, $params)) {
            $paramsMap[NotificationHelper::PARAM_WORKER_PHONE] = $orderData->worker->phone;
        }

        if (ArrayHelper::isIn(NotificationHelper::PARAM_CODE, $params)) {
            $paramsMap[NotificationHelper::PARAM_CODE] = $recipientData->code;
        }

        return $paramsMap;
    }
}
