<?php

namespace app\modules\v1\services;

use api\common\models\TenantSetting;
use api\modules\v1\models\email\EmailTypes;
use api\modules\v1\models\email\templates\EmailTemplate;
use app\models\worker\Worker;
use app\modules\v1\components\gearman\Gearman;
use app\modules\v1\repositories\dto\EmailSettingData;
use app\modules\v1\repositories\EmailRepository;
use app\modules\v1\repositories\NotFoundException;
use app\modules\v1\repositories\TenantRepository;
use Ramsey\Uuid\Uuid;
use yii\helpers\ArrayHelper;
use yii\swiftmailer\Mailer;

class EmailService
{
    const DEFAULT_LANG = 'ru';

    public $emailRepository;
    public $tenantRepository;

    public function __construct(EmailRepository $emailRepository, TenantRepository $tenantRepository)
    {
        $this->emailRepository  = $emailRepository;
        $this->tenantRepository = $tenantRepository;
    }

    /**
     * @param integer  $tenantId
     * @param integer  $cityId
     * @param string   $type
     * @param string   $lang
     * @param string[] $to
     * @param string[] $params
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function send($tenantId, $cityId, $type, $lang, array $to, $params)
    {
        $settingData = $this->getSetting($type, $tenantId, $cityId);
        $domain      = !empty($tenantId) ? $this->tenantRepository->getTenantDomain($tenantId) : '';

        $params['lang']        = $this->getLang($tenantId, $cityId, $lang);
        $params['siteAddress'] = ArrayHelper::getValue(\Yii::$app->params, 'siteProtocol') . '://'
            . $domain . '.' . ArrayHelper::getValue(\Yii::$app->params, 'siteDomain');

        /** @var EmailTemplate $template */
        $template = \Yii::createObject([
            'class'    => EmailTemplate::className(),
            'layout'   => $settingData->template,
            'viewPath' => EmailTypes::getViewByType($type),
            'params'   => $params,
        ]);

        if(($type == EmailTypes::WORKER_INFO_ORDERS) && isset($params['worker_id']) && isset($params['time_start']) ) {
            $subject = $this->getWorker($params['worker_id'] ,$params['time_start'] );
        }else{
            $subject = \Yii::t('email', mb_strtolower('subject:' . $type), [], $params['lang']);
        }

        $body    = $template->render();


        $gearmanParams = [
            'requestId' => Uuid::uuid4()->toString(),
            'transport' => [
                'host'       => $settingData->provider->server,
                'port'       => $settingData->provider->port,
                'username'   => $settingData->sender->email,
                'password'   => $settingData->sender->password,
                'encryption' => 'ssl',
            ],
            'mail'      => [
                'from'    => [
                    $settingData->sender->email => $settingData->sender->name,
                ],
                'to'      => $to,
                'subject' => $subject,
                'body'    => $body,
            ],
        ];

        try {
            \Yii::$app->get('gearman')->doBackground(Gearman::EMAIL_TASK, $gearmanParams);

        } catch (\Exception $exception) {
            /** @var Mailer $mailer */
            $mailer = \Yii::createObject(Mailer::className());
            $mailer->setTransport([
                'class'      => 'Swift_SmtpTransport',
                'host'       => ArrayHelper::getValue($gearmanParams, 'transport.host'),
                'port'       => ArrayHelper::getValue($gearmanParams, 'transport.port'),
                'username'   => ArrayHelper::getValue($gearmanParams, 'transport.email'),
                'password'   => ArrayHelper::getValue($gearmanParams, 'transport.password'),
                'encryption' => ArrayHelper::getValue($gearmanParams, 'transport.encryption'),
            ]);

            $messages = [];

            foreach ($to as $addressee) {
                $messages[] = $mailer->compose()
                    ->setFrom(ArrayHelper::getValue($gearmanParams, 'mail.from'))
                    ->setTo($addressee)
                    ->setSubject(ArrayHelper::getValue($gearmanParams, 'mail.subject'))
                    ->setHtmlBody(ArrayHelper::getValue($gearmanParams, 'mail.body'));
            }
            $mailer->sendMultiple($messages);
            $mailer->getTransport()->stop();
        }
    }

    public function getWorker($worker_id, $time_start){
        $worker =  Worker::find()
            ->where(['worker_id' =>$worker_id])
            ->asArray()
            ->one();

        $worker_first = ucfirst(ArrayHelper::getValue($worker, 'last_name', '' ));
        $worker_name = ucfirst(ArrayHelper::getValue($worker, 'name', '' ));
        $date_from = date('d.m.Y', $time_start);

        return $worker_first. ', '. $worker_name. '; ' . $date_from;

    }

    /**
     * @param string  $type
     * @param integer $tenantId
     * @param integer $cityId
     *
     * @return EmailSettingData
     */
    private function getSetting($type, $tenantId, $cityId)
    {

        if (EmailTypes::isFromTenant($type)) {

            try {
                return $this->emailRepository->findTenantSetting($tenantId, $cityId);
            } catch (NotFoundException $ignore) {
            }

            return $this->emailRepository->findDefaultSettingFromTenant();
        }

        return $this->emailRepository->findDefaultSettingFromSystem();
    }

    /**
     * @param integer $tenantId
     * @param integer $cityId
     * @param string  $lang
     *
     * @return string
     */
    private function getLang($tenantId, $cityId, $lang)
    {
        if ($lang) {
            return $lang;
        }

        if ($currentLang = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_LANGUAGE, $cityId)) {
            return $currentLang;
        }

        return self::DEFAULT_LANG;
    }
}