<?php

namespace app\modules\v1\models\tariff;

use app\models\client_tariff\FixTariff;
use yii\db\ActiveRecord;

/**
 * Class TariffHasFixRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $id
 * @property integer $fix_id
 * @property integer $type_id
 * @property float   $price_to
 * @property float   $price_back
 */
class TariffHasFixRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_fix}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFix()
    {
        return $this->hasOne(FixTariff::className(), ['fix_id' => 'fix_id']);
    }
}