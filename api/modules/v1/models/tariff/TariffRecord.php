<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $tariff_id
 * @property integer $tenant_id
 * @property integer $class_id
 * @property integer $block
 * @property integer $group_id
 * @property string  $name
 * @property string  $description
 * @property integer $sort
 * @property integer $auto_downtime
 * @property integer $enabled_site
 * @property integer $enabled_app
 * @property integer $enabled_operator
 * @property integer $enabled_bordur
 * @property integer $enabled_cabinet
 * @property string  $logo
 * @property integer $position_id
 * @property string  $type
 */
class TariffRecord extends ActiveRecord
{
    const TYPE_COMPANY = 'COMPANY';
    const TYPE_COMPANY_CORP_BALANCE = 'COMPANY_CORP_BALANCE';
    const TYPE_BASE = 'BASE';
    const TYPE_ALL = 'ALL';

    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }
}