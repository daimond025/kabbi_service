<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffTypeRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $type_id
 * @property integer $tariff_id
 * @property string  $type
 * @property integer $sort
 */
class TariffTypeRecord extends ActiveRecord
{

    const TYPE_CURRENT = 'CURRENT';
    const TYPE_EXCEPTION = 'EXCEPTION';

    public static function tableName()
    {
        return '{{%taxi_tariff_option_type}}';
    }
}