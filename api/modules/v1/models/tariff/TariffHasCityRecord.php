<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffHasCityRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $tariff_id
 * @property integer $city_id
 */
class TariffHasCityRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }
}