<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffOptionRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $option_id
 * @property integer $type_id
 * @property string  $accrual
 * @property string  $area
 * @property float   $planting_price
 * @property float   $planting_include
 * @property float   $next_km_price
 * @property float   $min_price
 * @property float   $second_min_price
 * @property float   $supply_price
 * @property integer $wait_time
 * @property integer $wait_driving_time
 * @property float   $wait_price
 * @property float   $speed_downtime
 * @property float   $rounding
 * @property string  $rounding_type
 * @property integer $enabled_parking_ratio
 * @property integer $calculation_fix
 * @property string  $next_cost_unit
 * @property float   $next_km_price_time
 * @property integer $planting_include_time
 */
class TariffOptionRecord extends ActiveRecord
{
    const AREA_CITY = 'CITY';
    const AREA_TRACK = 'TRACK';
    const AREA_AIRPORT = 'AIRPORT';
    const AREA_STATION = 'RAILWAY';

    public static function tableName()
    {
        return '{{%taxi_tariff_option}}';
    }

    public function getType()
    {
        return $this->hasOne(TariffTypeRecord::className(), ['type_id' => 'type_id']);
    }
}