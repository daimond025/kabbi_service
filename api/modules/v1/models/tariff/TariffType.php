<?php

namespace app\modules\v1\models\tariff;

class TariffType extends TariffTypeRecord
{
    public function getCityAreaOption()
    {
        return $this->hasOne(TariffOptionRecord::className(), ['type_id' => 'type_id'])
            ->onCondition(['area' => TariffOptionRecord::AREA_CITY]);
    }

    public function getTrackAreaOption()
    {
        return $this->hasOne(TariffOptionRecord::className(), ['type_id' => 'type_id'])
            ->onCondition(['area' => TariffOptionRecord::AREA_TRACK]);
    }

    public function getAirportAreaOption()
    {
        return $this->hasOne(TariffOptionRecord::className(), ['type_id' => 'type_id'])
            ->onCondition(['area' => TariffOptionRecord::AREA_AIRPORT]);
    }

    public function getStationAreaOption()
    {
        return $this->hasOne(TariffOptionRecord::className(), ['type_id' => 'type_id'])
            ->onCondition(['area' => TariffOptionRecord::AREA_STATION]);
    }
}