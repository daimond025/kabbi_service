<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffHasAdditionalRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $additional_option_id
 * @property float   $price
 */
class TariffHasAdditionalRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_additional}}';
    }
}