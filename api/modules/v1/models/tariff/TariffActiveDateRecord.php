<?php

namespace app\modules\v1\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffActiveDateRecord
 * @package app\modules\v1\models\tariff
 *
 * @property integer          $date_id
 * @property integer          $type_id
 * @property string           $active_date
 *
 * @property TariffTypeRecord $tariffType
 */
class TariffActiveDateRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_option_active_date}}';
    }

    public function getTariffType()
    {
        return $this->hasOne(TariffTypeRecord::className(), ['type_id' => 'type_id']);
    }
}