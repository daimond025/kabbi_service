<?php

namespace app\modules\v1\models\tariff;

class TariffActiveDate extends TariffActiveDateRecord
{
    public function getTariffType()
    {
        return $this->hasOne(TariffType::className(), ['type_id' => 'type_id']);
    }
}