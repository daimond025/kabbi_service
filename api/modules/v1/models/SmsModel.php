<?php

namespace app\modules\v1\models;

use app\modules\v1\components\SmsTemplateService;
use Yii;
use app\models\order\OrderService;
use yii\helpers\ArrayHelper;

/**
 * Class SmsModel
 * @package app\modules\v1\models
 */
class SmsModel extends \yii\base\Model
{
    const TO_CLIENT = 'client';
    const TO_PASSENGER = 'passenger';
    /**
     * Отправка SMS уведомления клиенту о статусе заказа
     *
     * @param int    $tenantId
     * @param int    $statusId
     * @param int    $orderId
     * @param string $to
     *
     * @return boolean
     */
    public static function sendSmsNotification($tenantId, $orderId, $statusId, $to)
    {
        $order_data  = OrderService::getOrder($tenantId, $orderId);

        $cityId     = ArrayHelper::getValue($order_data, 'city_id');
        $positionId = ArrayHelper::getValue($order_data, 'position_id');

        switch ($to) {
            case self::TO_CLIENT:
                $phone = ArrayHelper::getValue($order_data, 'client.phone');
                break;
            case self::TO_PASSENGER:
                $phone = ArrayHelper::getValue($order_data, 'passenger.phone');
                break;
            default:
                $phone = null;
        }

        if (!$phone) {
            throw new \DomainException('Empty phone number');
        }

        $smsTemplateService = new SmsTemplateService();
        $templateText       = $smsTemplateService->getSmsMessage(
            $tenantId,
            $cityId,
            $phone,
            $statusId,
            $positionId,
            $order_data
        );

        if (empty($templateText)) {
            return false;
        }

        $smsComponent = Yii::$app->sms_v1;

        return (bool)$smsComponent->sendSms($tenantId, $cityId, $phone, $templateText);
    }

    public static function sendPassword($tenantId, $phone, $cityId, $positionId)
    {
        $smsTemplateService = new SmsTemplateService();
        $templateText       = $smsTemplateService->getSmsMessage($tenantId, $cityId, $phone, 'CODE',
            $positionId, ['tenant_id' => $tenantId]);

        if (empty($templateText)) {
            return false;
        }

        $smsComponent = Yii::$app->sms_v1;
        return (bool)$smsComponent->sendSms($tenantId, $cityId, $phone, $templateText);
    }

    public static function sendTextClient($tenantId,$phones, $text ){
        $smsComponent = Yii::$app->sms_v1;

        $countPnones = count($phones);
        $countSuccess = 0;
        foreach ($phones as $phone){
            $rezultSend =  $smsComponent->sendSms($tenantId, $phone['city'], $phone['phone'], $text);
            //$rezultSend =  $smsComponent->sendSms($tenantId, $phone['city'], '+79772602817', $text);
            //break;
            if($rezultSend){
                $countSuccess +=1;
            }
        }

        //return true;

        if(($countPnones == $countSuccess ) || ($countPnones == ($countSuccess - 1) )){
            return true;
        }
        return false;
    }

}