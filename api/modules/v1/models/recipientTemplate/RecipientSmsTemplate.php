<?php

namespace api\modules\v1\models\recipientTemplate;

use yii\db\ActiveRecord;

/**
 * Class RecipientSmsTemplate
 * @package backend\modules\setting\models
 *
 * @property integer $template_id
 * @property string  $text
 * @property integer $type
 * @property integer $city_id
 * @property integer $tenant_id
 * @property integer $position_id
 */
class RecipientSmsTemplate extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%recipient_sms_template}}';
    }
}
