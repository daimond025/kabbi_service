<?php

namespace api\modules\v1\models\email;

use yii\helpers\ArrayHelper;

class EmailTypes
{
    // Регистрация арендатора
    const REGISTER = 'register';

    // Регистрация арендатора
    const WORKER_INFO_ORDERS = 'WORKER_INFO_ORDERS';

    // Подтверждение регистации сотрудника
    const EMAIL_USER_CONFIRM = 'confirm_user_email';

    // Восстановление пароля сотрудника
    const PASSWORD_RESET_TOKEN = 'password_reset_token';

    // Непрочитанные сообщения в чате сотрудника
    const CHAT_UNREAD_MESSAGE = 'chat_unread_message';

    // Подтверждение получения оплаты от арендатора
    const PAYMENT_NOTICE = 'payment_notice';

    // Отчет о заказе клиенту
    const CLIENT_ORDER_REPORT = 'client_order_report';

    /**
     * @param string $type
     *
     * @return boolean
     */
    public static function isFromTenant($type)
    {
        $customizableTypes = self::getFromTenantTypeList();

        if (ArrayHelper::isTraversable($customizableTypes)) {
            return ArrayHelper::isIn($type, $customizableTypes);
        }

        return false;
    }

    /**
     * Список типов с использованием настроек арендатора
     *
     * @return string[]
     */
    public static function getFromTenantTypeList()
    {
        $customizableTypes = [
            self::EMAIL_USER_CONFIRM,
            self::WORKER_INFO_ORDERS,
            self::CHAT_UNREAD_MESSAGE,
            self::PASSWORD_RESET_TOKEN,
            self::CLIENT_ORDER_REPORT,
        ];

        return array_intersect(self::getTypeList(), $customizableTypes);
    }

    /**
     * Список всех типов
     *
     * @return string[]
     */
    public static function getTypeList()
    {
        $types = self::getTypes();

        if (ArrayHelper::isTraversable($types)) {
            return ArrayHelper::getColumn($types, 'type');
        }

        return [];
    }

    public static function getParamsByType($value)
    {
        return self::getAttributeByType('params', $value);
    }

    public static function getViewByType($value)
    {
        return self::getAttributeByType('view', $value);
    }

    private static function getAttributeByType($attribute, $typeValue)
    {
        $types = self::getTypes();

        if (ArrayHelper::isTraversable($types)) {
            foreach ($types as $type) {
                if (ArrayHelper::getValue($type, 'type') === $typeValue) {
                    return ArrayHelper::getValue($type, $attribute);
                }
            }
        }

        return [];
    }

    private static function getTypes()
    {
        return [
            [
                'type'   => self::REGISTER,
                'view'   => '/email-templates/register',
                'params' => ['user_name', 'user_password', 'user_email', 'url', 'sales_email', 'sales_phone'],
            ],
            [
                'type'   => self::WORKER_INFO_ORDERS,
                'view'   => '/email-templates/worker_info_orders',
                'params' => ['worker_id', 'time_start', 'time_end'],
            ],
            [
                'type'   => self::EMAIL_USER_CONFIRM,
                'view'   => '/email-templates/email-user-confirm',
                'params' => ['user_name', 'user_email', 'user_password', 'url'],
            ],
            [
                'type'   => self::PASSWORD_RESET_TOKEN,
                'view'   => '/email-templates/password-reset-token',
                'params' => ['user_name', 'url'],
            ],
            [
                'type'   => self::PAYMENT_NOTICE,
                'view'   => '/email-templates/payment-notice',
                'params' => ['user_name', 'payment'],
            ],
            [
                'type'   => self::CHAT_UNREAD_MESSAGE,
                'view'   => '/email-templates/chat-unread-message',
                'params' => ['user_name', 'sender_name', 'sender_role', 'sender_id', 'city', 'send_time', 'message'],
            ],
            [
                'type'   => self::CLIENT_ORDER_REPORT,
                'view'   => '/email-templates/client-order-report',
                'params' => [
                    'order_number', // номер заказа
                    'order_date', // время заказа
                    'address_points', // адреса через ;
                    'tariff_name', // название тарифа
                    'order_time', // время поездки. ?? мин
                    'order_cost', // стоимость заказа
                    'currency_symbol', // валюта
                    'company_name',
                    'company_phone',
                    'company_email',
                    'company_site',
                    'worker_name',
                    'worker_phone',
                    'car_model',
                    'car_brand',
                    'car_color',
                    'car_gos_number',
                    'supply_cost', // подача авто. ?? руб

                    'included_supply_time', // включено в подачу. ?? мин
                    'included_supply_distance', // включено в подачу. ?? км
                    'included_supply_cost', // включено в подачу авто. ?? руб

                    'supply_outside_city_tariff', //Стоимость подачи за город. ?? руб/мин.
                    'supply_outside_city_meter', //Стоимость подачи за город. ?? км
                    'supply_outside_city_cost', //Стоимость подачи за город. ?? руб

                    'before_boarding_tariff', // Тариф время ожидания до посадки. ?? руб/мин
                    'before_boarding_meter', // Счетчик время ожидания до посадки. ?? сек
                    'before_boarding_cost', // Стоимость время ожидания до посадки. ?? руб

                    'city_distance_tariff', // Город. ?? руб/км
                    'city_distance_meter', // Город. ?? км
                    'city_distance_cost', // Город. ?? руб

                    'city_time_tariff', // Город. ?? руб/мин
                    'city_time_meter', // город. ?? сек
                    'city_time_cost', // город. ?? руб

                    'outside_city_distance_tariff', // загород. ?? руб/км
                    'outside_city_distance_meter', // загород. ?? км
                    'outside_city_distance_cost', // загород. ?? руб

                    'outside_city_time_tariff', // загород. ?? руб/мин
                    'outside_city_time_meter', // загород ?? сек
                    'outside_city_time_cost', // загород. ?? руб

                    'waiting_city_tariff', // Ожидание в городе. ?? руб/мин
                    'waiting_city_meter', // Ожидание в городе. ?? сек
                    'waiting_city_cost', //Ожидание в городе. ?? руб

                    'waiting_outside_city_tariff', // ожидание загородом. ?? руб/мин
                    'waiting_outside_city_meter', // ожидание загородом. ?? сек
                    'waiting_outside_city_cost', // ожидание загородом. ?? руб

                    'additional_cost', // доп опции. ?? руб
                    'summary_distance', // суммарное расстояние. ?? км
                    'summary_time', // суммарное время. ?? сек.
                    'bonus_replenishment', // начисление бонусов. ?? Б(руб)
                    'promo_bonus_replenishment', // начисление промо бонусов. ?? Б(руб)
                    'summary_cost_no_discount', // стоимость без скидки. ?? руб
                    'summary_cost', // стоимость со скидкой. ?? руб
                ],
            ],
        ];
    }
}