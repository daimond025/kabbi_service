<?php

namespace api\modules\v1\models\email\templates;

use yii\base\Object;
use yii\base\View;

class EmailTemplate extends Object
{
    public $layout;
    public $viewPath;
    public $params;

    protected $view;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function render()
    {
        $content = $this->getView()->render($this->viewPath, $this->params);

        return str_replace('#CONTENT#', $content, $this->layout);
    }

    /**
     * @return View view instance.
     * @throws \yii\base\InvalidConfigException
     */
    protected function getView()
    {
        if (!$this->view instanceof View) {
            $this->view = $this->createView();
        }

        return $this->view;
    }

    /**
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    protected function createView()
    {
        return \Yii::createObject(View::className());
    }
}