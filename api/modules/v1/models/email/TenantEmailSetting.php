<?php

namespace api\modules\v1\models\email;


/**
 * Class DefaultEmailSetting
 * @package api\models\email
 *
 * @property integer $tenantId
 * @property integer $cityId
 * @property integer $active
 */
class TenantEmailSetting extends DefaultEmailSetting
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_email_settings}}';
    }
}