<?php

namespace api\modules\v1\models\email;

use yii\db\ActiveRecord;

/**
 * Class DefaultEmailSetting
 * @package api\models\email
 *
 * @property integer $setting_id
 * @property string  $provider_server
 * @property integer $provider_port
 * @property string  $sender_name
 * @property string  $sender_email
 * @property string  $sender_password
 * @property string  $template
 * @property string  $type
 */
class DefaultEmailSetting extends ActiveRecord
{
    const TYPE_SYSTEM = 'system';
    const TYPE_TENANT = 'tenant';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_email_settings}}';
    }
}