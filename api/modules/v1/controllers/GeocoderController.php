<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class GeocoderController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'ping' => ['get'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                        'ips'   => ['192.168.*.*'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function actionPing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [
            "code"   => "0",
            "info"   => "OK",
            "result" => "PONG",
        ];
        return $result;
    }

    /**
     * Получение координат по строке адреса
     * Тип запроса: GET
     * @params string address
     * @return mix|array
     */
    public function actionFind_coords_by_address()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $address = Yii::$app->request->get('address');
        $address = urldecode($address);
        $geocoderType = Yii::$app->request->get('geocoder_type');
        $geocoderType = isset($geocoderType) ? $geocoderType : "ru";
        $lang = Yii::$app->request->get('lang');
        $lang = isset($lang) ? $lang : "ru";
        $geocoder = new \app\modules\v1\components\taxiGeocoder\TaxiGeocoder();
        try {
            $coords = $geocoder->findCoordsByAddress($address, $lang, $geocoderType);
            $result = [
                "code"   => "0",
                "info"   => "OK",
                "result" => $coords,
            ];
            return $result;
        } catch (\Exception $exc) {
            Yii::error($exc);
            $result = [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
            return $result;
        }
    }

    public function actionFind_address_by_coords()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $lat = Yii::$app->request->get('lat');
        $lon = Yii::$app->request->get('lon');
        $lang = Yii::$app->request->get('lang');
        $geocoder_type = Yii::$app->request->get('geocoder_type');
        $lang = isset($lang) ? $lang : "ru";
        $geocoder_type = isset($geocoder_type) ? $geocoder_type : "ru";
        $geocoder = new \app\modules\v1\components\taxiGeocoder\TaxiGeocoder();
        try {
            $address = $geocoder->findAddressByCoords($lat, $lon, $lang, $geocoder_type);
            if (!empty($address)) {
                $result = [
                    "code"   => "0",
                    "info"   => "OK",
                    "result" => $address,
                ];
            } else {
                $result = [
                    "code"   => "1",
                    "info"   => "INTERNAL_ERROR",
                    "result" => null,
                ];
            }
            return $result;
        } catch (\Exception $exc) {
            Yii::error($exc);
            $result = [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
            return $result;
        }
    }

}
