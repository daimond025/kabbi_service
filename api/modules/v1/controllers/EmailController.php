<?php

namespace app\modules\v1\controllers;

use api\modules\v1\requests\SendEmailRequest;
use api\modules\v1\requests\ValidateRequestException;
use app\modules\v1\services\EmailService;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class EmailController extends Controller
{
    public $emailService;

    public function __construct($id, Module $module, EmailService $emailService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->emailService = $emailService;

    }

    /**
     * @throws ValidateRequestException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSend()
    {

        $params  = ArrayHelper::merge(\Yii::$app->request->get(), ['params' => \Yii::$app->request->post()]);


        $request = new SendEmailRequest();
        $request->load($params, '');

        if (!$request->validate()) {
            throw new ValidateRequestException($request->getErrorsMap());
        }

        $this->emailService->send($request->tenant_id, $request->city_id, $request->type, $request->lang,
            $request->to, $request->params);
    }

}