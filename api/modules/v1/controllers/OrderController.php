<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\modules\v1\components\gearman\Gearman;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderController
 *
 * @author Дка
 */
class OrderController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'order_statistic' => ['get'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Отправка заказа в статистику
     * @params tenant_id
     * @params order_id
     * @return type
     */
    public function actionOrder_statistic()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $orderId = Yii::$app->request->get('order_id');
        Yii::$app->gearman->doBackground(Gearman::ORDER_STATISTIC, ['order_id' => $orderId]);
        $response = array(
            'result' => 1,
        );
        return $response;
    }

}
