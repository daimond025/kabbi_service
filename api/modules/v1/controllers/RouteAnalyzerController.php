<?php

namespace app\modules\v1\controllers;

use api\components\logger\filter\Filter;
use app\modules\v1\components\routeAnalyzer\formatter\BaseFormatter;
use app\modules\v1\components\routeAnalyzer\formatter\ManyFormatter;
use app\modules\v1\components\routeAnalyzer\RouteAnalyzer;
use app\modules\v1\requests\AnalyzeRouteRequest;
use app\modules\v1\requests\CalculateRequest;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class RouteAnalyzerController extends Controller
{

    public $headers;

    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'ping'                  => ['get'],
                    'fix_coords'            => ['get'],
                    'get_parking_by_coords' => ['get'],
                    'analyze_route'         => ['post'],
                    'format_parkings'       => ['post'],
                    'add_options'           => ['post'],
                    'distance_matrix'           => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                        'ips'   => ['192.168.*.*'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result                      = [
            "code" => "404",
            "info" => "METHOD_NOT_FOUND",
        ];

        return $result;
    }

    public function actionPing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result                      = [
            "code"   => "0",
            "info"   => "OK",
            "result" => "PONG",
        ];

        return $result;
    }

    /**
     * Получение информации о парковке по координатам точки
     * Тип запроса - GET
     *
     * @params string $tenantId
     * @params string $cityId
     * @params string $lat
     * @params string $lon
     * @params string $jsonFormat 1|0
     *
     * @return json
     *
     * {
     * "parking": [
     *  {
     * "parking_id": "53",
     * "tenant_id": "68",
     *  "city_id": "26068",
     * "name": "ленина",
     *  "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.27991485595703,56.84893677800939],[53.28036546707153,56.843961192143915],[53.28062295913696,56.84147315115749],[53.28085899353027,56.83892625868492],[53.28130960464477,56.83481800247118],[53.28025817871094,56.83376152086744],[53.27903509140015,56.832857618501045],[53.27665328979492,56.832528921321654],[53.27261924743652,56.832000651953045],[53.2704734802246,56.831777602648756],[53.26937913894653,56.83183629996291],[53.26487302780151,56.83312761759713],[53.263113498687744,56.833597176605565],[53.25519561767578,56.834794525426055],[53.248844146728516,56.835674904544426],[53.23888778686523,56.84240031783683],[53.23373794555664,56.846003139954476],[53.24118375778198,56.84660162078837],[53.245968818664544,56.84688325551642],[53.24845790863037,56.84725876519049],[53.25268507003784,56.848174054236786],[53.25491666793823,56.84838527160708],[53.257126808166504,56.848467411373576],[53.26347827911377,56.84887810750185],[53.26946496963501,56.84934746898674],[53.273305892944336,56.84937093690648],[53.277082443237305,56.84926533115184],[53.27991485595703,56.84893677800939]]]}}",
     *  "sort": "70",
     *  "type": "city",
     * "is_active": "1"
     *  },
     * {
     * "parking_id": "52",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "ворошилова",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.25841426849365,56.8822819360832],[53.25905799865723,56.88683042966106],[53.25742721557617,56.890440695386],[53.2672119140625,56.892222256419885],[53.27536582946777,56.89362869196798],[53.28137397766113,56.89470692336635],[53.28701734542847,56.89557417386579],[53.293519020080566,56.896582034286446],[53.29991340637207,56.89770705566122],[53.31047058105469,56.89951170667828],[53.30828189849853,56.890346926662616],[53.30819606781006,56.88933889802422],[53.30871105194092,56.88838945083311],[53.31047058105469,56.88538857023418],[53.31182241439819,56.88299707097524],[53.30961227416992,56.8825867493497],[53.30791711807251,56.8822819360832],[53.3052134513855,56.88181298774221],[53.30287456512451,56.881449548731],[53.30105066299438,56.881144726190215],[53.297295570373535,56.88051162527767],[53.29493522644043,56.880077827720726],[53.29293966293335,56.87957367828879],[53.29023599624634,56.878682606849225],[53.28751087188721,56.87742803605432],[53.28588008880615,56.876396207958884],[53.28397035598755,56.875329173952906],[53.28201770782471,56.87426210949167],[53.28019380569458,56.873206741058],[53.27789783477783,56.87171744825589],[53.27596664428711,56.870368824855184],[53.275344371795654,56.869020152812844],[53.26753377914429,56.869993547084384],[53.263885974884026,56.87043918901793],[53.26000213623047,56.870837916772395],[53.25644016265869,56.87129527572575],[53.25828552246093,56.879749545141955],[53.25841426849365,56.8822819360832]]]}}",
     * "sort": "50",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "87",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "Болото",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.229103088378906,56.8388323607198],[53.227128982543945,56.84455970562897],[53.226871490478516,56.84563935281034],[53.22609901428223,56.84643733288495],[53.228759765625,56.84629651410805],[53.23073387145996,56.8462026346291],[53.23305130004883,56.8462026346291],[53.234853744506836,56.84535770872194],[53.24901580810547,56.835921406988156],[53.269615173339844,56.83193021547418],[53.275880813598626,56.82845518469092],[53.2642936706543,56.82474499865309],[53.24051856994629,56.8187327778105],[53.22120666503906,56.81596119487891],[53.20455551147461,56.81365921592482],[53.206443786621094,56.81638399196758],[53.20850372314453,56.819766196936506],[53.20764541625976,56.8254964584162],[53.20747375488281,56.829535296362046],[53.20575714111328,56.83601531225413],[53.20575714111328,56.837517764479735],[53.22549819946289,56.83864456408303],[53.229103088378906,56.8388323607198]]]}}",
     * "sort": "50",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "51",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "буммаш",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.227729797363274,56.88518928447419],[53.24420928955078,56.888471502777286],[53.25777053833008,56.890440695386],[53.25897216796875,56.88603331100045],[53.258113861083984,56.88049990108558],[53.25725555419922,56.87233897136202],[53.24455261230469,56.87215134283375],[53.22893142700195,56.87215134283375],[53.22326660156249,56.87205752821654],[53.22257995605469,56.878061189194725],[53.227729797363274,56.88518928447419]]]}}",
     * "sort": "40",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "50",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "восточка",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.22554111480713,56.87201062081965],[53.233609199523926,56.871940259613986],[53.2430076599121,56.871940259613986],[53.24978828430176,56.871987167099135],[53.25755596160889,56.87128354864297],[53.26515197753906,56.87027500576558],[53.27523708343505,56.86910224728399],[53.27643871307373,56.86736649723102],[53.27798366546631,56.86267487773748],[53.27888488769531,56.8580061321979],[53.27952861785889,56.853430665520484],[53.27982902526855,56.84897198026975],[53.27613830566406,56.849277065139304],[53.27240467071533,56.84932400105233],[53.26673984527588,56.8491597250992],[53.26420783996581,56.848913309817405],[53.261568546295166,56.84873729790846],[53.25583934783935,56.84832660023491],[53.253071308135986,56.848220991533225],[53.247385025024414,56.84709448017166],[53.24397325515747,56.846765907970614],[53.234102725982666,56.84605007997358],[53.22669982910156,56.8453811791436],[53.22627067565918,56.8464490677591],[53.22378158569335,56.85317254559932],[53.220884799957275,56.85985962353991],[53.220970630645745,56.860798065148344],[53.22127103805542,56.861677832774696],[53.222880363464355,56.863765731779736],[53.22330951690674,56.86464542960775],[53.22305202484131,56.86829302248672],[53.222858905792236,56.87011082179233],[53.22273015975952,56.871987167099135],[53.22554111480713,56.87201062081965]]]}}",
     * "sort": "30",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "49",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "центр",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.196659088134766,56.86032884728699],[53.220863342285156,56.86145496026633],[53.22275161743164,56.85591791169062],[53.22597026824951,56.8473995803469],[53.22897434234619,56.838808886191714],[53.216185569763184,56.83786989299879],[53.20301055908203,56.83714215707566],[53.20142269134521,56.836883924799494],[53.2007360458374,56.8415083604382],[53.19803237915039,56.84139099604034],[53.19756031036377,56.8433391974011],[53.1961441040039,56.84455970562897],[53.19305419921875,56.85385303973245],[53.19099426269531,56.8562933307125],[53.196659088134766,56.86032884728699]]]}}",
     * "sort": "20",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "48",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "металлург",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.16953659057617,56.87740458573206],[53.17502975463867,56.88406388611977],[53.1818962097168,56.889127911844376],[53.190221786499016,56.88804951943078],[53.188934326171875,56.88561130012011],[53.1998348236084,56.884392130807164],[53.20146560668945,56.88425145486556],[53.20258140563965,56.88425145486556],[53.205928802490234,56.88500172043518],[53.208932876586914,56.88542373819915],[53.21159362792968,56.885845751197536],[53.229618072509766,56.889878079515874],[53.22893142700195,56.88781508217916],[53.226871490478516,56.884157670610335],[53.225154876708984,56.881531615913396],[53.22257995605469,56.87787358938221],[53.22257995605469,56.874778056550454],[53.222408294677734,56.870650280711686],[53.22326660156249,56.86783562681172],[53.22223663330078,56.86323790315132],[53.220863342285156,56.86201800404304],[53.19562911987305,56.86070422204693],[53.18635940551758,56.86126727712404],[53.17863464355469,56.86117343519976],[53.1764030456543,56.86483309579991],[53.17176818847656,56.867929452021656],[53.169021606445305,56.871494635570066],[53.16953659057617,56.87740458573206]]]}}",
     * "sort": "10",
     * "type": "city",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "71",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "аэропрт тест",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.394756317138665,56.826811476726405],[53.44865798950195,56.848127116881514],[53.47200393676758,56.85328987305697],[53.4814453125,56.85328987305697],[53.49071502685547,56.82014197864235],[53.45277786254883,56.814222978948195],[53.41896057128906,56.81328336919729],[53.392696380615234,56.81459881625187],[53.39424133300781,56.82737504186915],[53.394756317138665,56.826811476726405]]]}}",
     * "sort": null,
     * "type": "airport",
     * "is_active": "1"
     * },
     * {
     * "parking_id": "95",
     * "tenant_id": "68",
     * "city_id": "26068",
     * "name": "Базовая парковка (границы города)",
     * "polygon":
     *     "{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[53.169708251953125,56.88068748771793],[53.218116760253906,56.88537684757183],[53.2591438293457,56.89109706986074],[53.29004287719726,56.89634765056862],[53.328838348388665,56.90300357598824],[53.274421691894524,56.87721698262444],[53.269615173339844,56.868304750507384],[53.30686569213867,56.869149158329456],[53.310813903808594,56.85713800973836],[53.31098556518555,56.84765774009097],[53.304290771484375,56.83779946755993],[53.282833099365234,56.829535296362046],[53.25244903564453,56.82314809660043],[53.232364654541016,56.819766196936506],[53.206443786621094,56.814222978948195],[53.183441162109375,56.79937439008826],[53.147735595703125,56.80012635875],[53.115806579589844,56.81854488036145],[53.10619354248047,56.84258809563361],[53.10859680175781,56.85741956517573],[53.169708251953125,56.88068748771793]]]}}",
     * "sort": null,
     * "type": "basePolygon",
     * "is_active": "1"
     * }
     * ],
     * "parking_name": null,
     * "inside": null,
     * "error": "Адрес вне парковок."
     * }
     *
     *
     */
    public function actionGet_parking_by_coords()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tenantId                    = Yii::$app->request->get('tenant_id');
        $cityId                      = Yii::$app->request->get('city_id');
        $lat                         = Yii::$app->request->get('lat');
        $lon                         = Yii::$app->request->get('lon');
        $jsonFormat                  = Yii::$app->request->get('json_format');

        \Yii::$app->geoservice->setTenantId($tenantId);
        \Yii::$app->geoservice->setCityId($cityId);

        \Yii::$app->parkingService->setTenantId($tenantId);
        \Yii::$app->parkingService->setCityId($cityId);


        $taxiRouteAnalyzer = new \app\modules\v1\components\routeAnalyzerNew\TaxiRouteAnalyzer();
        $parkingInfo       = $taxiRouteAnalyzer->getParkingByCoords($tenantId, $cityId, $lat, $lon,
            $jsonFormat);
        $result            = [
            "code"   => "0",
            "info"   => "OK",
            "result" => $parkingInfo,
        ];

        return $result;
    }

    /**
     * Определение доп опций для тарифа
     *
     * @param type $tariff_id
     * @param type $date 19.04.2016 11:35:00
     *
     * @return array
     */
    public function actionAdd_options()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tariff_id                   = Yii::$app->request->post('tariff_id');
        $date                        = Yii::$app->request->post('date');

        $taxiRouteAnalyzer           = new \app\modules\v1\components\routeAnalyzerNew\TaxiRouteAnalyzer();
        $add_options                 = $taxiRouteAnalyzer->getAddOptions($tariff_id, $date);

        $result                      = [
            "code"   => "0",
            "info"   => "OK",
            "result" => $add_options,
        ];

        return $result;
    }

    public function actionDistance_matrix(){
        try {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $params = $request->bodyParams;


            \Yii::$app->get('geoservice')->setTenantId($params['tenant_id']);
            \Yii::$app->get('geoservice')->setCityId($params['city_id']);


            $origins = isset($params['origins']) ? unserialize($params['origins']) : [];
            $destinations = isset($params['destinations']) ? unserialize($params['destinations']) : [];


            $distanceRezult = \Yii::$app->geoservice->getmatrixDistance([
                'tenant_id' => $params['tenant_id'],
                'tenant_login' => $params['tenant_login'],
                'origins' => $origins,
                'destinations' => $destinations,
            ]);


            return [
                "code" => "0",
                "info" => "OK",
                "result" => serialize($distanceRezult),
            ];
        }catch (\Exception $exception) {
            Yii::$app->get('logger')->error('Внутренняя ошибка: {message}', ['message' => $exception->getMessage()]);

            return [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
        }


    }
    /**
     * Анализатор маршрута
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAnalyze_route()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        if ($requestId = Yii::$app->request->headers->get('request-id', false)) {
            Yii::$app->get('logger')->setId($requestId);
        }

        /** @var CalculateRequest $request */
        $request             = Yii::createObject(AnalyzeRouteRequest::className());
        $request->attributes = Yii::$app->request->post();


        if (!$request->validate()) {
            Yii::$app->get('logger')->error('Ошибка валидации {errors}',
                ['errors' => json_encode($request->firstErrors)]);

            return [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
        }

        $key  = json_encode($request->attributes);
        $hash = 'route_analyze_' . hash('md5', $key);

//        if ($result = Yii::$app->cache->get($hash)) {
//            Yii::$app->get('logger')->log('Беру данные из кеша. Ключ: {key}', ['key' => $hash]);
//            Yii::$app->get('logger')->log('Response: {response}',
//                ['response' => json_encode($result)]);
//
//            return [
//                "code"   => "0",
//                "info"   => "OK",
//                "result" => $result,
//            ];
//        }

        \Yii::$app->get('geoservice')->setTenantId($request->tenant_id);
        \Yii::$app->get('geoservice')->setCityId($request->city_id);

        \Yii::$app->get('parkingService')->setTenantId($request->tenant_id);
        \Yii::$app->get('parkingService')->setCityId($request->city_id);

        try {
            /** @var RouteAnalyzer $calculateRoute */
            $calculateRoute = Yii::createObject([
                'class'        => RouteAnalyzer::className(),
                'tenantId'     => $request->tenant_id,
                'cityId'       => $request->city_id,
                'address'      => $request->address_array,
                'additional'   => $request->additional,
                'tariffId'     => (array)$request->tariff_id,
                'orderTime'    => $request->order_time,
                'lang'         => $request->lang,
                'geocoderType' => $request->geocoder_type,
                'clientId'     => $request->client_id,

            ]);
            $result         = $calculateRoute->execute();
            $result         = BaseFormatter::formatter(ArrayHelper::getValue($result, '0'));

            Yii::$app->cache->set($hash, $result, 60);
            Yii::$app->get('logger')->log('Записываю ответ в кеш на {duration} сек. Ключ: {key}',
                ['duration' => 60, 'key' => $hash]);

            Yii::$app->get('logger')->log('Response: {response}', ['response' => json_encode($result)]);

            return [
                "code"   => "0",
                "info"   => "OK",
                "result" => $result,
            ];

        } catch (\Exception $exception) {
            Yii::$app->get('logger')->error('Внутренняя ошибка: {message}', ['message' => $exception->getMessage()]);

            return [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
        }
    }


    /**
     * Анализатор маршрута
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCalculate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($requestId = Yii::$app->request->headers->get('request-id', false)) {
            Yii::$app->get('logger')->setId($requestId);
        }

        /** @var CalculateRequest $request */
        $request             = Yii::createObject(CalculateRequest::className());
        $request->attributes = Yii::$app->request->post();

        if (!$request->validate()) {
            Yii::$app->get('logger')->error('Ошибка валидации {errors}',
                ['errors' => json_encode($request->firstErrors)]);

            return [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
        }

        $key  = json_encode($request->attributes);
        $hash = 'route_analyze_' . hash('md5', $key);


        if ($result = Yii::$app->cache->get($hash)) {
            Yii::$app->get('logger')->log('Беру данные из кеша. Ключ: {key}', ['key' => $hash]);
            Yii::$app->get('logger')->log('Response: {response}',
                ['response' => json_encode(array_map(function ($item) {
                    return Filter::filter($item);
                }, $result))]);

            return [
                "code"   => "0",
                "info"   => "OK",
                "result" => ManyFormatter::formatter($result),
            ];
        }

        \Yii::$app->get('geoservice')->setTenantId($request->tenant_id);
        \Yii::$app->get('geoservice')->setCityId($request->city_id);

        \Yii::$app->get('parkingService')->setTenantId($request->tenant_id);
        \Yii::$app->get('parkingService')->setCityId($request->city_id);



        try {
            /** @var RouteAnalyzer $calculateRoute */
            $calculateRoute = Yii::createObject([
                'class'        => RouteAnalyzer::className(),
                'tenantId'     => $request->tenant_id,
                'cityId'       => $request->city_id,
                'address'      => $request->address_array,
                'additional'   => $request->additional,
                'tariffId'     => $request->tariff_id,
                'orderTime'    => $request->order_time,
                'lang'         => $request->lang,
                'geocoderType' => $request->geocoder_type,
                'clientId'     => $request->client_id,

            ]);
            $result         = $calculateRoute->execute();

            Yii::$app->cache->set($hash, $result, 60);
            Yii::$app->get('logger')->log('Записываю ответ в кеш на {duration} сек. Ключ: {key}',
                ['duration' => 60, 'key' => $hash]);

            Yii::$app->get('logger')->log('Response: {response}',
                [
                    'response' => json_encode(array_map(function ($item) {
                        return Filter::filter($item);
                    }, $result)),
                ]);

            return [
                "code"   => "0",
                "info"   => "OK",
                //                "result" => BaseFormatter::formatter(ArrayHelper::getValue($result, '0')),
                "result" => ManyFormatter::formatter($result),
            ];

        } catch (\Exception $exception) {
            Yii::$app->get('logger')->error('Внутренняя ошибка: {message}', ['message' => $exception->getMessage()]);

            return [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];
        }
    }

    /**
     * Форматирование парковок
     * Тип запроса - POST
     *
     * @params string parkings
     * @params int reverse 1|0
     */
    public function actionFormat_parkings()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $parkingArr                  = Yii::$app->request->post('parkings');
        $reverse                     = Yii::$app->request->post('reverse');
        $taxiRouteAnalyzer           = new \app\modules\v1\components\routeAnalyzerNew\TaxiRouteAnalyzer();
        $parkingArr                  = urldecode($parkingArr);
        $parkingArr                  = json_decode($parkingArr, true);
        try {
            $parkings = $taxiRouteAnalyzer->formatParkings($parkingArr, $reverse);
            $result   = [
                "code"   => "0",
                "info"   => "OK",
                "result" => $parkings,
            ];

            return $result;
        } catch (\Exception $exc) {
            Yii::error($exc);
            $result = [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];

            return $result;
        }
    }

    /**
     * Определение принадлежности точки парковке
     * Тип запроса - POST
     *
     * @params string $parkings
     * @params string $lat
     * @params string $lon
     */
    public function actionFind_point_location_in_parking()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $parkings                    = Yii::$app->request->post('parkings');
        $lat                         = Yii::$app->request->post('lat');
        $lon                         = Yii::$app->request->post('lon');
        $taxiRouteAnalyzer           = new \app\modules\v1\components\routeAnalyzerNew\TaxiRouteAnalyzer();
        $parkings                    = urldecode($parkings);
        $parkings                    = json_decode($parkings, true);
        try {
            $location = $taxiRouteAnalyzer->findPointLocationInParking($lat, $lon, $parkings);
            $result   = [
                "code"   => "0",
                "info"   => "OK",
                "result" => $location,
            ];

            return $result;
        } catch (\Exception $exc) {
            Yii::error($exc);
            $result = [
                "code"   => "1",
                "info"   => "INTERNAL_ERROR",
                "result" => null,
            ];

            return $result;
        }
    }

}
