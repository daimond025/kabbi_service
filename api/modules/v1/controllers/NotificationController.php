<?php

namespace app\modules\v1\controllers;

use api\modules\v1\requests\SendSmsNotificationRequest;
use api\modules\v1\requests\ValidateRequestException;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use api\common\models\Tenant;
use api\common\models\OrderStatus;
use api\common\models\Client;
use api\modules\v1\requests\SendSmsNotificationForPointRecipientRequest;
use app\modules\v1\models\SmsModel;
use yii\web\Response;
use app\modules\v1\services\RecipientService;
use yii\base\Module;

/**
 * Middlware для работы с уведомлениями (push,sms,call), обновление бейджиков заказов,
 *
 * @author Sergey K.
 */
class NotificationController extends Controller
{

    public $recipientService;

    public function __construct($id, Module $module, RecipientService $recipientService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->recipientService = $recipientService;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'ping'                             => ['get'],
                    'update_dispatcher_order_counters' => ['get'],
                    'send_sms_notification_for_client' => ['get'],
                    'send_text_for_client' => ['post'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Оновления счетчиков заказов у диспетчера. Вызывается nodejs api
     * @params status_id
     * @params tenant_id
     * @params city_id
     */ public function actionUpdate_dispatcher_order_counters()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tenantId = Yii::$app->request->get('tenant_id');
        $statusId = Yii::$app->request->get('status_id');
        $cityId = Yii::$app->request->get('city_id');
        $inc = (string) Yii::$app->request->get('inc');
        if (empty($inc)) {
            $inc = "1";
        }
        OrderStatus::updateDispatcherOrderCounters($statusId, $tenantId, $cityId, $inc);
        $response = array(
            'result' => 1,
        );
        return $response;
    }

    /**
     * Обновления счетчиков заказов у клиента.Вызывается nodejs api
     * @params tenant_id
     * @params client_id
     * @params status_id
     */
    public function actionUpdate_client_order_counters()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $statusId = Yii::$app->request->get('status_id');
        $clientId = Yii::$app->request->get('client_id');
        $response = array(
            'result' => (int)Client::updateClientOrderCounters($clientId, $statusId),
        );
        return $response;
    }

    /**
     * @throws ValidateRequestException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSend_notification_for_points_recipient()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = new SendSmsNotificationForPointRecipientRequest();
        $request->load(Yii::$app->request->get(), '');

        if (!$request->validate()) {
            throw new ValidateRequestException($request->getErrorsMap());
        }

        $this->recipientService->sendNotificationForRecipients(
            $request->tenant_id,
            $request->order_id,
            $request->status_id
        );
    }

    /**
     * @return int
     * @throws ValidateRequestException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSend_sms_notification_for_client()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SendSmsNotificationRequest $request */
        $request = Yii::createObject(SendSmsNotificationRequest::class);
        $request->load(Yii::$app->request->get(), '');

        if (!$request->validate()) {
            throw new ValidateRequestException($request->getErrorsMap());
        }



        return (integer) SmsModel::sendSmsNotification(
            $request->tenant_id,
            $request->order_id,
            $request->status_id,
            SmsModel::TO_CLIENT
        );
    }

    /**
     * @throws ValidateRequestException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSend_sms_notification_for_client_passenger()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SendSmsNotificationRequest $request */
        $request = Yii::createObject(SendSmsNotificationRequest::class);
        $request->load(Yii::$app->request->get(), '');

        if (!$request->validate()) {
            throw new ValidateRequestException($request->getErrorsMap());
        }

        return (integer) SmsModel::sendSmsNotification(
            $request->tenant_id,
            $request->order_id,
            $request->status_id,
            SmsModel::TO_PASSENGER
        );
    }


    public function actionSend_password_for_client()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        $tenantId = Yii::$app->request->get('tenant_id');
        $phone = Yii::$app->request->get('phone');
        $cityId = Yii::$app->request->get('city_id');
        $positionId = Yii::$app->request->get('position_id', 1);

        $sendResult = SmsModel::sendPassword($tenantId, $phone, $cityId, $positionId);

        return [
            'result' => $sendResult ? 1 : 0,
        ];
    }

    //TODO массовая отправка смс сообщений всем клиентам
    public function actionSend_text_for_client(){

        $response = new Response();
        $response->setStatusCode(200);
        $response->format = Response::FORMAT_JSON;

        $response->data = ['rezult' => 1];
        $response->send();



        $phone = Yii::$app->request->post('phone');
        $text = Yii::$app->request->post('text');
        $tenantId= Yii::$app->request->post('tenant_id');

        $sendResult = SmsModel::sendTextClient($tenantId, $phone,$text);
    }

    /**
     * Функция перевода на другой язык
     * @param type $category
     * @param type $message
     * @param type $params
     * @param type $language
     * @return type
     */
    private function t($category, $message, $params = [], $language = null)
    {
        return Yii::t($category, $message, $params, $language);
    }

    /**
     * Получить язык
     * @return type
     */
    private function getLang()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers = $headersObject->toArray();
        $lang = isset($headers['lang']['0']) ? $headers['lang']['0'] : null;
        return $lang;
    }

    /**
     * Получение ID тенанта по его логину, результат кэшим на сутки
     * @param type $tenantLogin
     * @return type
     */
    private function getTenantId($tenantLogin)
    {
        $tenantData = Tenant::getDb()->cache(function ($db) use ($tenantLogin) {
            return Tenant::find()
                    ->where(['domain' => $tenantLogin])
                    ->asArray()
                    ->one();
        }, 60 * 60 * 24);
        if (!empty($tenantData)) {
            return $tenantData['tenant_id'];
        }
        return null;
    }

}