<?php

namespace app\modules\v1\controllers;

use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    public function actionVersion()
    {
        $format = \Yii::$app->request->get('format', 'json');
        $version = \Yii::$app->params['version'];
        switch ($format) {

            case 'text':
                return 'v' . $version;

            default:
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'version' => $version
                ];
        }
    }
}