<?php

namespace api\modules\v1\exceptions;

use yii\web\HttpException;

class ErrorsException extends HttpException
{
    private $errors = [];


    public function __construct(array $errors, $status, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($status, $message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}