<?php

namespace app\modules\v1\components;


use api\common\models\DefaultSmsTemplate;
use api\common\models\DefaultSmsTemplateCourier;
use api\common\models\SmsTemplate;
use api\common\models\SmsTemplateCourier;
use yii\helpers\ArrayHelper;

class SmsTemplateService
{
    public function getSmsMessage($tenantId, $cityId, $clientPhone, $type, $positionId, $orderData)
    {
        $classId = ArrayHelper::getValue($orderData, 'tariff.class_id');

        if ($classId !== null && ArrayHelper::isIn($classId, ['26', '27', '28'])) {
            $default = DefaultSmsTemplateCourier::find()
                ->select(['text', 'params'])
                ->where([
                    'type'        => $type,
                    'position_id' => $positionId,
                ])
                ->one();

            $model = SmsTemplateCourier::find()
                ->select('text')
                ->where([
                    'tenant_id'   => $tenantId,
                    'city_id'     => $cityId,
                    'position_id' => $positionId,
                    'type'        => $type,
                ])
                ->one();
        } else {
            $default = DefaultSmsTemplate::find()
                ->select(['text', 'params'])
                ->where([
                    'type'        => $type,
                    'position_id' => $positionId,
                ])
                ->one();

            $model = SmsTemplate::find()
                ->select('text')
                ->where([
                    'tenant_id'   => $tenantId,
                    'city_id'     => $cityId,
                    'position_id' => $positionId,
                    'type'        => $type,
                ])
                ->one();
        }

        if (!$default) {
            return null;
        }

        $template = $model ? $model->text : $default->text;


        $params   = explode(';', $default->params);
        $params   = array_map(function ($item) {
            return trim($item);
        }, $params);

        $message = new MessageComponent([
            'template'    => $template,
            'params'      => $params,
            'orderData'   => $orderData,
            'clientPhone' => $clientPhone,
        ]);

        return (string)$message->replace();
    }
}
