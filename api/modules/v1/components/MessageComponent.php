<?php

namespace app\modules\v1\components;


use api\common\models\Currency;
use api\common\models\OrderDetailCost;
use api\common\models\Tenant;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class MessageComponent extends Object
{

    public $template;
    public $params;
    public $orderData;
    public $clientPhone;

    const PARAM_TAXI_NAME = '#TAXI_NAME#';

    const PARAM_CAR_MODEL = '#MODEL#';
    const PARAM_CAR_BRAND = '#BRAND#';
    const PARAM_CAR_COLOR = '#COLOR#';
    const PARAM_CAR_NUMBER = '#NUMBER#';

    const PARAM_TIME_TO_CLIENT = '#TIME#';
    const PARAM_WORKER_PHONE = '#PHONE#';

    const PARAM_ADDRESS = '#ADDRESS#';
    const PARAM_ORDER_TIME = '#DATETIME#';
    const PARAM_PRICE = '#PRICE#';
    const PARAM_CURRENCY = '#CURRENCY#';

    const PARAM_TENANT_COMPANY_NAME = '#COMPANY_NAME#';
    const PARAM_TENANT_COMPANY_PHONE = '#COMPANY_PHONE#';

    const PARAM_CODE = '#CODE#';

    public function __toString()
    {
        return (string)$this->template;
    }

    public function replace()
    {
        return $this->replaceTaxiName()
            ->replaceAddress()
            ->replaceOrderTime()
            ->replacePrice()
            ->replaceCurrency()
            ->replaceTimeToClient()
            ->replaceWorkerPhone()
            ->replaceCarModel()
            ->replaceCarBrand()
            ->replaceCarColor()
            ->replaceCarNumber()
            ->replaceTenantCompanyName()
            ->replaceTenantCompanyPhone()
            ->replaceCode();
    }

    public function replaceTaxiName()
    {
        if ($this->checkParam(self::PARAM_TAXI_NAME)) {

            $tenantId = ArrayHelper::getValue($this->orderData, 'tenant_id');
            if (!$tenantId) {
                return $this;
            }

            $tenant = Tenant::findOne($tenantId);
            if (!$tenant) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_TAXI_NAME, $tenant->company_name, $this->template);

        }

        return $this;
    }

    public function replaceCarModel()
    {
        if ($this->checkParam(self::PARAM_CAR_MODEL)) {

            $carModel = ArrayHelper::getValue($this->orderData, 'car.model');
            if ($carModel === null) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_CAR_MODEL, $carModel, $this->template);

        }

        return $this;
    }

    public function replaceCarBrand()
    {
        if ($this->checkParam(self::PARAM_CAR_BRAND)) {

            $carBrand = ArrayHelper::getValue($this->orderData, 'car.brand');
            if ($carBrand === null) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_CAR_BRAND, $carBrand, $this->template);

        }

        return $this;
    }

    public function replaceCarColor()
    {
        if ($this->checkParam(self::PARAM_CAR_COLOR)) {

            $carColor = ArrayHelper::getValue($this->orderData, 'car.color');
            $lang     = ArrayHelper::getValue($this->orderData, 'client.lang');

            if ($carColor === null) {
                return $this;
            }

            $carColor = \Yii::t('car', $carColor, [], $lang);

            $this->template = str_replace(self::PARAM_CAR_COLOR, $carColor, $this->template);

        }

        return $this;
    }

    public function replaceCarNumber()
    {
        if ($this->checkParam(self::PARAM_CAR_NUMBER)) {

            $carNumber = ArrayHelper::getValue($this->orderData, 'car.gos_number');
            if ($carNumber === null) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_CAR_NUMBER, $carNumber, $this->template);

        }

        return $this;
    }

    public function replaceTimeToClient()
    {
        if ($this->checkParam(self::PARAM_TIME_TO_CLIENT)) {

            $timeToClient = ArrayHelper::getValue($this->orderData, 'time_to_client');
            if ($timeToClient === null) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_TIME_TO_CLIENT, $timeToClient, $this->template);

        }

        return $this;
    }

    public function replaceWorkerPhone()
    {
        if ($this->checkParam(self::PARAM_WORKER_PHONE)) {

            $workerPhone = ArrayHelper::getValue($this->orderData, 'worker.phone');
            if ($workerPhone === null) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_WORKER_PHONE, $workerPhone, $this->template);

        }

        return $this;
    }

    public function replaceAddress()
    {
        if ($this->checkParam(self::PARAM_ADDRESS)) {

            $address = ArrayHelper::getValue($this->orderData, 'address');
            if ($address === null) {
                return $this;
            }

            $address        = unserialize($address);
            $addressToArray = [];

            if (!empty($address['A'])) {
                $addressToArray[] = $address['A']['city'];

                if (!empty($address['A']['street'])) {
                    $addressToArray[] = $address['A']['street'];
                }

                if (!empty($address['A']['house'])) {
                    $addressToArray[] = $address['A']['house'];
                }

                if (!empty($address['A']['porch'])) {
                    $addressToArray[] = $address['A']['porch'];
                }

            }

            $addressToString = implode(', ', $addressToArray);

            $this->template = str_replace(self::PARAM_ADDRESS, $addressToString, $this->template);

        }

        return $this;
    }

    public function replaceOrderTime()
    {
        if ($this->checkParam(self::PARAM_ORDER_TIME)) {

            $orderTime = ArrayHelper::getValue($this->orderData, 'order_time');
            if ($orderTime === null) {
                return $this;
            }

            $orderTime = \Yii::$app->formatter->asDatetime($orderTime, 'php:H:i d.m.Y');

            $this->template = str_replace(self::PARAM_ORDER_TIME, $orderTime, $this->template);

        }

        return $this;
    }

    public function replacePrice()
    {
        if ($this->checkParam(self::PARAM_PRICE)) {

            $orderId = ArrayHelper::getValue($this->orderData, 'order_id');
            if (!$orderId) {
                return $this;
            }

            /** @var OrderDetailCost $orderDetail */
            $orderDetail = OrderDetailCost::find()
                ->where(["order_id" => $orderId])
                ->one();
            if (!$orderDetail) {
                return $this;
            }

            $this->template = str_replace(self::PARAM_PRICE, $orderDetail->summary_cost, $this->template);

        }

        return $this;
    }

    public function replaceCurrency()
    {
        if ($this->checkParam(self::PARAM_CURRENCY)) {

            $currencyId = ArrayHelper::getValue($this->orderData, 'currency_id');
            if (!$currencyId) {
                return $this;
            }

            $currency = Currency::getSymbol((int)$currencyId);

            $this->template = str_replace(self::PARAM_CURRENCY, $currency, $this->template);

        }

        return $this;
    }

    public function replaceTenantCompanyName()
    {
        if ($this->checkParam(self::PARAM_TENANT_COMPANY_NAME)) {
            $companyName    = ArrayHelper::getValue($this->orderData, 'worker.tenant_company.name', '');
            $this->template = str_replace(self::PARAM_TENANT_COMPANY_NAME, $companyName, $this->template);
        }

        return $this;
    }

    public function replaceTenantCompanyPhone()
    {
        if ($this->checkParam(self::PARAM_TENANT_COMPANY_PHONE)) {
            $companyPhone   = ArrayHelper::getValue($this->orderData, 'worker.tenant_company.phone', '');
            $this->template = str_replace(self::PARAM_TENANT_COMPANY_PHONE, $companyPhone, $this->template);
        }

        return $this;
    }

    public function replaceCode()
    {
        $code = ClientService::getCode(ArrayHelper::getValue($this->orderData, 'tenant_id'), $this->clientPhone);

        if ($this->checkParam(self::PARAM_CODE) && $code) {
            $this->template = str_replace(self::PARAM_CODE, $code, $this->template);
        }

        return $this;
    }

    public function checkParam($param)
    {
        return ArrayHelper::isIn($param, $this->params) && strpos($this->template, $param) !== false;
    }
}