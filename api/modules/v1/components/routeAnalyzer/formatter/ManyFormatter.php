<?php

namespace app\modules\v1\components\routeAnalyzer\formatter;

use app\modules\v1\components\routeAnalyzer\route\Point;
use yii\helpers\ArrayHelper;

class ManyFormatter implements FormatterInterface
{
    public static function formatter($data)
    {
        $first = reset($data);


        $result = [
            'additionalParking'   => (array)ArrayHelper::getValue($first, 'district'),
            'cityDistance'        => (float)(ceil($first->route->getCityDistance() / 100) / 10),
            'cityTime'            => (int)ceil($first->route->getCityTime() / 60),
            'finishPointLocation' => $first->route->getFinishPointType() == Point::TYPE_CITY ? self::POINT_IN : self::POINT_OUT,
            'outCityDistance'     => (float)(ceil($first->route->getTrackDistance() / 100) / 10),
            'outCityTime'         => (int)ceil($first->route->getTrackTime() / 60),
            'summaryDistance'     => (float)(ceil($first->route->getSummaryDistance() / 100) / 10),
            'summaryTime'         => (int)ceil($first->route->getSummaryTime() / 60),
            'routeLine'           => (array)$first->route->getInterval(),
            'startPointLocation'  => $first->route->getStartPointType() == Point::TYPE_CITY ? self::POINT_IN : self::POINT_OUT,
        ];

        $result['tariffs'] = array_map(function ($item) {
            return [
                'tariffId'              => (int)ArrayHelper::getValue($item, 'tariffInfo.tariffType.tariff_id'),
                'cityCost'              => (float)ArrayHelper::getValue($item, 'cityCost'),
                'isFix'                 => (bool)ArrayHelper::getValue($item, 'isFix'),
                'isAirport'             => (bool)ArrayHelper::getValue($item, 'isStation'),
                'isStation'             => (bool)ArrayHelper::getValue($item, 'isStation'),
                'enableParkingRatio'    => (bool)ArrayHelper::getValue($item, 'enableParkingRatio'),
                'minPrice'              => (float)$item->getMinPrice(),
                'outCityCost'           => (float)ArrayHelper::getValue($item, 'trackCost'),
                'summaryCost'           => (float)ArrayHelper::getValue($item, 'summaryCost'),
                'summaryCostNoDiscount' => (float)ArrayHelper::getValue($item, 'summaryCostNoDiscount'),
                'discount'              => ArrayHelper::getValue($item, 'discount'),
                'additionalCost'        => (float)ArrayHelper::getValue($item, 'additionalCost'),
                //            '_dev'                => $data['_dev'],
            ];
        }, $data);

        return $result;
    }
}