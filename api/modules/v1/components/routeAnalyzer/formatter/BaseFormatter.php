<?php

namespace app\modules\v1\components\routeAnalyzer\formatter;

use app\modules\v1\components\routeAnalyzer\route\Point;
use yii\helpers\ArrayHelper;

class BaseFormatter implements FormatterInterface
{
    public static function formatter($data)
    {
        $result = [
            'additionalCost'        => (float)ArrayHelper::getValue($data, 'additionalCost'),
            'additionalParking'     => (array)ArrayHelper::getValue($data, 'district'),
            'cityCost'              => (float)ArrayHelper::getValue($data, 'cityCost'),
            'cityDistance'          => (float)(ceil($data->route->getCityDistance() / 100) / 10),
            'cityTime'              => (int)ceil($data->route->getCityTime() / 60),
            'isFix'                 => (bool)ArrayHelper::getValue($data, 'isFix'),
            'isAirport'             => (bool)ArrayHelper::getValue($data, 'isAirport'),
            'isStation'             => (bool)ArrayHelper::getValue($data, 'isStation'),
            'enableParkingRatio'    => (bool)ArrayHelper::getValue($data, 'enableParkingRatio'),
            'finishPointLocation'   => $data->route->getFinishPointType() == Point::TYPE_CITY ? self::POINT_IN : self::POINT_OUT,
            'minPrice'              => (float)$data->getMinPrice(),
            'outCityCost'           => (float)ArrayHelper::getValue($data, 'trackCost'),
            'outCityDistance'       => (float)(ceil($data->route->getTrackDistance() / 100) / 10),
            'outCityTime'           => (int)ceil($data->route->getTrackTime() / 60),
            'routeLine'             => (array)$data->route->getInterval(),
            'startPointLocation'    => $data->route->getStartPointType() == Point::TYPE_CITY ? self::POINT_IN : self::POINT_OUT,
            'summaryCost'           => (float)ArrayHelper::getValue($data, 'summaryCost'),
            'summaryCostNoDiscount' => (float)ArrayHelper::getValue($data, 'summaryCostNoDiscount'),
            'summaryDistance'       => (float)(ceil($data->route->getSummaryDistance() / 100) / 10),
            'summaryTime'           => (int)ceil($data->route->getSummaryTime() / 60),
            'tariffInfo'            => self::formatterTariffInfo(ArrayHelper::getValue($data, 'tariffInfo')),
            'discount'              => ArrayHelper::getValue($data, 'discount'),
            //            '_dev'                => $data['_dev'],
        ];

        return $result;
    }

    protected static function formatterTariffInfo($tariffInfo)
    {
        return [
            'tariffType'      => $tariffInfo->tariffType->type,
            'tariffDataCity'  => self::formatterTariffOption($tariffInfo->tariffDataCity),
            'tariffDataTrack' => self::formatterTariffOption($tariffInfo->tariffDataTrack),
            'isDay'           => (int)$tariffInfo->isDay,
        ];
    }

    protected static function formatterTariffOption($model)
    {
        return [
            'option_id'                   => $model->option_id,
            'tariff_id'                   => $model->type->tariff_id,
            'accrual'                     => $model->accrual,
            'area'                        => $model->area,
            'planting_price_day'          => $model->planting_price,
            'planting_price_night'        => 0,
            'planting_include_day'        => $model->planting_include,
            'planting_include_night'      => 0,
            'next_km_price_day'           => $model->next_km_price,
            'next_km_price_night'         => 0,
            'min_price_day'               => $model->min_price,
            'min_price_night'             => 0,
            'second_min_price_day'        => $model->second_min_price,
            'second_min_price_night'      => 0,
            'supply_price_day'            => $model->supply_price,
            'supply_price_night'          => 0,
            'wait_time_day'               => $model->wait_time,
            'wait_time_night'             => 0,
            'wait_driving_time_day'       => $model->wait_driving_time,
            'wait_driving_time_night'     => 0,
            'wait_price_day'              => $model->wait_price,
            'wait_price_night'            => 0,
            'speed_downtime_day'          => $model->speed_downtime,
            'speed_downtime_night'        => 0,
            'rounding_day'                => $model->rounding,
            'rounding_night'              => 0,
            'tariff_type'                 => null,
            'allow_day_night'             => 0,
            'start_day'                   => '00:00',
            'end_day'                     => '23:59',
            'active'                      => 1,
            'enabled_parking_ratio'       => $model->enabled_parking_ratio,
            'calculation_fix'             => $model->calculation_fix,
            'next_cost_unit_day'          => $model->next_cost_unit,
            'next_cost_unit_night'        => '1_km',
            'next_km_price_day_time'      => $model->next_km_price_time,
            'next_km_price_night_time'    => 0,
            'planting_include_day_time'   => $model->planting_include_time,
            'planting_include_night_time' => 0,
            'rounding_type_day'           => $model->rounding_type,
            'rounding_type_night'           => $model->rounding_type,
        ];
    }
}