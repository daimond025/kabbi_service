<?php

namespace app\modules\v1\components\routeAnalyzer\formatter;

interface FormatterInterface
{
    const POINT_IN = 'in';
    const POINT_OUT = 'out';

    public static function formatter($data);
}