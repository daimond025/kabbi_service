<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Point;
use app\modules\v1\components\routeAnalyzer\route\Route;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;
use app\modules\v1\components\routeAnalyzer\TariffInfo;

class CalculateAccrualMixed extends BaseCalculate
{
    // class BaseCalculate
    public  $prev = null;

    public function __construct($prev_calculate)
    {
        $this->prev = $prev_calculate;
    }

    /**
     * @inheritdoc
     */
    public function execute($route, $tariffInfo, $type)
    {
        $costAsTime = $this->calculateAsTime($route, $tariffInfo, $type);

        $costAsDistance = $this->calculateAsDistance($route, $tariffInfo, $type);

        return $costAsTime + $costAsDistance;
    }

    /**
     * @param Route $route
     * @param TariffInfo $tariffInfo
     * @param string $type
     *
     * @return float
     */
    protected function calculateAsTime($route, $tariffInfo, $type)
    {
        $time = 0;
        $cost = 0;
        /** @var RouteItem $routeItem */
        foreach ($route->items as $routeItem) {
            if ($this->isAirportItem($routeItem, $tariffInfo) || $this->isStationItem($routeItem, $tariffInfo)) {
                continue;
            }

            switch ($type) {
                case Point::TYPE_CITY:
                    $time += $routeItem->getCityTime();
                    break;
                case Point::TYPE_TRACK:
                    $time += $routeItem->getTrackTime();
                    break;
            }
        }

        if(!is_null($this->prev)){
            if( isset($this->prev->time_consider) && $this->prev->time_consider < 0){
                $time += $this->prev->time_consider;
                $time = $time < 0 ? 0 : $time;
            }
        }

        // Вычитаем "включено в стоимость посадки"
        if ($route->getStartPointType() === $type) {
            $plantingInclude = $tariffInfo->getPlantingIncludeTime($type);
            $plantingInclude = (int)$plantingInclude * 60;
            $time            -= $plantingInclude;
            if($time < 0){
                $this->time_consider = $time;
            }
            $time = $time < 0 ? 0 : $time;
        }

        $nextCostUnit = $tariffInfo->getNextCostUnit($type);
        $nextKmPrice  = $tariffInfo->getNextKmPriceTime($type);

        $roundingTime = ceil($time / $nextCostUnit);
        $cost = $roundingTime * $nextKmPrice;

        return $cost;
    }

    /**
     * @param Route $route
     * @param TariffInfo $tariffInfo
     * @param string $type
     *
     * @return float
     */
    protected function calculateAsDistance($route, $tariffInfo, $type)
    {
        $distance = 0;

        /** @var RouteItem $routeItem */
        foreach ($route->items as $routeItem) {
            if ($this->isAirportItem($routeItem, $tariffInfo) || $this->isStationItem($routeItem, $tariffInfo)) {
                continue;
            }
            switch ($type) {
                case Point::TYPE_CITY:
                    $distance += $routeItem->getCityDistance();
                    break;
                case Point::TYPE_TRACK:
                    $distance += $routeItem->getTrackDistance();
                    break;
            }
        }

        if(!is_null($this->prev)){
            if( isset($this->prev->distance_consider) && $this->prev->distance_consider < 0){
                $distance += $this->prev->distance_consider;
                $distance = $distance < 0 ? 0 : $distance;
            }

        }

        // Вычитаем "включено в стоимость посадки"
        if ($route->getStartPointType() === $type) {
            $plantingInclude = $tariffInfo->getPlantingInclude($type);
            $plantingInclude = (int)$plantingInclude * 1000;
            $distance        -= $plantingInclude;

            if($distance < 0){
                $this->distance_consider = $distance;
            }
            $distance        = $distance < 0 ? 0 : $distance;
            $this->cost      += $tariffInfo->getPlantingPrice($type);
        }
        $nextKmPrice = $tariffInfo->getNextKmPrice($type);
        $this->cost  += $distance * $nextKmPrice / 1000;

        return $this->cost;
         // old version
        //return (new CalculateAccrualDistance())->execute($route, $tariffInfo, $type);
    }
}