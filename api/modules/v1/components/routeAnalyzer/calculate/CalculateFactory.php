<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\models\client_tariff\TaxiTariffOption;

class CalculateFactory
{
    /**
     * @param $accrual
     *
     * @return BaseCalculate|null
     */
    public static function getFactory($accrual, $prev_calculate = null) {
        switch ($accrual) {
            case TaxiTariffOption::ACCRUAL_FIX:
                return new CalculateAccrualFix();
            case TaxiTariffOption::ACCRUAL_TIME:
                return new CalculateAccrualTime($prev_calculate);
            case TaxiTariffOption::ACCRUAL_DISTANCE:
                return new CalculateAccrualDistance($prev_calculate);
            case TaxiTariffOption::ACCRUAL_MIXED:
                return new CalculateAccrualMixed($prev_calculate);
            case TaxiTariffOption::ACCRUAL_INTERVAL:
                return new CalculateAccrualInterval();
        }

        return null;
    }
}