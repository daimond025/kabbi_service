<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualStation extends BaseCalculate
{
    public $isStation = [];

    public function execute($route, $tariffInfo, $type = null)
    {
        /** @var RouteItem $item */
        foreach ($route->items as $item) {
            if ($this->isAirportItem($item, $tariffInfo) || !$this->isStationItem($item, $tariffInfo)) {
                continue;
            }

            if (!$item->getIsAirport() && $item->finishPoint) {
                $typeId = $tariffInfo->tariffDataStation->type_id;

                $fromParkingId = $item->startPoint->getParkingId();
                $toParkingId   = $item->finishPoint->getParkingId();

                $cost = \Yii::$app->get('parkingService')->getFix($typeId, $fromParkingId, $toParkingId);
                if ((int)$cost === 0) {
                    $this->isStation[] = false;
                    continue;
                }

                $this->isStation[] = true;
                $this->cost        += $cost;
            } else {
                $this->isStation[] = false;
            }
        }

        return $this->cost;
    }

    public function getIsStation()
    {
        foreach ($this->isStation as $item) {
            if (!$item) {
                return false;
            }
        }

        if (count($this->isStation) < 1) {
            return false;
        }

        return true;
    }
}