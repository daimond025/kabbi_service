<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Route;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;
use app\modules\v1\components\routeAnalyzer\TariffInfo;
use yii\base\Object;

abstract class BaseCalculate extends Object
{
    public $distance_consider = 0;
    public $time_consider = 0;
    protected $cost = 0;

    public $class_calculate = null;

    /**
     * @param Route      $route
     * @param TariffInfo $tariffInfo
     * @param string     $type
     *
     * @return float
     */
    public function execute($route, $tariffInfo, $type)
    {

    }

    /**
     * @param RouteItem  $routeItem
     * @param TariffInfo $tariffInfo
     *
     * @return bool
     */
    protected function isAirportItem($routeItem, $tariffInfo)
    {
        if ($tariffInfo->tariffDataAirport && $routeItem->getIsAirport()) {

            $type_id       = $tariffInfo->tariffDataAirport->type_id;
            $fromParkingId = $routeItem->startPoint->getParkingId();
            $toParkingId   = $routeItem->finishPoint->getParkingId();

            $cost = \Yii::$app->get('parkingService')->getFix($type_id, $fromParkingId, $toParkingId);

            if ($cost === (float)0) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param RouteItem  $routeItem
     * @param TariffInfo $tariffInfo
     *
     * @return bool
     */
    protected function isStationItem($routeItem, $tariffInfo)
    {
        if ($tariffInfo->tariffDataStation && $routeItem->getIsStation()) {

            $typeId        = $tariffInfo->tariffDataStation->type_id;
            $fromParkingId = $routeItem->startPoint->getParkingId();
            $toParkingId   = $routeItem->finishPoint->getParkingId();

            $cost = \Yii::$app->get('parkingService')->getFix($typeId, $fromParkingId, $toParkingId);

            if ($cost === (float)0) {
                return false;
            }

            return true;
        }

        return false;
    }

}
