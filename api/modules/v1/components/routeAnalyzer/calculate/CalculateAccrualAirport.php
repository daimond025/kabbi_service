<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualAirport extends BaseCalculate
{
    public $isAirport = [];

    public function execute($route, $tariffInfo, $type = null)
    {
        /** @var RouteItem $item */
        foreach ($route->items as $item) {
            if ($tariffInfo->tariffDataAirport && $item->getIsAirport() && $item->finishPoint) {
                $typeId = $tariffInfo->tariffDataAirport->type_id;

                $fromParkingId = $item->startPoint->getParkingId();
                $toParkingId   = $item->finishPoint->getParkingId();

                $cost = \Yii::$app->get('parkingService')->getFix($typeId, $fromParkingId, $toParkingId);

                if ((int)$cost == 0) {
                    $this->isAirport[] = false;
                    continue;
                }
                $this->isAirport[] = true;
                $this->cost        += $cost;

            } else {
                if (!$this->isStationItem($item, $tariffInfo)) {
                    $this->isAirport[] = false;
                }
            }
        }

        return $this->cost;
    }

    public function getIsAirport()
    {
        foreach ($this->isAirport as $item) {
            if (!$item) {
                return false;
            }
        }

        if (count($this->isAirport) < 1) {
            return false;
        }

        return true;
    }
}