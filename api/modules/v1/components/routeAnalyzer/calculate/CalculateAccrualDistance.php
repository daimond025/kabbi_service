<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Point;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualDistance extends BaseCalculate
{
    // class BaseCalculate
     public  $prev = null;

    public function __construct($prev_calculate)
    {
        $this->prev = $prev_calculate;
    }

    /**
     * @inheritdoc
     */
    public function execute($route, $tariffInfo, $type)
    {
        $distance = 0;

        /** @var RouteItem $routeItem */
        foreach ($route->items as $routeItem) {
            if ($this->isAirportItem($routeItem, $tariffInfo) || $this->isStationItem($routeItem, $tariffInfo)) {
                continue;
            }
            switch ($type) {
                case Point::TYPE_CITY:
                    $distance += $routeItem->getCityDistance();
                    break;
                case Point::TYPE_TRACK:
                    $distance += $routeItem->getTrackDistance();
                    break;
            }
        }

        if(!is_null($this->prev)){
            if( isset($this->prev->distance_consider) && $this->prev->distance_consider < 0){
                $distance += $this->prev->distance_consider;
                $distance = $distance < 0 ? 0 : $distance;
            }

        }


        // Вычитаем "включено в стоимость посадки"
        if ($route->getStartPointType() === $type) {
            $plantingInclude = $tariffInfo->getPlantingInclude($type);
            $plantingInclude = (int)$plantingInclude * 1000;
            $distance        -= $plantingInclude;

            if($distance < 0){
                $this->distance_consider = $distance;
            }
            $distance        = $distance < 0 ? 0 : $distance;
            $this->cost      += $tariffInfo->getPlantingPrice($type);
        }
        $nextKmPrice = $tariffInfo->getNextKmPrice($type);
        $this->cost  += $distance * $nextKmPrice / 1000;

        return $this->cost;
    }
}