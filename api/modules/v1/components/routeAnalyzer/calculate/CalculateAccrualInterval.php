<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Interval;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualInterval extends BaseCalculate
{

    public function execute($route, $tariffInfo, $type)
    {
        $nextKmPrice = $tariffInfo->getNextKmPrice($type);
        $nextKmPrice = unserialize($nextKmPrice);
        if(!empty($nextKmPrice)) {
            $firstInterval = [
                'interval' => [0, $nextKmPrice[0]['interval'][0]],
                'price' => 0,
            ];
            array_unshift($nextKmPrice, $firstInterval);
        }

        $currentDistance = 0;

        /** @var RouteItem $item */
        foreach ($route->items as $item) {
            if ($this->isAirportItem($item, $tariffInfo) || $this->isStationItem($item, $tariffInfo)) {
                continue;
            }
            /** @var Interval $interval */
            foreach ($item->intervals as $interval) {
                if($interval->startPoint->type === $type) {
                    $distance = $interval->distance;
                    $this->cost += $this->calculateOneInterval($currentDistance, $distance, $nextKmPrice);
                    $currentDistance += $distance;
                }
            }
        }

        if ($route->getStartPointType() === $type) {
            $this->cost      += $tariffInfo->getPlantingPrice($type);
        }

        return $this->cost;
    }

    protected function calculateOneInterval($start, $distance, $rules)
    {
        // До какого метра считаем
        $finish = $start + $distance;
        $cost   = 0;
        // Перебираем какждый отрезок интервала.
        foreach ($rules as $rule) {
            $s = $rule['interval'][0] * 1000;
            $f = isset($rule['interval'][1]) ? $rule['interval'][1] * 1000 : $finish;
            // Отрезок лежит левее
            if ($start > $f) {
                continue;
            }
            // Отрезок лежит правее
            if ($finish < $s) {
                break;
            }
            // Если отрезок подходит частично
            if ($start > $s) {
                $s = $start;
            }
            // Если отрезок подходит частично
            if ($finish < $f) {
                $f = $finish;
            }
            $cost += ($f - $s) * $rule['price'] / 1000;
        }

        return $cost;
    }
}