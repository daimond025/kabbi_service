<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Point;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualTime extends BaseCalculate
{
    // class BaseCalculate
    public  $prev = null;

    public function __construct($prev_calculate)
    {
        $this->prev = $prev_calculate;
    }
    /**
     * @inheritdoc
     */
    public function execute($route, $tariffInfo, $type)
    {
        $time = 0;

        /** @var RouteItem $routeItem */
        foreach ($route->items as $routeItem) {
            if ($this->isAirportItem($routeItem, $tariffInfo) || $this->isStationItem($routeItem, $tariffInfo)) {
                continue;
            }

            switch ($type) {
                case Point::TYPE_CITY:
                    $time += $routeItem->getCityTime();
                    break;
                case Point::TYPE_TRACK:
                    $time += $routeItem->getTrackTime();
                    break;
            }
        }

        // учитываем предыдущий диапазон - если
        if(!is_null($this->prev)){
            if( isset($this->prev->time_consider) && $this->prev->time_consider < 0){
                $time += $this->prev->time_consider;
                $time = $time < 0 ? 0 : $time;
            }
        }


        // Вычитаем "включено в стоимость посадки"
        if ($route->getStartPointType() === $type) {
            $plantingInclude = $tariffInfo->getPlantingInclude($type);
            $plantingInclude = (int)$plantingInclude * 60;
            $time            -= $plantingInclude;

            if($time < 0){
                $this->time_consider = $time;
            }
            $time            = $time < 0 ? 0 : $time;
            $this->cost      += $tariffInfo->getPlantingPrice($type);
        }

        $nextCostUnit = $tariffInfo->getNextCostUnit($type);
        $nextKmPrice  = $tariffInfo->getNextKmPrice($type);

        $roundingTime = ceil($time / $nextCostUnit);

        $this->cost += $roundingTime * $nextKmPrice;

        return $this->cost;
    }

}