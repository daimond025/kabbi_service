<?php

namespace app\modules\v1\components\routeAnalyzer\calculate;

use app\modules\v1\components\routeAnalyzer\route\Point;
use app\modules\v1\components\routeAnalyzer\route\RouteItem;

class CalculateAccrualFix extends BaseCalculate
{

    public function execute($route, $tariffInfo, $type)
    {
        /** @var RouteItem $item */
        foreach ($route->items as $item) {
            if ($this->isAirportItem($item, $tariffInfo) || $this->isStationItem($item, $tariffInfo)) {
                continue;
            }

            if ($item->startPoint->type !== $type && $item->finishPoint->type !== $type) {
                continue;
            }

            if ($type === Point::TYPE_CITY
                && ($item->startPoint->type === Point::TYPE_TRACK || $item->finishPoint->type === Point::TYPE_TRACK)) {
                continue;
            }

            $optionId      = null;

            $typeId = $tariffInfo->tariffDataCity->type_id;

            $fromParkingId = $item->startPoint->getParkingId();
            $toParkingId   = $item->finishPoint->getParkingId();
            if ($item->finishPoint->type === Point::TYPE_TRACK && $type !== Point::TYPE_TRACK) {
                $transitPoint = $item->getTransitPoint();
                $toParkingId  = $transitPoint ? $transitPoint->getParkingId() : null;
            }
            if ($item->startPoint->type === Point::TYPE_TRACK && $type !== Point::TYPE_TRACK) {
                $transitPoint  = $item->getTransitPoint(false);
                $fromParkingId = $transitPoint ? $transitPoint->getParkingId() : null;
            }

            if ($fromParkingId !== null && $toParkingId != null) {
                $this->cost += \Yii::$app->get('parkingService')->getFix($typeId, $fromParkingId, $toParkingId);
            }
        }

        if ($route->getStartPointType() === $type) {
            $this->cost += $tariffInfo->getPlantingPrice($type);
        }

        return $this->cost;
    }
}
