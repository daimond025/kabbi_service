<?php

namespace app\modules\v1\components\routeAnalyzer;

use api\common\models\Parking;
use app\models\client_tariff\TaxiTariffOption;
use app\modules\v1\components\routeAnalyzer\calculate\CalculateAccrualAirport;
use app\modules\v1\components\routeAnalyzer\calculate\CalculateAccrualStation;
use app\modules\v1\components\routeAnalyzer\calculate\CalculateFactory;
use app\modules\v1\components\routeAnalyzer\route\Point;
use app\modules\v1\components\routeAnalyzer\route\Route;
use yii\base\Object;

/**
 * Class RouteAnalyzerResponse
 * @package app\modules\v1\components\routeAnalyzer
 *
 * @property TariffInfo $tariffInfo
 * @property Route      $route
 * @property string     $orderTime
 */
class RouteAnalyzerResponse extends Object
{
    public $tariffInfo;
    public $route;
    public $orderTime;
    public $additionalCost;
    public $discount;

    public $isFix = true;
    public $enableParkingRatio = false;
    public $cityCost;
    public $trackCost;
    public $summaryCost;
    public $summaryCostNoDiscount;
    public $isAirport;
    public $isStation;
    public $district;

    public $_dev;

    const DISTRICT_TYPE_PERCENT = 'PERCENT';
    const DISTRICT_TYPE_MONEY = 'MONEY';


    public function calculate()
    {
        $calculateAirport = new CalculateAccrualAirport();
        $response         = $calculateAirport->execute($this->route, $this->tariffInfo);
        $airportCost      = $response;

        $calculateStation = new CalculateAccrualStation();
        $response         = $calculateStation->execute($this->route, $this->tariffInfo);
        $stationCost      = $response;

        // расчет стоимости поездки город / пригород - с учетом расстояния
        $trackCost = 0;
        $cityCost = 0;

        if($this->route->getStartPointType() == Point::TYPE_TRACK ){
            $calculateTrack = CalculateFactory::getFactory($this->tariffInfo->tariffDataTrack->accrual);
            $response       = $calculateTrack->execute($this->route, $this->tariffInfo, Point::TYPE_TRACK);
            $trackCost      = $response;

            $calculateCity = CalculateFactory::getFactory($this->tariffInfo->tariffDataCity->accrual, $calculateTrack);
            $response      = $calculateCity->execute($this->route, $this->tariffInfo, Point::TYPE_CITY);
            $cityCost      = $response;
        }elseif($this->route->getStartPointType() == Point::TYPE_CITY ){
            $calculateCity = CalculateFactory::getFactory($this->tariffInfo->tariffDataCity->accrual);
            $response      = $calculateCity->execute($this->route, $this->tariffInfo, Point::TYPE_CITY);
            $cityCost      = $response;


            $calculateTrack = CalculateFactory::getFactory($this->tariffInfo->tariffDataTrack->accrual, $calculateCity);
            $response       = $calculateTrack->execute($this->route, $this->tariffInfo, Point::TYPE_TRACK);
            $trackCost      = $response;
        }
        /*$
        calculateTrack = CalculateFactory::getFactory($this->tariffInfo->tariffDataTrack->accrual);
        $response       = $calculateTrack->execute($this->route, $this->tariffInfo, Point::TYPE_TRACK);
        $trackCost      = $response;

        $calculateCity = CalculateFactory::getFactory($this->tariffInfo->tariffDataCity->accrual);
        $response      = $calculateCity->execute($this->route, $this->tariffInfo, Point::TYPE_CITY);
        $cityCost      = $response;*/

        $rounding     = $this->tariffInfo->getRounding($this->route->getFinishPointType());
        $roundingType = $this->tariffInfo->getRoundingType($this->route->getFinishPointType());
        $minPrice     = $this->getMinPrice();

        $enabledParkingRatio = $this->tariffInfo->getEnabledParkingRatio($this->route->getStartPointType());
        $district            = ($enabledParkingRatio === 1) ? $this->route->getDistinctValues()
            : $this->route->getDistinctDefaultValues();

        $summaryCostNoDiscount = $cityCost + $trackCost + $airportCost + $stationCost;
        $summaryCostNoDiscount = $summaryCostNoDiscount * $district[Parking::DISTRICT_TYPE_PERCENT] / 100
            + $district[Parking::DISTRICT_TYPE_MONEY];
        $summaryCostNoDiscount += $this->additionalCost;

        // Округляем по правилам тарифа
        switch ($roundingType) {
            case TaxiTariffOption::ROUNDING_TYPE_FLOOR:
                $summaryCostNoDiscount = floor($summaryCostNoDiscount / $rounding) * $rounding;
                break;

            case TaxiTariffOption::ROUNDING_TYPE_CEIL:
                $summaryCostNoDiscount = ceil($summaryCostNoDiscount / $rounding) * $rounding;
                break;

            default:
                $summaryCostNoDiscount = round($summaryCostNoDiscount / $rounding) * $rounding;
        }

        if ($calculateAirport->getIsAirport() || $calculateStation->getIsStation()) {
            $this->isFix = true;
        } else {
            // Сравниваем с минималькой
            $summaryCostNoDiscount = $summaryCostNoDiscount < $minPrice ? $minPrice : $summaryCostNoDiscount;
        }

        // Добавляем скидку клиента
        $summaryCost = $summaryCostNoDiscount * (100 - $this->discount) / 100;


        // Запрещаем отрицательное значение
        $summaryCost           = $summaryCost < 0 ? 0 : $summaryCost;
        $summaryCostNoDiscount = $summaryCostNoDiscount < 0 ? 0 : $summaryCostNoDiscount;


        if ($this->tariffInfo->tariffDataCity->accrual !== TaxiTariffOption::ACCRUAL_FIX
            && $this->route->getIsCity()) {
            $this->isFix = false;
        }

        if ($this->tariffInfo->tariffDataTrack->accrual !== TaxiTariffOption::ACCRUAL_FIX
            && $this->route->getIsTrack()) {
            $this->isFix = false;
        }

        if ($this->tariffInfo->tariffDataTrack->calculation_fix === 1
            && $this->tariffInfo->tariffDataCity->calculation_fix === 1) {
            $this->isFix = true;
        }

        if ($this->route->getIsOnePoint()) {
            $this->isFix = false;
        }

        $this->cityCost              = $cityCost;
        $this->trackCost             = $trackCost;
        $this->summaryCost           = $summaryCost;
        $this->summaryCostNoDiscount = $summaryCostNoDiscount;
        $this->isAirport             = $calculateAirport->getIsAirport();
        $this->isStation             = $calculateStation->getIsStation();
        $this->district              = $district;
        $this->enableParkingRatio    = (boolean)$this->tariffInfo->tariffDataCity->enabled_parking_ratio;
    }


    public function getMinPrice()
    {
        $minPrice = $this->tariffInfo->getMinPrice($this->route->getFinishPointType());

        $calculationFix = $this->tariffInfo->getCalculationFix($this->route->getFinishPointType());
        $secondMinPrice = $calculationFix === 1
            ? $this->tariffInfo->getSecondMinPrice($this->route->getFinishPointType()) : 0;

        $count = count($this->route->items);

        return $minPrice + $secondMinPrice * ($count - 1);
    }

}