<?php

namespace app\modules\v1\components\routeAnalyzer\route;

use api\common\models\Parking;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class Point extends Object
{
    public $lat;
    public $lon;
    public $cityId;
    public $parkingId;
    public $polygons;
    public $type;

    const TYPE_CITY = 'city';
    const TYPE_TRACK = 'track';
    const TYPE_AIRPORT = 'airport';
    const TYPE_STATION = 'station';


    public function init()
    {
        if ($this->cityId) {
            $this->cityId = (int)$this->cityId;
        }

        if ($this->parkingId) {
            $this->parkingId = (int)$this->parkingId;
        }

        $this->setPolygon();

        if ($this->lat) {
            $this->lat = (float)$this->lat;
        }
        if ($this->lon) {
            $this->lon = (float)$this->lon;
        }
    }

    /**
     * @param $data
     *
     * @return Point
     */
    public static function make($data)
    {

        return new Point([
            'lat'       => ArrayHelper::getValue($data, 'lat'),
            'lon'       => ArrayHelper::getValue($data, 'lon'),
            'cityId'    => ArrayHelper::getValue($data, 'city_id'),
            'parkingId' => ArrayHelper::getValue($data, 'parking_id'),
        ]);
    }

    public function getCoordsAsArray()
    {
        return [$this->lat, $this->lon];
    }

    public function isInBasePolygon()
    {
        return $this->type === self::TYPE_CITY;
    }

    public function setPolygon()
    {

        $this->polygons = \Yii::$app->get('parkingService')->getPolygonsByPoint([
            $this->lat,
            $this->lon,
        ], false, false, [
            'parking_id',
            'type',
            'in_district',
            'out_district',
            'in_distr_coefficient_type',
            'out_distr_coefficient_type',
        ]);

        if ($this->parkingId) {
            $polygon        = \Yii::$app->get('parkingService')->getPolygonById($this->parkingId, [
                'parking_id',
                'type',
                'in_district',
                'out_district',
                'in_distr_coefficient_type',
                'out_distr_coefficient_type',
            ]);
            $this->polygons = array_merge([$polygon], $this->polygons);
        }

        $isBasePolygon = \Yii::$app->get('parkingService')->isPointInBasePolygon([
            $this->lat,
            $this->lon,
        ]);

        $this->type = $isBasePolygon ? self::TYPE_CITY : self::TYPE_TRACK;
    }

    public function getIsAirport()
    {
        if (!empty($this->polygons)) {
            $firstPolygon = reset($this->polygons);

            return $firstPolygon['type'] === Parking::TYPE_AIRPORT;
        }

        return false;
    }

    public function getIsStation()
    {
        if (!empty($this->polygons)) {
            $firstPolygon = reset($this->polygons);

            return $firstPolygon['type'] === Parking::TYPE_STATION;
        }

        return false;
    }

    public function getIsParking()
    {
        if (!empty($this->polygons)) {
            $firstPolygon = reset($this->polygons);

            return $firstPolygon['type'] !== Parking::TYPE_BASE_POLYGON;
        }

        return false;
    }

    public function getParkingId()
    {
        if (is_array($this->polygons) && !empty($this->polygons)) {
            $polygon = reset($this->polygons);

            return $polygon['parking_id'];
        }

        return null;
    }

    public function getDistricts()
    {
        if (is_array($this->polygons) && !empty($this->polygons)) {
            $polygon = reset($this->polygons);

            return [
                'in' => $polygon['in_district'],
                'out' => $polygon['out_district'],
                'inType' => $polygon['in_distr_coefficient_type'],
                'outType' => $polygon['out_distr_coefficient_type'],
            ];
        }

        return null;
    }


}