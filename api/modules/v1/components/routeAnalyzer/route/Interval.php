<?php

namespace app\modules\v1\components\routeAnalyzer\route;

use yii\base\Object;

/**
 * Class Interval
 * @package app\modules\v1\components\routeAnalyzer
 *
 * @property Point $startPoint
 * @property Point $finishPoint
 */
class Interval extends Object
{
    public $startPoint;
    public $finishPoint;

    public $time = 0;
    public $distance = 0;

    public $points = [];

    public function init()
    {
        $response = \Yii::$app->geoservice->getRouteInfo([
            'coords' => $this->getCoordsAsArray(),
        ]);

        if ($response['status'] === 1) {
            $this->time     = (int)$response['result'][0]['time'];
            $this->distance = (float)$response['result'][0]['distance'];
        }
    }

    public static function make($data, $startPoint = null, $finishPoint = null)
    {
        if ($startPoint) {
            $start = $startPoint;
        } else {
            $startPoint = reset($data);
            $start      = Point::make([
                'lat' => $startPoint[0],
                'lon' => $startPoint[1],
            ]);
        }
        if ($finishPoint) {
            $finish = $finishPoint;
        } else {
            $finishPoint = end($data);
            $finish      = Point::make([
                'lat' => $finishPoint[0],
                'lon' => $finishPoint[1],
            ]);
        }


        $interval = new Interval([
            'startPoint'  => $start,
            'finishPoint' => $finish,
            'points'      => $data,
        ]);

        return $interval;
    }

    public function getCoordsAsArray()
    {
        $result = [$this->startPoint->getCoordsAsArray()];

        if ($this->finishPoint) {
            $result[] = $this->finishPoint->getCoordsAsArray();
        }

        return $result;
    }
}