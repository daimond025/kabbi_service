<?php

namespace app\modules\v1\components\routeAnalyzer\route;

use api\common\models\Parking;
use yii\base\Object;

/**
 * Class Route
 * @package app\modules\v1\components\routeAnalyzer
 *
 * @property RouteItem[] $items
 */
class Route extends Object
{
    public $items = [];

    /**
     * @param array $data
     *
     * @return self
     */
    public static function make($data)
    {
        $startPoint = reset($data);
        $finishPoint = next($data);

        $items = [];
        do{
            $items[] = RouteItem::make($startPoint, $finishPoint);
            $startPoint = current($data);
            $finishPoint = next($data);
        } while($finishPoint);


        return new Route([
            'items' => $items,
        ]);
    }

    public function getCityTime()
    {
        $time = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $time += $item->getCityTime();
        }

        return $time;
    }

    public function getTrackTime()
    {
        $time = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $time += $item->getTrackTime();
        }

        return $time;
    }

    public function getSummaryTime()
    {
        $time = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $time += $item->getSummaryTime();
        }

        return $time;
    }


    public function getCityDistance()
    {
        $distance = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $distance += $item->getCityDistance();
        }

        return $distance;
    }

    public function getTrackDistance()
    {
        $distance = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $distance += $item->getTrackDistance();
        }

        return $distance;
    }

    public function getSummaryDistance()
    {
        $distance = 0;
        foreach ($this->items as $item) {
            /** @var $item RouteItem */
            $distance += $item->getSummaryDistance();
        }

        return $distance;
    }

    public function getStartPointType()
    {
        if(!empty($this->items)) {
            $firstInterval = reset($this->items);

            return $firstInterval->startPoint->type;
        }

        return null;
    }

    public function getFinishPointType()
    {
        if(!empty($this->items)) {
            $firstInterval = end($this->items);

            return $firstInterval->finishPoint ? $firstInterval->finishPoint->type : $firstInterval->startPoint->type;
        }

        return null;
    }

    public function getIsOnePoint()
    {
        if(!empty($this->items)) {
            $firstInterval = reset($this->items);

            $startPoint = $firstInterval->startPoint;
            $finishPoint = $firstInterval->finishPoint;

            if( ((double)$startPoint->lat === (double)$finishPoint->lat) && ((double)$startPoint->lat === (double)$finishPoint->lat) ){
                return true;

            }
            return false;
           // return !$firstInterval->finishPoint;
        }

        return false;
    }

    public function getInterval()
    {
        $result = [];
        foreach ($this->items as $item) {
            $result = array_merge($result, $item->getInterval());
        }

        return $result;
    }

    public function getIsAirport()
    {
        if(empty($this->items)) {
            return false;
        }

        foreach ($this->items as $item) {
            if(!$item->getIsAirport()) {
                return false;
            }
        }

        return true;
    }

    public function getIsStation()
    {
        if(empty($this->items)) {
            return false;
        }

        foreach ($this->items as $item) {
            if(!$item->getIsStation()) {
                return false;
            }
        }

        return true;
    }

    public function getIsCity()
    {
        if(empty($this->items)) {
            return false;
        }

        foreach ($this->items as $item) {
            if($item->getIsCity()) {
                return true;
            }
        }

        return false;
    }

    public function getIsTrack()
    {
        if(empty($this->items)) {
            return false;
        }

        foreach ($this->items as $item) {
            if($item->getIsTrack()) {
                return true;
            }
        }

        return false;
    }

    public function getDistinctValues()
    {
        $district = $this->getDistinctDefaultValues();

        foreach ($this->items as $item) {
            $districtByStartPoint = $item->startPoint->getDistricts();
            switch ($districtByStartPoint['outType']) {
                case Parking::DISTRICT_TYPE_PERCENT:
                    $district[Parking::DISTRICT_TYPE_PERCENT] += $districtByStartPoint['out'];

                    break;
                case Parking::DISTRICT_TYPE_MONEY:
                    $district[Parking::DISTRICT_TYPE_MONEY] += $districtByStartPoint['out'];
                    break;
            }


            if($item->finishPoint) {

                $districtByFinishPoint = $item->finishPoint->getDistricts();
                switch ($districtByFinishPoint['inType']) {
                    case Parking::DISTRICT_TYPE_PERCENT:
                        $district[Parking::DISTRICT_TYPE_PERCENT] += $districtByFinishPoint['in'];
                        break;
                    case Parking::DISTRICT_TYPE_MONEY:
                        $district[Parking::DISTRICT_TYPE_MONEY] += $districtByFinishPoint['in'];
                        break;
                }
            }

        }

        return $district;
    }

    public function getDistinctDefaultValues()
    {
        return [
            Parking::DISTRICT_TYPE_PERCENT => 100,
            Parking::DISTRICT_TYPE_MONEY   => 0,
        ];
    }
}