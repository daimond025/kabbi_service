<?php

namespace app\modules\v1\components\routeAnalyzer\route;

use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class RouteItem
 * @package app\modules\v1\components\routeAnalyzer
 *
 * @property Point      $startPoint
 * @property Point      $finishPoint
 * @property Interval[] $intervals
 */
class RouteItem extends Object
{
    public $startPoint;
    public $finishPoint;

    public $intervals = [];

    public function init()
    {
        $this->setInterval();
    }

    /**
     * @param array|bool $startPoint
     * @param array|bool $finishPoint
     *
     * @return null|self
     */
    public static function make($startPoint, $finishPoint)
    {
        if (!$startPoint) {
            return null;
        }

        $start = Point::make($startPoint);

        $finish = $finishPoint ? Point::make($finishPoint) : $start;


        return new RouteItem([
            'startPoint'  => $start,
            'finishPoint' => $finish,
        ]);
    }

    public function getCoordsAsArray()
    {
        $result = [$this->startPoint->getCoordsAsArray()];

        if ($this->finishPoint) {
            $result[] = $this->finishPoint->getCoordsAsArray();
        }

        return $result;
    }

    public function setInterval()
    {
        $response = \Yii::$app->geoservice->getRoute([
            'coords' => $this->getCoordsAsArray(),
        ]);

        if ($response['status'] === 1) {

            $this->chunkInterval($response['result']);
        }
    }

    protected function chunkInterval($interval)
    {
        if ($this->isChunk()) {

            $differentPointNumber = $this->getDifferentPointNumber($interval);

            $this->intervals = [
                Interval::make(array_slice($interval, 0, $differentPointNumber), $this->startPoint),
                interval::make(array_slice($interval, $differentPointNumber), null, $this->finishPoint),
            ];

        } else {
            $this->intervals = [
                new Interval([
                    'startPoint'  => $this->startPoint,
                    'finishPoint' => $this->finishPoint,
                    'points'      => $interval,
                ]),
            ];
        }


    }

    /**
     * Нужно ли разбивать маршрут.
     * Если в маршруте есть точка смены типа "city" - "track", то нужно
     *
     * @return bool
     */
    protected function isChunk()
    {
        if (!$this->startPoint || !$this->finishPoint) {
            return false;
        }

        return $this->startPoint->type !== $this->finishPoint->type;
    }

    protected function getDifferentPointNumber($interval)
    {

        if ($this->startPoint->isInBasePolygon()) {
            $points          = array_reverse($interval);
            $isInBasePolygon = $this->finishPoint->isInBasePolygon();

        } else {
            $points          = $interval;
            $isInBasePolygon = $this->startPoint->isInBasePolygon();
        }


        foreach ($points as $key => $point) {
            $isInBasePolygonCurrent = \Yii::$app->parkingService->isPointInBasePolygon($point);
            if ($isInBasePolygon !== $isInBasePolygonCurrent) {
                if ($this->startPoint->isInBasePolygon()) {
                    return count($points) - $key;
                }

                return $key;
            }
        }

        return 0;
    }


    public function getCityTime()
    {
        $time = 0;
        /** @var Interval $interval */
        foreach ($this->intervals as $interval) {
            if ($interval->startPoint->isInBasePolygon()) {
                $time += $interval->time;
            }
        }

        return $time;
    }

    public function getTrackTime()
    {
        $time = 0;
        foreach ($this->intervals as $interval) {
            /** @var Interval $interval */
            if (!$interval->startPoint->isInBasePolygon()) {
                $time += $interval->time;
            }
        }
        return $time;
    }

    public function getSummaryTime()
    {
        return $this->getCityTime() + $this->getTrackTime();
    }

    public function getCityDistance()
    {
        $distance = 0;
        foreach ($this->intervals as $interval) {
            /** @var Interval $interval */
            if ($interval->startPoint->isInBasePolygon()) {
                $distance += $interval->distance;
            }
        }

        return $distance;
    }

    public function getTrackDistance()
    {
        $distance = 0;
        foreach ($this->intervals as $interval) {
            /** @var Interval $interval */
            if (!$interval->startPoint->isInBasePolygon()) {
                $distance += $interval->distance;
            }
        }
        return $distance;
    }

    public function getSummaryDistance()
    {
        return $this->getCityDistance() + $this->getTrackDistance();
    }

    public function getInterval()
    {
        $result = [];
        foreach ($this->intervals as $interval) {
            $result = array_merge($result, $interval->points);
        }

        return $result;
    }

    public function getIsAirport()
    {

        if(!$this->finishPoint || !$this->startPoint) {
            return false;
        }


        if($this->startPoint->getIsParking() && $this->finishPoint->getIsAirport()) {
            return true;
        }
        if($this->startPoint->getIsAirport() && $this->finishPoint->getIsParking()) {
            return true;
        }

        return false;
    }

    public function getIsStation()
    {
        if(!$this->finishPoint || !$this->startPoint) {
            return false;
        }

        if($this->finishPoint->getIsStation() || $this->startPoint->getIsStation()) {
            return true;
        }

        return false;
    }

    public function getIsCity()
    {
        if($this->startPoint && $this->startPoint->type === Point::TYPE_CITY) {
            return true;
        }

        if($this->finishPoint && $this->finishPoint->type === Point::TYPE_CITY) {
            return true;
        }

        return false;
    }

    public function getIsTrack()
    {
        if($this->startPoint && $this->startPoint->type === Point::TYPE_TRACK) {
            return true;
        }

        if($this->finishPoint && $this->finishPoint->type === Point::TYPE_TRACK) {
            return true;
        }

        return false;
    }

    /**
     * Получить транзитную точку.
     *
     * @param bool $inStartPolygon
     *
     * @return Point|null
     */
    public function getTransitPoint($inStartPolygon = true)
    {
        /** @var Interval $interval */
        $interval = ArrayHelper::getValue($this->intervals, $inStartPolygon ? 0 : 1);

        if ($interval) {
            return $inStartPolygon ? $interval->finishPoint : $interval->startPoint;
        }

        return null;
    }
}