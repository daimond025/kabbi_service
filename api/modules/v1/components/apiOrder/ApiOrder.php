<?php

namespace app\modules\v1\components\apiOrder;

use api\components\logger\filter\Filter;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class ApiOrder extends Component
{
    public $baseUrl;
    public $timeout = 10;

    const ACTION_INFO = 'promocode/promo/promo_code_info';
    const ACTION_FIND_SUITABLE = 'promocode/promo/find_suitable_promo_code';

    const RESPONSE_OK = 0;

    /**
     * @param $tenantId
     * @param $cityId
     * @param $positionId
     * @param $clientId
     * @param $carClassId
     * @param $time
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    function findSuitablePromoCode($tenantId, $cityId, $positionId, $clientId, $carClassId, $time)
    {
        $params = [
            'tenant_id'    => $tenantId,
            'city_id'      => $cityId,
            'position_id'  => $positionId,
            'client_id'    => $clientId,
            'car_class_id' => $carClassId,
            'order_time'   => $time,
            'expand'       => 'promo',
        ];

        return $this->get(self::ACTION_FIND_SUITABLE, $params);
    }

    /**
     * @param $codeId
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    function getPromoCodeInfo($codeId)
    {

        $params = [
            'code_id' => $codeId,
        ];

        return $this->get(self::ACTION_INFO, $params);
    }

    /**
     * @param $action
     * @param $params
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function get($action, $params)
    {
        $options = [
            'query'   => $params,
            'timeout' => $this->timeout,
        ];

        $result = $this->send('GET', $action, $options);

        if (!$result || ArrayHelper::getValue($result, 'code') !== self::RESPONSE_OK) {
            return [];
        }

        return ArrayHelper::getValue($result, 'result', []);
    }

    /**
     * @param $action
     * @param $params
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function post($action, $params)
    {
        $options = [
            'form_params' => $params,
            'timeout'     => $this->timeout,
        ];

        $result = $this->send('POST', $action, $options);

        if (!$result || ArrayHelper::getValue($result, 'code') !== self::RESPONSE_OK) {
            return [];
        }

        return ArrayHelper::getValue($result, 'result', []);
    }

    /**
     * @param $method
     * @param $action
     * @param $options
     *
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function send($method, $action, $options)
    {

        $url = $this->getUrl($action);

        $client = new Client();

        \Yii::$app->get('logger')->log('Запрос в api_order {url}', ['url' => $url]);
        \Yii::$app->get('logger')->log('Параметры запроса: {options}', ['options' => json_encode($options)]);
        $time = microtime(true);

        try {
            $response = $client->request($method, $url, $options);

            $result   = json_decode($response->getBody(), true);
            $timeout  = round((microtime(true) - $time) / 1000, 3);

            \Yii::$app->get('logger')->log('Запрос занял {timeout} сек', ['timeout' => $timeout]);
            \Yii::$app->get('logger')->log('Ответ: {response}', ['response' => json_encode(Filter::filter($result))]);

            return json_decode($response->getBody(), true);

        } catch (BadResponseException $exception) {
            $timeout = round((microtime(true) - $time) / 1000, 3);
            \Yii::$app->get('logger')->log('Запрос занял {timeout} сек', ['timeout' => $timeout]);

            \Yii::$app->get('logger')->error('Ошибка. Статус ответа {statusCode}. {message}', [
                'statusCode' => $exception->getResponse()->getStatusCode(),
                'message'    => $exception->getMessage(),
            ]);

            \Yii::error("Ошибка при подключении к серверу $url. Status code {$exception->getResponse()->getStatusCode()}");

            return null;
        }

    }


    protected function getUrl($action)
    {
        return rtrim($this->baseUrl, '/') . '/' . $action;
    }
}