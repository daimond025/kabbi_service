<?php

namespace app\modules\v1\components\taxiGeocoder;

use yii\base\Object;
use Yii;
use api\common\models\GeodataAz;

require 'vendor/autoload.php';

class TaxiGeocoder extends Object
{

    const GEOCODER_ACCURACY_AZ = 0.0006;
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Получение координат по адресу
     * @param type $address
     * @return type
     */
    public function findCoordsByAddress($address, $lang = "ru", $geocoderType = "ru")
    {
        $result = call_user_func_array(array(__NAMESPACE__ . '\TaxiGeocoder', 'findCoordsByAddress_' . $geocoderType), array($address, $lang));
        return $result;
    }

    private function findCoordsByAddress_ru($address, $lang)
    {
        //Кэш
        $cache = Yii::$app->cache;
        //Ключ кэша - адрес поиска
        $cacheKey = md5($address);
        //Ищем координаты в кэше
        $result = $cache->get($cacheKey);
        // Если не нашли то отбращаемся к геокодеру, если геокодер нашел то кэширууем результат
        if (empty($result)) {
            $result['lat'] = "";
            $result['lon'] = "";
            $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter(2);
            $geocoder = new \Geocoder\Geocoder();
            try {
                $chain = new \Geocoder\Provider\ChainProvider([
                    new \Geocoder\Provider\YandexProvider($adapter, $lang),
                    new \Geocoder\Provider\OpenStreetMapProvider($adapter, $lang),
                    new \Geocoder\Provider\GoogleMapsProvider($adapter, $lang),
                    new \Geocoder\Provider\ArcGISOnlineProvider($adapter, $lang),
                    new \Geocoder\Provider\NominatimProvider($adapter, 'http://open.mapquestapi.com/nominatim/v1/', $lang)
                ]);
                $geocoder->registerProvider($chain);
                $geoData = $geocoder->geocode($address);
            } catch (\RuntimeException $e) {
                \Yii::error('Гекодеру не удалось рапознать адресс. Трэйс ошибки:' . $e);
            } finally {
                if (isset($geoData['latitude']) && !empty($geoData['latitude'])) {
                    $result['lat'] = (string) $geoData['latitude'];
                }
                if (isset($geoData['longitude']) && !empty($geoData['longitude'])) {
                    $result['lon'] = (string) $geoData['longitude'];
                }
                //Если гэокодер нашел координаты, то кэшиурем их на сутки
                if (!empty($result['lon']) && !empty($result['lat'])) {
                    $cache->set($cacheKey, $result, 24 * 2 * 3600);
                }
            }
        }
        return $result;
    }

    private function findCoordsByAddress_az($address, $lang)
    {
        $result = [
            "lat" => "",
            "lon" => ""
        ];
        $city = null;
        $street = null;
        $house = null;
        $addressArr = explode(",", $address);
        if (isset($addressArr[0])) {
            $city = trim($addressArr[0]);
        }
        if (isset($addressArr[1])) {
            $street = trim($addressArr[1]);
        }
        if (isset($addressArr[2])) {
            $house = trim($addressArr[2]);
        }

        try {
            $geoData = GeodataAz::findBySql("SELECT lat,lon from " . GeodataAz::tableName() . " "
                            . "where UPPER(trim(city_" . $lang . "))= UPPER(:city) "
                            . "and UPPER(trim(street_" . $lang . "))= UPPER(:street) and house= :house", [':city' => $city, ':street' => $street, ':house' => $house])
                    ->asArray()
                    ->one();
        } catch (\ErrorException $err) {
            Yii::error($err);
        } finally {
            if (!empty($geoData)) {
                $result["lat"] = (string) $geoData["lat"];
                $result["lon"] = (string) $geoData["lon"];
            }
            return $result;
        }
    }

    public function findAddressByCoords($lat, $lon, $lang = "ru", $geocoderType = "ru")
    {

        $result = call_user_func_array(array(__NAMESPACE__ . '\TaxiGeocoder', 'findAddressByCoords_' . $geocoderType), array($lat, $lon, $lang));
        return $result;
    }

    private function findAddressByCoords_ru($lat, $lon, $lang)
    {
        $addressData = [
            "type"           => "house",
            "name"           => "",
            "lat"            => (string) $lat,
            "lon"            => (string) $lon,
            "street"         => "",
            "city"           => "",
            "house"          => "",
            "housing"        => "",
            "porch"          => "",
            "summaryAddress" => ""
        ];
        $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter(2);
        $geocoder = new \Geocoder\Geocoder();
        $chain = new \Geocoder\Provider\ChainProvider([
            new \Geocoder\Provider\YandexProvider($adapter, $lang),
            new \Geocoder\Provider\OpenStreetMapProvider($adapter, $lang),
            new \Geocoder\Provider\GoogleMapsProvider($adapter, $lang),
        ]);
        $geocoder->registerProvider($chain);
        $geoData = $geocoder->reverse($lat, $lon);
        if (!empty($geoData)) {
            $addressData['city'] = (string) $geoData['city'];
            $addressData['street'] = (string) $geoData['streetName'];
            $addressData['house'] = (string) $geoData['streetNumber'];
            $addressData['summaryAddress'] = (string) $geoData['city'] . ',' . $geoData['streetName'] . ',' . $geoData['streetNumber'];
            return $addressData;
        }
    }

    private function findAddressByCoords_az($lat, $lon, $lang)
    {
        $half = self::GEOCODER_ACCURACY_AZ;
        $addressData = [
            "type"           => "house",
            "label"          => "",
            "lat"            => (string) $lat,
            "lon"            => (string) $lon,
            "street"         => "",
            "city"           => "",
            "house"          => "",
            "housing"        => "",
            "porch"          => "",
            "summaryAddress" => ""
        ];
        $geoData = GeodataAz::findBySql(
                        "SELECT city_" . $lang . ",street_" . $lang . ",label_" . $lang . ",house,type,SQRT(POW(':lat'-`lat`,2)+POW(':lon'-`lon`,2)) AS distance from " . GeodataAz::tableName() . " "
                        . "where (lat BETWEEN :lat - :half AND :lat + :half AND lon BETWEEN :lon - :half AND :lon + :half) ", [':lat' => $lat, ':lon' => $lon, ':half' => $half])
                ->orderBy('distance')
                ->asArray()
                ->one();
        if (!empty($geoData)) {
            if (isset($geoData['type'])) {
                if ($geoData['type'] == "street") {
                    $geoData['type'] = "house";
                }
                if ($geoData['type'] == "geo_object") {
                    $geoData['type'] = "public_place";
                }
            }
            $addressData = [
                "type"           => (string) $geoData['type'],
                "label"          => (string) trim($geoData["label_" . $lang]),
                "lat"            => (string) $lat,
                "lon"            => (string) $lon,
                "street"         => (string) trim($geoData["street_" . $lang]),
                "city"           => (string) trim($geoData["city_" . $lang]),
                "house"          => (string) trim($geoData['house']),
                "housing"        => "",
                "porch"          => "",
                "summaryAddress" => (string) trim($geoData['city_' . $lang] . ',' . $geoData['street_' . $lang] . ',' . $geoData['house'])
            ];
            return $addressData;
        }
    }

}
