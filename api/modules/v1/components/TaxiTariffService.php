<?php

namespace app\modules\v1\components;

use app\models\client_tariff\TaxiTariff;
use app\models\client_tariff\TaxiTariffOption;
use app\modules\v1\models\tariff\TariffActiveDate;
use app\modules\v1\models\tariff\TariffHasAdditionalRecord;
use app\modules\v1\models\tariff\TariffType;
use app\modules\v1\models\tariff\TariffTypeRecord;
use yii\helpers\ArrayHelper;

class TaxiTariffService
{

    public function findAdditionalModels($typeId)
    {
        return TariffHasAdditionalRecord::find()
            ->where([
                'type_id' => $typeId,
            ])
            ->all();
    }

    /**
     * Получить тип тарифа по времени
     *
     * @param int    $tariffId
     * @param string $orderTime 23.05.2017 16:33:00
     *
     * @return string $tariffType тип тарифа  ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS')
     */
    public function getTypeModel($tariffId, $orderTime)
    {
        $time = new \DateTime($orderTime);

        $dateFull       = $time->format('d.m.Y');
        $dateShort      = $time->format('d.m');
        $dayOfWeek      = date('l', strtotime($orderTime));
        $hoursAndMinute = $time->format('H:i');
        $toDay          = [$dayOfWeek, $dateShort, $dateFull];

        $records = $this->findOptionActiveDateModel($tariffId);


        /** @var TariffActiveDate $record */
        foreach ($records as $record) {

            $arr           = explode('|', $record->active_date, 2);
            $date          = ArrayHelper::getValue($arr, 0);
            $interval      = ArrayHelper::getValue($arr, 1);
            $isInterval    = (boolean)$interval;
            $arr           = $isInterval ? explode('-', $interval, 2) : [];
            $startInterval = ArrayHelper::getValue($arr, 0, '00:00');
            $endInterval   = ArrayHelper::getValue($arr, 1, '23:59');

            if (empty($date)) {
                if ($startInterval <= $endInterval) {
                    if ($startInterval <= $hoursAndMinute && $hoursAndMinute < $endInterval) {

                        return $record->tariffType;
                    }
                } else {
                    if ($startInterval <= $hoursAndMinute || $hoursAndMinute < $endInterval) {

                        return $record->tariffType;
                    }
                }

                continue;
            }

            if (ArrayHelper::isIn($date, $toDay)) {
                if ($startInterval <= $hoursAndMinute && $hoursAndMinute < $endInterval) {

                    return $record->tariffType;
                }
            }
        }
        return TariffType::findOne(['tariff_id' => $tariffId, 'type' => TariffTypeRecord::TYPE_CURRENT]);
    }

    protected function findOptionActiveDateModel($tariffId)
    {
        $model = TariffActiveDate::find()
            ->alias('date')
            ->joinWith('tariffType type', false)
            ->where([
                'type.tariff_id' => $tariffId,
            ])
            ->orderBy([
                'type.sort' => SORT_ASC,
            ])
            ->all();

        return $model;
    }

    protected function isActiveTariff($tariffId, $tariffType)
    {
        return TaxiTariffOption::find()
            ->where([
                'tariff_id'   => $tariffId,
                'tariff_type' => $tariffType,
            ])
            ->andWhere('active = :active OR active IS NULL', ['active' => TaxiTariff::ACTIVE])
            ->exists();
    }
}