<?php

namespace app\modules\v1\components;


use api\common\models\Parking;
use app\models\client_tariff\FixHasOption;
use app\modules\v1\components\routeAnalyzerNew\TaxiPointLocation;
use app\modules\v1\models\tariff\TariffHasFixRecord;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class ParkingService
 * @package app\modules\v1\components
 *
 * @property string $key
 */
class ParkingService
{
    protected $_pointLocation;
    protected $tenantId;
    protected $cityId;

    protected $_polygons = [];
    protected $_basePolygon = [];

    public function __construct()
    {
        $this->_pointLocation = new TaxiPointLocation();
    }

    public function setTenantId($tenantId)
    {
        $this->tenantId = (int)$tenantId;
    }

    public function setCityId($cityId)
    {
        $this->cityId = (int)$cityId;
    }


    /**
     * Создание ключа для хранения пар tenantId и cityId
     * @return string
     */
    public function getKey()
    {
        return $this->tenantId . '_' . $this->cityId;
    }


    /**
     * Проверяет находится ли точка в базовом полигоне филиала у арендатора
     *
     * @param array $point
     * $point = [
     *      53.252449035645,
     *      53.252449035645
     * ]
     *
     * @return bool
     */
    public function isPointInBasePolygon($point)
    {
        $basePolygon = $this->getBasePolygon();
        //
        if ($basePolygon) {
            return $this->_pointLocation->pointInPolygon($point,
                    $basePolygon['polygon']) === TaxiPointLocation::TYPE_INSIDE;
        }

        return false;
    }

    /**
     * Возвращает все полигоны, в которые попадает точка
     *
     * @param array $point
     * @param bool  $exceptAirport исключая аэропорты
     * @param bool  $exceptStation исключая вокзалы
     * @param array $only
     *
     * $point = [
     *      53.252449035645,
     *      53.252449035645
     * ]
     *
     * @return array
     *
     * [
     *      [
     *          'parking_id' => 70,
     *          'type' => 'city',
     *          'polygon' => [
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *          ],
     *      ],
     *      [
     *          'parking_id' => 71,
     *          'type' => 'basePolygon',
     *          'polygon' => [
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *          ],
     *      ]
     * ]
     */
    public function getPolygonsByPoint($point, $exceptAirport = false, $exceptStation = false, $only = [])
    {
        $polygons = $this->getPolygons();
        $result   = [];

        if (is_array($polygons)) {
            foreach ($polygons as $polygon) {

                if ($exceptAirport && $polygon['type'] = Parking::TYPE_AIRPORT) {
                    continue;
                }

                if ($exceptStation && $polygon['type'] = Parking::TYPE_STATION) {
                    continue;
                }

                if ($this->_pointLocation->pointInPolygon($point,
                        $polygon['polygon']) === TaxiPointLocation::TYPE_INSIDE
                ) {
                    $result[] = (!empty($only) && is_array($only)) ? $this->getColumns($polygon, $only) : $polygon;
                }
            }
        }
        return $result;
    }

    /**
     * Возвращает полигон, в которую попадает точка
     *
     * @param array $point
     * @param bool  $exceptAirport исключая аэропорты
     * @param bool  $exceptStation исключая вокзалы
     * @param array $only
     *
     * $point = [
     *      53.252449035645,
     *      53.252449035645
     * ]
     *
     * @return array
     *
     * [
     *      'parking_id' => 71,
     *      'type' => 'basePolygon',
     *      'polygon' => [
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *      ],
     * ]
     */
    public function getPolygonByPoint($point, $exceptAirport = false, $exceptStation = false, $only = [])
    {
        $polygons = $this->getPolygonsByPoint($point, $exceptAirport, $exceptStation, $only);

        if (is_array($polygons) && !empty($polygons)) {
            return reset($polygons);
        }

        return [];
    }

    /**
     * Возвращает полигон по его идентификатору
     *
     * @param int $id
     *
     * @return array|null
     *
     *
     * [
     *      'parking_id' => 71,
     *      'type' => 'city',
     *      'polygon' => [
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *      ],
     * ]
     */
    public function getPolygonById($id, $only = [])
    {
        $polygons = $this->getPolygons();

        if (array_key_exists($id, $polygons)) {
            return (!empty($only) && is_array($only)) ? $this->getColumns($polygons[$id], $only) : $polygons[$id];
        }

        return null;
    }


    /**
     * Получить базовый полигон филиала у арендатора
     *
     * @return array|null
     *
     * [
     *      'parking_id' => 71,
     *      'type' => 'basePolygon',
     *      'polygon' => [
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *          [
     *              53.252449035645,
     *              53.252449035645,
     *          ],
     *      ],
     * ]
     */
    public function getBasePolygon()
    {

        if (!ArrayHelper::keyExists($this->key, $this->_basePolygon)) {
            $polygons = $this->getPolygons();

            if (is_array($polygons)) {
                // Базовый полигон всегда один и всегда в конце массива.
                $polygon = end($polygons);
                if ($polygon['type'] === Parking::TYPE_BASE_POLYGON) {
                    $this->_basePolygon[$this->key] = $polygon;
                }
            }
        }

        return ArrayHelper::getValue($this->_basePolygon, $this->key);

    }

    /**
     * Получить все активные парковки филиала у арендатора.  Кроме reseptionArea.
     * Результат кешируется.
     * Порядок полигонов в массиве. Аэропорты > Вокзалы > Остальные > Базовый полигон.
     * Полигоны одного типа сортируются по возрастанию по св-ву sort
     *
     * @return array|null
     *
     * {
     *      '71' => [
     *          'parking_id' => 71,
     *          'type' => 'airport',
     *          'polygon' => [
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *          ],
     *      ],
     *      '72' => [
     *          'parking_id' => 72,
     *          'type' => 'station',
     *          'polygon' => [
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *              [
     *                  53.252449035645,
     *                  53.252449035645,
     *              ],
     *          ],
     *      ],
     *
     * }
     */
    public function getPolygons()
    {
        if (!ArrayHelper::keyExists($this->key, $this->_polygons)) {

            $models = $this->findPolygonsAsArray($this->tenantId, $this->cityId);

            if ($models) {
                $models                      = ArrayHelper::map($models, 'parking_id', function ($model) {
                    $polygon = json_decode($model['polygon'], true);

                    // Перевернем координаты в lat - lon, так как в БД хранятся пары lon - lat
                    $model['polygon'] = array_map(function ($item) {
                        return array_reverse($item);
                    }, $polygon['geometry']['coordinates'][0]);

                    return $model;
                });
                $this->_polygons[$this->key] = $models;
            }
        }

        return ArrayHelper::getValue($this->_polygons, $this->key);
    }


    /**
     * Получить стоимость перехода между парковками
     *
     * @param int $typeId
     * @param int $parkingIdFrom
     * @param int $parkingIdTo
     *
     * @return float|null
     */
    public function getFix($typeId, $parkingIdFrom, $parkingIdTo)
    {
        $model = $this->findFixOption($typeId, $parkingIdFrom, $parkingIdTo);

        if (!$model) {
            return 0;
        }

        return $model->fix->from == $parkingIdFrom ? (float)$model->price_to : (float)$model->price_back;
    }

    /**
     * @param $typeId
     * @param $parkingIdFrom
     * @param $parkingIdTo
     *
     * @return array|null|FixHasOption
     */
    protected function findFixOption($typeId, $parkingIdFrom, $parkingIdTo)
    {
        return TariffHasFixRecord::find()
            ->alias('hasFix')
            ->joinWith([
                'fix f' => function ($query) use ($parkingIdFrom, $parkingIdTo) {
                    /** @var $query ActiveQuery */
                    $query->andWhere([
                        'f.from' => $parkingIdFrom,
                        'f.to'   => $parkingIdTo,
                    ])
                        ->orWhere([
                            'f.from' => $parkingIdTo,
                            'f.to'   => $parkingIdFrom,
                        ]);
                },
            ])
            ->where([
                'hasFix.type_id' => $typeId,
            ])
            ->one();
    }

    /**
     * Получить данные по полигонам
     *
     * @param int $tenantId
     * @param int $cityId
     *
     * @return null|Parking[]
     */
    protected function findPolygonsAsArray($tenantId, $cityId)
    {
        return Parking::find()
            ->select([
                'parking_id',
                'type',
                'polygon',
                'in_district',
                'out_district',
                'in_distr_coefficient_type',
                'out_distr_coefficient_type',
            ])
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'is_active' => Parking::ACTIVE,
            ])
            ->andWhere(['not', ['type' => Parking::TYPE_RECEPTION_AREA]])
            ->orderBy("`type` = 'airport' DESC, `type` = 'station' DESC, `type` = 'basePolygon', sort")
            ->asArray()
            ->all();
    }

    protected function getColumns($array, $columns)
    {
        $columns = (array)$columns;

        if (is_array($array)) {
            foreach ($array as $key => $item) {
                if (!in_array($key, $columns)) {
                    unset($array[$key]);
                }
            }

            return $array;
        }

        return [];
    }

}