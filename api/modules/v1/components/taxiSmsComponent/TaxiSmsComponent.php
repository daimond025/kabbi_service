<?php

namespace app\modules\v1\components\taxiSmsComponent;

use yii\base\Object;
use Yii;
use api\common\models\TenantHasSms;
use app\modules\v1\components\gearman\Gearman;

/**
 * Компонент для отправки смс
 * @author Сергей К.
 */
class TaxiSmsComponent extends Object
{

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**f
     * Отправка задачи в Gearman на отправку СМС уведомления
     * @param type $tenantId
     * @param type $cityId
     * @param type $phone
     * @param type $text
     * @return boolean
     */
    public function sendSms($tenantId, $cityId, $phone, $text)
    {
        $data['server'] = TenantHasSms::find()
                ->where([
                    'tenant_id' => $tenantId,
                    "city_id"   => $cityId,
                    "active"    => 1
                ])
                ->asArray()
                ->all();
        if (empty($data['server'])) {
            Yii::error("Нет серверов для отправки СМС уведомления. tenant_id=$tenantId;city_id=$cityId;phone=$phone;text=$text");
            return false;
        }


        $data['phone'] = $phone;
        $data['text'] = $text;
        $data['tenant_id'] = $tenantId;
        $data['city_id'] = $cityId;

        Yii::$app->gearman->doBackground(Gearman::SMS_TASK, $data);

        return true;
    }

}
