<?php

namespace app\modules\v1\components\geoservice\exceptions;

use yii\base\Exception;

class BadResponseException extends Exception
{
    protected $message = 'Geoservise bad response';
}