<?php

namespace app\modules\v1\components\geoservice\exceptions;

use yii\base\Exception;

class NotResponseException extends Exception
{
    protected $message = 'Geoservise not response';
}