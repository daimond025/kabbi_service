<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 06.07.16
 * Time: 14:27
 */

namespace app\modules\v1\components\geoservice;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class Geoservice extends \yii\base\Component
{

    const STATUS_ERROR = 0;
    const STATUS_OK = 1;
    const STATUS_EMPTY_RESPONSE = 2;

    public $baseUrl;
    public $version;

    public $type_app = 'frontend';
    public $params;
    public $timeout = 5;

    private $tenantId;
    private $cityId;

    private $processingTime = [];

    /**
     * Must be configure.
     * Examples:
     * 'apiKey' => '34324'
     * 'apiKey' => function () {
     *    return 'sdfsdff';
     * }
     * @var string|callable
     */
    private $_api_key;

    private $_status = null;

    private $_response = null;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->_api_key)) {
            throw new InvalidConfigException('Api key hasn`t been configure');
        }
    }

    public function hasError()
    {
        if (is_null($this->_response)) {
            $this->_status = self::STATUS_EMPTY_RESPONSE;
        } else {
            if (!empty($this->_response->geocoding->errors)) {
                $this->_status = self::STATUS_ERROR;
            } else {
                $this->_status = self::STATUS_OK;
            }
        }

        if ($this->_status === self::STATUS_OK) {
            return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->_response->geocoding->errors;
    }

    public function getProcessingTime()
    {
        return array_sum($this->processingTime);
    }

    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
    }

    public function setTenantId($tenantId)
    {
        $this->tenantId = $tenantId;
    }


    // выисление матрицы матрица расстояний (времени) (через гугл)
    public function getmatrixDistance($data){
        if (!empty($this->tenantId)) {
            $data['tenant_id'] = $this->tenantId;
        }
        if (!empty($this->cityId)) {
            $data['city_id'] = $this->cityId;
        }
        $origins_str = $this->getCoords($data['origins']);
        $destinations_str = $this->getCoords($data['destinations']);

        $this->sendResponse('distancematrix', [
            'type_app' => 'client',
            'tenant_id' => $this->tenantId,
            'tenant_domain' => trim($data['tenant_login']),
            'city_id' => $this->cityId,
            'origins' => $origins_str,

            'destinations' => $destinations_str,
        ]);

        $distanceMatrix = $this->formanDistanceRezult( (array)$this->_response);

        return $distanceMatrix;
    }


    // формтирвание данных для
    public function formanDistanceRezult($distanceMatrix){
        $rezult = [];
        if($distanceMatrix['results'][0] && (count($distanceMatrix['results'][0]) > 0)){
            foreach ($distanceMatrix['results'][0]  as $rezult_item){
                $distance_item['distance'] =   ArrayHelper::getValue($rezult_item, 'distance' , '');
                $distance_item['duration'] =   ArrayHelper::getValue($rezult_item, 'duration' , '');
                $distance_item['destination'] =   ArrayHelper::getValue($rezult_item, 'destination' , '');
                $distance_item['latitude'] =   ArrayHelper::getValue($rezult_item, 'latitude' , '');
                $distance_item['longitude'] =   ArrayHelper::getValue($rezult_item, 'longitude' , '');

                $rezult[] = $distance_item;
            }
        }
        return $rezult;

    }
    /**
     * Получить маршрут из точек
     *
     * @param $data - массив параметров
     *
     * @throws InvalidConfigException
     * @return mixed
     */
    public function getRoute($data)
    {
        if (!empty($this->tenantId)) {
            $data['tenant_id'] = $this->tenantId;
        }
        if (!empty($this->cityId)) {
            $data['city_id'] = $this->cityId;
        }

        Yii::$app->get('logger')->log('Получение маршрута из точек');
        $cacheKey     = json_encode($data);
        $cacheHashKey = 'geoservice__' . hash('md5', $cacheKey);
        if ($result = Yii::$app->cache->get($cacheHashKey)) {
            Yii::$app->get('logger')->log('Беру данные из кеша. Ключ {key}', ['key' => $cacheHashKey]);

           // return $result;
        }

        $data['coords'] = $this->getCoords($data['coords']);

        if (empty($data['coords'])) {
            return [
                'status' => self::STATUS_OK,
                'result' => [],
            ];
        }

        $params = $this->getUrlParams($data);

        $this->sendResponse('getroute', $params);

        if (!$this->hasError()) {
            $result = $this->_response->results[0]->route;
            Yii::$app->cache->set($cacheHashKey, [
                'status' => $this->_status,
                'result' => $result,
            ], 5 * 60);

            Yii::$app->get('logger')->log('Записываю данные в кеш на {duration} сек. Ключ {key}', [
                'duration' => 5 * 60,
                'key'      => $cacheHashKey,
            ]);
        } else {
            $result = ['error'];
        }

        return [
            'status' => $this->_status,
            'result' => $result,
        ];
    }

    /**
     * Получить пару время-дистанция между точками
     *
     * @param $data - массив параметров
     *
     * @throws InvalidConfigException
     * @return array
     * [
     *  'status' => 1,
     *  'result' => [
     *      [
     *          'time' => 2,
     *          'distance' => 1234,
     *      ],
     *      [
     *          'time => 3,
     *          'distance' => 1655,
     *      ]
     *  ]
     * ]
     */
    public function getRouteInfo($data)
    {
        Yii::$app->get('logger')->log('Получение дистанции и времени между между точками');

        if (!empty($this->tenantId)) {
            $data['tenant_id'] = $this->tenantId;
        }
        if (!empty($this->cityId)) {
            $data['city_id'] = $this->cityId;
        }

        $data['coords'] = $this->getCoords($data['coords']);


        $cacheKey     = json_encode($data);
        $cacheHashKey = 'geoservice__' . hash('md5', $cacheKey);
        if ($result = Yii::$app->cache->get($cacheHashKey)) {
            Yii::$app->get('logger')->log('Беру данные из кеша. Ключ {key}', ['key' => $cacheHashKey]);

            //return $result;
        }

        if (empty($data['coords'])) {
            return [
                'status' => self::STATUS_OK,
                'result' => [],
            ];
        }

        $data['splitted_route'] = 1;
        $params                 = $this->getUrlParams($data);
        $this->sendResponse('getroute', $params);
        $result = [];

        if (!$this->hasError()) {
            foreach ($this->_response->results as $interval) {
                $result[] = [
                    'time'     => $interval->time * 60,
                    'distance' => $interval->distance,
                ];
            }
        } else {
            $result = ['error'];
        }

        Yii::$app->cache->set($cacheHashKey, [
            'status' => $this->_status,
            'result' => $result,
        ], 5 * 60);

        Yii::$app->get('logger')->log('Записываю данные в кеш на {duration} сек. Ключ {key}', [
            'duration' => 5 * 60,
            'key'      => $cacheHashKey,
        ]);

        return [
            'status' => $this->_status,
            'result' => $result,
        ];
    }

    public function getApiKey()
    {
        return $this->_api_key;
    }

    public function setApiKey($api_key)
    {
        if (is_callable($api_key)) {
            $this->_api_key = call_user_func($api_key);
        } else {
            $this->_api_key = $api_key;
        }
    }


    /**
     * Преобразует массив точек в строку
     *
     * @param $data - массив точек
     *
     * @return string
     *
     * $data = [
     *      [
     *          "56.856816",
     *          "53.221624"
     *      ],
     *      [
     *          "56.857131",
     *          "53.210222"
     *      ]
     * ]
     *
     * return "56.856816,53.221624;56.857131,53.210222"
     */
    private function getCoords($data)
    {
        if (!is_array($data)) {
            return $data;
        }

        $result = [];

        foreach ($data as $item) {
            $result[] = implode(',', $item);
        }

        return implode(';', $result);
    }

    private function getUrl($action)
    {
        return $this->baseUrl . 'v' . $this->version . '/' . $action;
    }

    /**
     * @param $data Массив параметров
     *
     * @return array
     */
    private function getUrlParams($data)
    {
        $data['type_app'] = $this->type_app;
        $data['hash']     = $this->getHash($data);
        ksort($data);

        return $data;
    }

    private function getHash(array $params)
    {
        $api_key     = $this->getApiKey();
        $queryString = http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        return md5($queryString . $api_key);
    }

    /**
     * @param $method - экшн
     * @param $params - параметры
     *
     * @throws InvalidConfigException
     *
     * Пример
     * $method = 'getroute'
     * $params = [
     *      'tenant_id' => 68,
     *      'city_id' => 26068,
     *      'coords' => '56.856816,53.221624;56.856816,53.221624;56.857131,53.210222',
     *      'hash' => '261505b67e211107036fb52caf9f40c5',
     *      'type_app' => 'frontend'
     * ];
     */
    private function sendResponse($method, $params)
    {

        $url = $this->getUrl($method) . (!empty($params) ? '/?'
                . http_build_query($params, '', '&', PHP_QUERY_RFC3986) : '');

        Yii::$app->get('logger')->log('Запрос в geoservice {url}', ['url' => $url]);

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_TIMEOUT        => $this->timeout,
        ]);

        $startTime = microtime(true);
        $response  = curl_exec($curl);

        $timeout = microtime(true) - $startTime;
        Yii::$app->get('logger')->log('Запрос занял {timeout} сек.', ['timeout' => $timeout]);
        $this->processingTime[] = $timeout;

        $this->_response = json_decode($response);

        Yii::$app->get('logger')->log('Ответ: {response}', ['response' => $response]);
        curl_close($curl);
    }
}