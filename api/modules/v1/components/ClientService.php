<?php

namespace app\modules\v1\components;

use api\common\models\Client;
use api\common\models\ClientTemp;

class ClientService
{

    /**
     * Получить код для клиента(существующего или нового)
     *
     * @param integer $tenantId
     * @param string $phone
     *
     * @return null|string
     */
    public static function getCode($tenantId, $phone)
    {
        /** @var Client $client */
        $client = Client::find()
            ->alias('cl')
            ->joinWith('clientPhones ph')
            ->where([
                'cl.tenant_id' => $tenantId,
                'ph.value'     => $phone,
            ])
            ->one();

        if($client) {
            return (string)$client->password;
        }

        /** @var ClientTemp $clientTemp */
        $clientTemp = ClientTemp::find()
            ->where([
                'tenant_id' => $tenantId,
                'phone' => $phone
            ])
            ->one();

        return $clientTemp ? $clientTemp->password : null;
    }
}