<?php

namespace api\modules\v1\components;

use app\modules\v1\components\apiOrder\ApiOrder;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class PromoCodeService extends Object
{

    public function getDiscount($tenantId, $cityId, $positionId, $clientId, $carClassId, $time)
    {
        /** @var ApiOrder $apiOrder */
        $apiOrder         = \Yii::$app->get('apiOrder');
        $suitableResponse = $apiOrder->findSuitablePromoCode($tenantId, $cityId, $positionId, $clientId, $carClassId, $time);
        $codeId           = ArrayHelper::getValue($suitableResponse, 'promo.client_discount', 0);

        return $codeId;
    }

}