<?php

namespace app\modules\v1\components\routeAnalyzerNew;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaxiRouteInfo
 *  Класс описывает возвращаемы поля работы компонента
 * @author Калашников С.П.
 */
class TaxiRouteInfo
{

    //Суммарное время
    public $summaryTime;
    //Суммарное расстояние
    public $summaryDistance;
    //Суммарная цена
    public $summaryCost;
    //Время по городу
    public $cityTime;
    //Расстояние по городу
    public $cityDistance;
    //Цена по городу
    public $cityCost;
    //Время за городом
    public $outCityTime;
    //Расстояние за городом
    public $outCityDistance;
    //Цена за городом
    public $outCityCost;
    //Фиксированная цена или нет
    public $isFix;
    //Тип отрезка start,waypoint
    public $typeSection;
    //Где находится старостовая точка маршрута in/out
    public $startPointLocation;
    //Где находится конечная точка маршрута in/out
    public $finishPointLocation;
    //Цена за допики
    public $additionalCost;
    //Цена за парковки
    public $additionalParking;
    //Маршрут
    public $routeLine;
    public $min_price;

    /**
     * Инфа по тарифу
     * @var type
     */
    public $tariffInfo;

    }
