<?php

namespace app\modules\v1\components\routeAnalyzerNew;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaxiOsrmRouter
 * Класс для работы  с маршрутом (Используется OSRM,
 * если OSRM не может построить маршрут то работает с maps.googleapis.com/maps/api/directions
 * @author Сергей К.
 */
class TaxiOsrmRouter
{

    public $geoDataOsrm;
    public $geoDataGoogle;

    /**
     * Метод отправляет GET запросы через curl
     * @param string $url
     * @return array
     */
    public function sendGet($url, $toArray = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
        $output = curl_exec($ch);
        curl_close($ch);
        $routeData = $output;
        $routeData = utf8_encode($routeData);
        $routeData = str_replace(array("\r\n", "\r", "\n"), '', strip_tags($routeData));
        $roteData = json_decode($routeData, $toArray);
        return $roteData;
    }

    /**
     * Получение инф-ии по маршруту, сначала отправляем запрос на router.project-osrm.org,
     * если не удалось проанализировать/построить маршрут то отправляем запрос http://maps.googleapis.com
     * @param string $fromLat
     * @param string $fromLon
     * @param string $toLat
     * @param string $toLon
     * @return array $result Массив содержащий время и расстояние по маршруту
     */
    public function getRouteInfo($fromLat, $fromLon, $toLat, $toLon)
    {
        $url = "http://router.project-osrm.org/viaroute?loc={$fromLat},{$fromLon}&loc={$toLat},{$toLon}";
        $geoData = $this->sendGet($url);
        $result = null;
        if (isset($geoData['route_summary'] ['total_time'])) {
            $result['time'] = $geoData['route_summary'] ['total_time'];
        }
        if (isset($geoData['route_summary']['total_distance'])) {
            $result['distance'] = $geoData['route_summary']['total_distance'];
        }
        if (empty($result)) {
            $url = "http://maps.googleapis.com/maps/api/directions/json?origin=$fromLat,"
                    . "$fromLon&destination=$toLat,$toLon&sensor=false";
            $geoData = $this->sendGet($url);
            if (isset($geoData['routes']['0']['legs']['0']['distance']['value'])) {
                $result['distance'] = $geoData['routes']['0']['legs']['0']['distance']['value'];
            }
            if (isset($geoData['routes']['0']['legs']['0']['duration']['value'])) {
                $result['time'] = $geoData['routes']['0']['legs']['0']['duration']['value'];
            }
        }
        return $result;
    }

    /**
     * Метод по координатам точек откуда и куда получает маршрут(массив точек)
     * @param string $fromLat
     * @param string $fromLon
     * @param string $toLat
     * @param string $toLon
     * @return array
     */
    public function getRoutePoints($coordsArr)
    {
        $fromLat = $coordsArr[0]['lat'];
        $fromLon = $coordsArr[0]['lon'];
        $countPoints = count($coordsArr);
        $toLat = $coordsArr[$countPoints - 1]['lat'];
        $toLon = $coordsArr[$countPoints - 1]['lon'];
        $lineCoords = '';
        foreach ($coordsArr as $coords) {
            $lineCoords = $lineCoords . "loc={$coords['lat']},{$coords['lon']}&";
        }
        $lineCoords = substr($lineCoords, 0, -1);
        $url = "http://router.project-osrm.org/viaroute?$lineCoords&alt=false";
        $geoData = $this->sendGet($url);
        if (empty($geoData)) {
            $geoData = $this->sendGet($url, false);
        }
        $routePointsArr = array();
        $routeGeometry = null;
        if (is_array($geoData)) {
            if (isset($geoData['route_geometry'])) {
                $routeGeometry = $geoData['route_geometry'];
            }
        }
        if (is_object($geoData)) {
            if (isset($geoData->route_geometry)) {
                $routeGeometry = $geoData->route_geometry;
            }
        }
        if (isset($routeGeometry)) {
            $routePoints = $this->decodePolylineToArray($routeGeometry, 'osrm');
            $startPoint = array("0" => $fromLat, "1" => $fromLon);
            array_unshift($routePoints, $startPoint);
            $endPoint = array("0" => $toLat, "1" => $toLon);
            array_push($routePoints, $endPoint);
            $routePointsArr = $routePoints;
        }
        return $routePointsArr;
    }

    /**
     * Декодирует polyline в массив
     * @param type $encoded
     * @return string (google/osrm)
     */
    function decodePolylineToArray($encoded, $type)
    {
        $length = strlen($encoded);
        $index = 0;
        $points = array();
        $lat = 0;
        $lng = 0;

        while ($index < $length) {
            // Temporary variable to hold each ASCII byte.
            $b = 0;

            // The encoded polyline consists of a latitude value followed by a
            // longitude value.  They should always come in pairs.  Read the
            // latitude value first.
            $shift = 0;
            $result = 0;
            do {
                // The `ord(substr($encoded, $index++))` statement returns the ASCII
                //  code for the character at $index.  Subtract 63 to get the original
                // value. (63 was added to ensure proper ASCII characters are displayed
                // in the encoded polyline string, which is `human` readable)
                $b = ord(substr($encoded, $index++)) - 63;

                // AND the bits of the byte with 0x1f to get the original 5-bit `chunk.
                // Then left shift the bits by the required amount, which increases
                // by 5 bits each time.
                // OR the value into $results, which sums up the individual 5-bit chunks
                // into the original value.  Since the 5-bit chunks were reversed in
                // order during encoding, reading them in this way ensures proper
                // summation.
                $result |= ($b & 0x1f) << $shift;
                $shift += 5;
            }
            // Continue while the read byte is >= 0x20 since the last `chunk`
            // was not OR'd with 0x20 during the conversion process. (Signals the end)
            while ($b >= 0x20);

            // Check if negative, and convert. (All negative values have the last bit
            // set)
            $dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));

            // Compute actual latitude since value is offset from previous value.
            $lat += $dlat;

            // The next values will correspond to the longitude for this point.
            $shift = 0;
            $result = 0;
            do {
                $b = ord(substr($encoded, $index++)) - 63;
                $result |= ($b & 0x1f) << $shift;
                $shift += 5;
            } while ($b >= 0x20);

            $dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
            $lng += $dlng;

            // The actual latitude and longitude values were multiplied by
            // 1e5 before encoding so that they could be converted to a 32-bit
            // integer representation. (With a decimal accuracy of 5 places)
            // Convert back to original values.
            if ($type == 'osrm') {
                $pow = '1e-6';
            } else if ($type == 'google') {
                $pow = '1e-5';
            }
            $points[] = array($lat * $pow, $lng * $pow);
        }

        return $points;
    }

}
