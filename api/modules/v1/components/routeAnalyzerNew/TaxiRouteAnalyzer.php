<?php

namespace app\modules\v1\components\routeAnalyzerNew;

use app\modules\v1\components\routeAnalyzerNew\exceptions\LimitAddressPoints;
use app\modules\v1\components\taxiGeocoder\TaxiGeocoder;
use app\modules\v1\components\TaxiTariffService;
use DateTime;
use Yii;
use yii\base\Exception;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Description of TaxiRouteAnalyzer
 * Анализатор маршрута
 * @author Сергей К.
 */
class TaxiRouteAnalyzer extends Object
{
    /**
     * Округлять если не задано.
     */
    const DEFAULT_ROUNDING = 0.01;

    /**
     * Тип тарифа (по городу)
     */
    const TARIFF_TYPE_CITY = 'CITY';

    /**
     * Тип тарифа (за городом)
     */
    const TARIFF_TYPE_TRACK = 'TRACK';

    /**
     * Тип тарифа аэропорт загородний
     */
    const TARIFF_TYPE_AIRPORT_OUT = 'AIRPORT';

    /**
     * Тип тарифа вокзал
     */
    const TARIFF_TYPE_RAILWAY = "RAILWAY";

    /**
     * Тип тарифа по времени
     */
    const TARIFF_TYPE_TIME = 'TIME';

    /**
     * Тип тарифа по расстоянию
     */
    const TARIFF_TYPE_DISTANCE = 'DISTANCE';

    /**
     * Тип тарифа MIX
     */
    const TARIFF_TYPE_MIX = 'MIXED';

    /**
     * Тип тарифа INTERVAL
     */
    const TARIFF_TYPE_INTERVAL = 'INTERVAL';

    /**
     * Тип тарифа FIX
     */
    const TARIFF_TYPE_FIX = 'FIX';

    /**
     * Коды ошибок
     */
    const SUCCESS = 0;
    const ERROR_NO_EXIST_PARKING = 1;
    const ERROR_NO_INTERVAL = 2;

    /**
     * Геокодер
     * @var type
     */
    public $_geocoder;

    /**
     * Роутер (построение анализ маршрута)
     * @var type
     */
    public $_router;

    /**
     * Тип анализа маршрута (mix,simple)
     * @var string
     */
    public $typeAnalyze = 'simple';

    /**
     * Лог
     * @var type
     */
    //public $_log;

    /**
     * Положение точки отосительно полигона
     * @var type
     */
    public $_pointLocation;

    /**
     * Прослойка для работы с БД
     * @var type
     */
    public $_dataBaseLayer;

    /**
     * Тип тарифа current/holidays/exeptions
     * @var array
     */
    public $tariffType;

    /**
     * Инфа траифа по городу
     * @var array
     */
    public $tariffDataCity;

    /**
     * Инфа тарифа за городом
     * @var array
     */
    public $tariffDataTrack;

    /**
     * День/ночь
     * @var bool
     */
    public $isDay;

    /**
     * Координаты адреса
     * @var type
     */
    public $addressCoords;

    /**
     * Где находится старотовая точка (город, загород) in/out
     * @var string in/out
     */
    public $startPointLocation = null;

    /**
     * Где находится конечная точка (город, загород) in/out
     * @var string in/out
     */
    public $finishPointLocation = null;

    /**
     * Районный коэфицент
     * @var type
     */
    public $district = [
        'MONEY'   => 0,
        'PERCENT' => 100,
    ];

    /**
     * Доп.опции
     * @var type
     */
    public $additional;

    /**
     * Нужно ли зафиксировать цену
     * Если в тарифе стоит галочка- то надо
     * @var type
     */
    public $needFixCost = 0;

    /**
     * Маршрут
     *
     */
    public $routeLine = [];

    public $sectionLine = [];

    public $ifCounted = true;

    /**
     * Код ошибки
     */
    public $error = self::SUCCESS;
    public $test = '';

    /*
     * Функция инициализации, нужна будет при создании расширения на Yii
     */

    public function init()
    {

    }

    /**
     * Конструктор анализаора маршрута
     *
     * @param string $config Конфиг yii
     */
    public function __construct($config = [])
    {
        $this->_geocoder      = new TaxiGeocoder();
        $this->_router        = new TaxiOsrmRouter();
        $this->_pointLocation = new TaxiPointLocation();
        $this->_dataBaseLayer = new TaxiRouteDataBaseLayer();
        parent::__construct($config);
    }

    /**
     * Получить полигоны Базового объекта (города) из БД
     *
     * @param boolaen $reverse (переворот координат)
     *
     * @return array
     */
    public function getBaseObjectPoligon($reverse = false)
    {

        if ($polygon = $this->_dataBaseLayer->getBasePolygon()) {
            $roteData = json_decode($polygon, true);
            $poligons = [];
            if ((isset($roteData['geometry']['type'])) && (isset($roteData['geometry']['coordinates']))) {
                $type = $roteData['geometry']['type'];
                if ($type == "Polygon") {
                    $result   = $roteData['geometry']['coordinates'];
                    $poligons = [];
                    foreach ($result['0'] as $res) {
                        $poligons['0'][] = $res;
                    }
                }
                if ($reverse == true) {
                    $reverse = [];
                    $i       = 0;
                    $j       = 0;
                    foreach ($poligons as $poligon) {
                        $reverse[$i] = [];
                        foreach ($poligon as $poligon2) {
                            $reverse[$i][$j]['0'] = $poligon2['1'];
                            $reverse[$i][$j]['1'] = $poligon2['0'];
                            $j++;
                        }
                        $i++;
                        $j = 0;
                    }

                    return $reverse;
                }
            }

            return $poligons;
        }
    }

    /**
     * Получение точек маршрута
     *
     * @param string $fromLat
     * @param string $fromLon
     * @param string $toLat
     * @param string $toLon
     *
     * @return array
     */
    public function getRoutePoints($coordsArr)
    {
        $coords = array_map(function ($item) {
            return [$item['lat'], $item['lon']];
        }, $coordsArr);

        $result = \Yii::$app->geoservice->getRoute([
            'tenant_id' => $this->_dataBaseLayer->getTenantId(),
            'city_id'   => $this->_dataBaseLayer->getCityId(),
            'coords'    => $coords,
        ]);

        if ($result['status'] == 1) {
            return $result['result'];
        }

        return [];
    }

    /**
     *
     * @param int $count
     *
     * @return int
     */
    public function getStep($count)
    {
        return 1;
        if ($count <= 10) {
            return 1;
        } else {
            if ($count <= 50) {
                return 5;
            } else {
                if ($count <= 100) {
                    return 10;
                } else {
                    return 20;
                }
            }
        }
    }

    /**
     * Форматируем полигоны (из точек в виде массива к строкам)
     *
     * @param array $polygons
     *
     * @return array
     */
    public function formatPoligons($polygons)
    {
        $polygonArr = [];
        $polygons   = is_array($polygons) ? $polygons : [];
        $i          = 0;
        foreach ($polygons as $polygon) {
            foreach ($polygon as $n) {
                $polygonArr[$i][] = $n[0] . ' ' . $n[1];
            }
            $i++;
        }

        return $polygonArr;
    }

    /**
     * Форматируем массив точек маршрута (из точек в виде массива к строкам)
     *
     * @param array  $pointsArr
     * @param string $toLat
     * @param string $toLon
     * @param string $addEndPoint
     *
     * @return array
     */
    public function formatPoints($pointsArr, $toLat, $toLon, $addEndPoint = false)
    {
        // Для оценки будем использовать не все точки маршрута а через некий шаг
        // шаг зависит от количества точек маршрута
        $pointsArr1 = [];
        $count      = count($pointsArr);
        $step       = $this->getStep($count);
        for ($i = 0; $i < $count - $step; $i = $i + $step) {
            $pointsArr1[] = $pointsArr[$i];
        }
        //Форматируем массив точек маршрута
        $points = [];
        foreach ($pointsArr1 as $point) {
            $points[] = $point[0] . ' ' . $point[1];
        }
        if (($addEndPoint == true) && (count($points) > 2)) {
            // добавляем финальную  точку
            $count1   = count($points);
            $endPoint = $toLat . " " . $toLon;
            if ($points[$count1 - 1] !== $endPoint) {
                array_push($points, $endPoint);
            }
        }

        return $points;
    }

    /**
     * Определение положения точек маршрута относително полигона
     *
     * @param array $polygonArr
     * @param array $points
     *
     * @return array
     */
    public function inlocationArr($polygonArr, $points)
    {
        // Ищем где находятся точки маршрута (в полигоне или вне)
        $inLocation = [];
        $j          = 0;

        foreach ($polygonArr as $key => $value) {
            $i = 0;
            foreach ($points as $point) {
                $inLocation[$j][$i]['val']      = $this->_pointLocation->pointStringToCoordinates($point);
                $inLocation[$j][$i]['location'] = $this->_pointLocation->pointInPolygon($point, $polygonArr[$key]);
                $i++;
            }
            $j++;
        }

        return $inLocation;
    }

    /**
     * Базовая Функция ищет точки перелома (сменился location inside <-> outside)
     *
     * @param type $inLocation
     *
     * @return int
     */
    public function findChangePointsBase($inLocation)
    {
        // ищем точки перелома (сменился location inside <-> outside)
        $changePoints      = [];
        $k                 = 0;
        $numberOfLocaction = 0;
        foreach ($inLocation as $locationArr) {
            for ($i = 0; $i < count($locationArr) - 2; $i++) {
                if ($locationArr[$i]['location'] !== $locationArr[$i + 1]['location']) {
                    $changePoints[$k][0]['coords'] = $locationArr[$i]['val'];
                    $changePoints[$k][0]['loc']    = $locationArr[$i]['location'];
                    $changePoints[$k][1]['coords'] = $locationArr[$i + 1]['val'];
                    $changePoints[$k][1]['loc']    = $locationArr[$i + 1]['location'];
                    $changePoints[$k]['nLoc']      = $numberOfLocaction;
                    $k++;
                }
            }
            $numberOfLocaction++;
        }

        return $changePoints;
    }

    /**
     *  Функция ищет точки перелома (город/загород)
     *
     * @param array $changePointsBase
     * @param array $points
     * @param array $pointsArr
     * @param array $polygonArr
     *
     * @return array
     */
    public function findChangePoints($changePointsBase, $points, $pointsArr, $polygonArr)
    {
        $position     = [];
        $pogranPoints = [];
        $j            = 0;
        foreach ($changePointsBase as $node) {
            $point0x = (float)$node['0']['coords'][0];
            $point0y = (float)$node['0']['coords'][1];
            $point0  = ['0' => $point0x, '1' => $point0y];
            $point1x = (float)$node['1']['coords'][0];
            $point1y = (float)$node['1']['coords'][1];
            $point1  = ['0' => $point1x, '1' => $point1y];
            $nLock   = $node['nLoc'];
            $k       = 0;
            foreach ($pointsArr as $point) {
                if (((string)$point[0] == (string)$point0[0]) && ((string)$point[1] == (string)$point0[1])) {
                    $low = $k;
                }

                if (((string)$point[0] == (string)$point1[0]) && ((string)$point[1] == (string)$point1[1])) {
                    $hight = $k;
                }
                $k++;
            }

            for ($i = $low; $i <= $hight; $i++) {
                $pos[$i] = $this->_pointLocation->pointInPolygon($points[$i], $polygonArr[$nLock]);
                if (($i !== $low) && ($pos[$i] !== $pos[$i - 1])) {
                    $position[$j]['val'] = $this->_pointLocation->pointStringToCoordinates($points[$i]);

                    if ($pos[$i] == 'outside') {
                        $position[$j]['loc'] = 'fromInToOut';
                    } else {
                        $position[$j]['loc'] = 'fromOutToIn';
                    }
                    //Координаты точки до смены знака
                    $position[$j]['prev_point'] = $pointsArr[$low - 1];
                    //Координаты точки после смены знака
                    $position[$j]['next_point'] = $pointsArr[$hight + 1];
                    $j++;
                }
            }
        }

        return $position;
    }

    /**
     * Поиск точек перелома (город/загород)
     *
     * @param arr  $coordsArr
     * @param bool $routeIsBracked
     *
     * @return type
     */
    public function findPointsOfIntersection($coordsArr, $routeIsBracked)
    {
        // Получаем и форматируем массив полигонов объекта (Москва)
        $polygon    = $this->getBaseObjectPoligon(true);
        $polygonArr = $this->formatPoligons($polygon);
        //Получаем  массив точек маршрута
        $pointsArr = $this->getRoutePoints($coordsArr);

        if ($routeIsBracked) {
            if (empty($this->routeLine)) {
                $this->routeLine = $pointsArr;
            } else {
                $this->routeLine = array_merge($this->routeLine, $pointsArr);
            }
        } else {
            $this->routeLine = $pointsArr;
        }
        $fromLat     = $coordsArr[0]['lat'];
        $fromLon     = $coordsArr[0]['lon'];
        $countPoints = count($coordsArr);
        $toLat       = $coordsArr[$countPoints - 1]['lat'];
        $toLon       = $coordsArr[$countPoints - 1]['lon'];
        //базовые точки через определенный шаг
        $points = $this->formatPoints($pointsArr, $toLat, $toLon, true);
        // положение этих точек (ин аут)
        $inLocation = $this->inlocationArr($polygonArr, $points);
        // точки где меняется знак расширенный диапазон
        $changePointsBase = $this->findChangePointsBase($inLocation);
        $changePoints     = [];
        if (!empty($changePointsBase)) {
            // все точки
            $points = [];
            foreach ($pointsArr as $point) {
                $points[] = $point[0] . ' ' . $point[1];
            }

            $changePoints = $this->findChangePoints($changePointsBase, $points, $pointsArr, $polygonArr);
        }
        $endPointPos = $this->findPointLocation($toLat, $toLon);
        $endPoint    = [
            'val'        => [
                '0' => $toLat,
                '1' => $toLon,
            ],
            'prev_point' => [
                '0' => $toLat,
                '1' => $toLon,
            ],
            'loc'        => $endPointPos,
        ];
        array_push($changePoints, $endPoint);
        $startPointPos = $this->findPointLocation($fromLat, $fromLon);
        $startPoint    = [
            'val'        => [
                '0' => $fromLat,
                '1' => $fromLon,
            ],
            'next_point' => [
                '0' => $fromLat,
                '1' => $fromLon,
            ],
            'loc'        => $startPointPos,
        ];
        array_unshift($changePoints, $startPoint);

        return $changePoints;
    }

    /**
     * Возвращает массив пар время-дистанция точек
     *
     * @param $coords - Массив точек
     *
     * @return array
     * @throws Exception
     */
    function getRouteInfo($coords)
    {
        $result = \Yii::$app->geoservice->getRouteInfo([
            'tenant_id' => $this->_dataBaseLayer->getTenantId(),
            'city_id'   => $this->_dataBaseLayer->getCityId(),
            'coords'    => $coords,
        ]);

        if ($result['status'] == 1) {
            return $result['result'];
        } elseif ($result['status'] == 2) {
            Yii::error('Геосервис не ответил');
        } else {
            if ($result['status'] === 0) {
                Yii::error(\Yii::$app->geoservice->getErrors());
                throw new Exception();
            }
        }
        for ($i = 0; $i < count($coords) - 1; $i++) {
            $result[] = ['time' => 0, 'distance' => '0'];
        }

        return $result;
    }

    /**
     * Функция возвращает где находится точка в базовом полигоне (inside,outside,boundary)
     *
     * @param string $lat
     * @param string $lon
     *
     * @return string inside/outside/boundary
     */
    public function findPointLocation($lat, $lon)
    {
        $polygons   = (array)$this->getBaseObjectPoligon(true);
        $polygonArr = [];
        $i          = 0;
        foreach ($polygons as $polygon) {
            foreach ($polygon as $n) {
                $polygonArr[$i][] = $n[0] . ' ' . $n[1];
            }
            $i++;
        }
        $point      = ["{$lat} {$lon}"];
        $inLocation = [];
        foreach ($polygonArr as $key => $value) {
            $inLocation[] = $this->_pointLocation->pointInPolygon($point['0'], $polygonArr[$key]);
        }
        foreach ($inLocation as $location) {
            if ($location == 'inside') {
                return 'inside';
            } else {
                if (($location == 'boundary') || ($location == 'vertex')) {
                    return 'inside';
                }
            }
        }

        return 'outside';
    }

    /**
     * Функция ищет в какие парковки попадает точка
     *
     * @param string $lat
     * @param string $lon
     * @param array  $parkings
     *
     * @return array
     */
    public function findPointLocationInParking($lat, $lon, $parkings)
    {
        $polygonArr = [];
        foreach ($parkings as $key => $value) {
            $polygons = $parkings[$key];
            $i        = 0;
            foreach ($polygons as $polygon) {
                foreach ($polygon as $n) {
                    $polygonArr[$key][$i][] = $n[0] . ' ' . $n[1];
                }
                $i++;
            }
        }
        $point      = ["{$lat} {$lon}"];
        $inLocation = [];
        foreach ($polygonArr as $key => $value) {
            $inLocation['lat'] = $lat;
            $inLocation['lon'] = $lon;
            $polygonArr2       = $polygonArr[$key];
            foreach ($polygonArr2 as $key2 => $value2) {
                $loc = $this->_pointLocation->pointInPolygon($point['0'], $polygonArr2[$key2]);
                if ($loc == 'inside') {
                    $type                 = $this->getParkingTypeByid($key);
                    $info                 = [
                        'parkingId' => $key,
                        'type'      => $type['type'],
                    ];
                    $inLocation['info'][] = $info;
                }
            }
        }

        return $inLocation;
    }

    /**
     * Функция форматируем паркови к единому виду
     *
     * @param array   $parkings
     * @param boolean $reverse
     *
     * @return array
     */
    public function formatParkings($parkings, $reverse = false)
    {
        $resArr = [];
        foreach ($parkings as $parking) {
            $parkingId = $parking['parking_id'];
            $poligons  = [];
            $roteData  = json_decode($parking['polygon'], true);
            $type      = $roteData['geometry']['type'];
            $result    = $roteData['geometry']['coordinates'];
            $poligons  = [];
            foreach ($result['0'] as $res) {
                $poligons['0'][] = $res;
            }

            $resArr[$parkingId] = $poligons;
        }

        if ($reverse == true) {
            $reverse = [];
            foreach ($resArr as $key => $value) {
                $poligons = $resArr[$key];
                $i        = 0;
                $j        = 0;
                foreach ($poligons as $poligon) {
                    foreach ($poligon as $poligon2) {
                        $reverse[$key][$i][$j]['0'] = $poligon2['1'];
                        $reverse[$key][$i][$j]['1'] = $poligon2['0'];
                        $j++;
                    }
                    $i++;
                    $j = 0;
                }
            }

            return $reverse;
        }

        return $resArr;
    }

    /**
     * Получить парковки из БД
     * @return array
     */
    public function getParkings()
    {
        $parkings = $this->_dataBaseLayer->getParkings();
        $parkings = $this->formatParkings($parkings, true);

        return $parkings;
    }

    /**
     * Получить тип парковки по Ид
     *
     * @param type $parkingId
     *
     * @return string
     */
    public function getParkingTypeByid($parkingId)
    {
        return $this->_dataBaseLayer->getParkingTypeByid($parkingId);
    }

    /**
     * Функция выбирает одну парковку из нескольких парковок(если точка попала в несколько парковок(например ЦАО и
     * лени-й вокзал )) Парковки с типом airport и station имют более высокий приоритет
     *
     * @param array $fromPointParkingInArr
     *
     * @return int
     */
    public function findOneParking($pointInParkingData)
    {
        $type   = "out";
        $result = [
            'parkingId' => null,
            'type'      => $type,
        ];
        if (isset($pointInParkingData['info'])) {
            $parkingId = null;
            // Попала ли парковка в базовый полигон
            foreach ($pointInParkingData['info'] as $locationInfo) {
                if ($locationInfo['type'] == "basePolygon") {
                    $result['type'] = "in";
                }
            }
            if (count($pointInParkingData['info']) == 1) {
                $result['parkingId'] = $pointInParkingData['info'][0]['parkingId'];

                return $result;
            }
            //Санчала возвращаем аэропорты и вокзалы
            foreach ($pointInParkingData['info'] as $locationInfo) {
                if ($locationInfo['type'] == "airport" || $locationInfo['type'] == "station") {
                    $parkingId           = $locationInfo['parkingId'];
                    $result['parkingId'] = $parkingId;

                    return $result;
                }
            }
            //Иначе что то другое
            foreach ($pointInParkingData['info'] as $locationInfo) {
                if ($locationInfo['type'] !== "basePolygon") {
                    $parkingId           = $locationInfo['parkingId'];
                    $result['parkingId'] = $parkingId;

                    return $result;
                }
            }
            //Если нет то берем первую
            if (!$parkingId) {
                $parkingId = $pointInParkingData['info'][0]['parkingId'];
            }
        } else {
            $parkingId = null;
        }

        return [
            'parkingId' => $parkingId,
            'type'      => $type,
        ];
    }

    /**
     * Получение цены для фиксированных тарифов
     *
     * @param int    $fromPointParkingId ид парковки откуда
     * @param int    $toPointParkingId ид парковки куда
     * @param string $fixTariffs Массив всех фиксированных тарифов
     *
     * @return string
     */
    public function findFixCost($fromPointParkingId, $toPointParkingId, $fixTariffs)
    {
        if (empty($fixTariffs)) {
            return null;
        }
        foreach ($fixTariffs as $tariff) {
            $fixId    = $tariff['fix_id'];
            $optionId = $tariff['option_id'];
            $costData = $this->_dataBaseLayer->getFixTarrifInfo($fixId, $optionId);
            if (($tariff['from'] == $fromPointParkingId) && ($tariff['to'] == $toPointParkingId)) {
                return $costData['price_to'];
            } else {
                if (($tariff['from'] == $toPointParkingId) && ($tariff['to'] == $fromPointParkingId)) {
                    return $costData['price_back'];
                }
            }
        }

        return null;
    }

    /**
     * Получение цены для аэропортов и ЖД
     *
     * @param int    $fromParking ид парковки откуда
     * @param int    $toParking ид парковки куда
     * @param string $airportTariffs Массив фиксированных тарифов для аэропорта
     * @param string $stationTariffs Массив фиксированных тарифов для ЖД
     *
     * @return integer|null
     */
    public function findAirpotrOrStationCost($fromParking, $toParking, $airportTariffs, $stationTariffs)
    {

        $cost = $this->findFixCost($fromParking, $toParking, $airportTariffs);
        if ($cost === null) {
            $cost = $this->findFixCost($fromParking, $toParking, $stationTariffs);
        }

        return empty($cost) ? null : $cost;
    }

    public function formatRouteInfo($routeInfo)
    {
        $routeInfo->cityTime        = (int)ceil($routeInfo->cityTime / 60);
        $routeInfo->cityDistance    = round($routeInfo->cityDistance / 1000, 2);
        $routeInfo->outCityTime     = (int)ceil($routeInfo->outCityTime / 60);
        $routeInfo->outCityDistance = round($routeInfo->outCityDistance / 1000, 2);
        $routeInfo->summaryTime     = (int)ceil($routeInfo->summaryTime / 60);
        $routeInfo->summaryDistance = round($routeInfo->summaryDistance / 1000, 2);

        return $routeInfo;
    }

    /**
     * Функция анализирует отрезки, на которые поделен маршрут
     *
     * @param array $sectionsData
     *  [
     *     0 => [
     *               'fromLat' => 56.841472
     *               'fromLon' => 53.212741
     *               'toLat' => 56.83632635
     *               'toLon' => 53.463359825793
     *               'cost' => null
     *          ]
     *     1 => [
     *               'fromLat' => 56.83632635
     *               'fromLon' => 53.463359825793
     *               'toLat' => 56.85254395
     *               'toLon' => 53.230464226652
     *               'cost' => '500'
     *          ]
     *  ]
     */
    public function analyzeSections($sectionsData)
    {
        $tenantId       = $this->_dataBaseLayer->getTenantId();
        $cityId         = $this->_dataBaseLayer->getCityId();
        $i              = 0;
        $route          = [];
        $routeInfoArray = [];
        foreach ($sectionsData as $section) {
            $routeInfo = new TaxiRouteInfo();
            if ($i == 0) {
                $routeInfo->typeSection = "start";
                $route[]                = [$section['fromLat'], $section['fromLon']];
            } else {
                $routeInfo->typeSection = "waypoint";
            }
            $routeInfo->startPointLocation  = $section['startPointLocation'];
            $routeInfo->finishPointLocation = $section['finishPointLocation'];

            if ($this->startPointLocation === null) {
                $this->startPointLocation = $section['startPointLocation'];
            }
            $this->finishPointLocation = $section['finishPointLocation'];

            $routeInfo->summaryCost         = $section['cost'];
            $routeInfo->ifCounted           = is_null($section['cost']) ? false : true;
            $this->ifCounted = $this->ifCounted && $routeInfo->ifCounted;

            $addressCoordsMix = [
                [
                    'lat' => $section['fromLat'],
                    'lon' => $section['fromLon'],
                ],
                [
                    'lat' => $section['toLat'],
                    'lon' => $section['toLon'],
                ],
            ];
            //Ищем точки смены знака
            $changePoints        = $this->findPointsOfIntersection($addressCoordsMix, true);
            $routeInfo->interval = [];
            for ($i = 0; $i < count($changePoints) - 1; $i++) {
                $toLat        = $changePoints[$i + 1]['val']['0'];
                $toLon        = $changePoints[$i + 1]['val']['1'];
                $route[]      = [$toLat, $toLon];
                $lat_next     = $changePoints[$i + 1]['prev_point'][0];
                $lon_next     = $changePoints[$i + 1]['prev_point'][1];
                $current_parking = $this->getParkingByCoords($tenantId, $cityId, $toLat, $toLon);
                $next_parking = $this->getParkingByCoords($tenantId, $cityId, $lat_next, $lon_next);
                if (($changePoints[$i]['loc'] == 'inside') || ($changePoints[$i]['loc'] == 'fromOutToIn')) {
                    $routeInfo->interval[] = [
                        'type'         => 'in',
                        'current_parking' => $current_parking['inside'],
                        'next_parking' => $next_parking['inside'],
                    ];
                } else {
                    $routeInfo->interval[] = [
                        'type'         => 'out',
                        'current_parking' => $current_parking['inside'],
                        'next_parking' => $next_parking['inside'],
                    ];
                }
            }
            $routeInfo->interval[0]['current_parking']                               = $section['in_parking'];
            $routeInfo->interval[count($routeInfo->interval) - 1]['next_parking'] = $section['out_parking'];

            $routeInfoArray[] = $routeInfo;
        }

        $i    = 0;
        $info = $this->getRouteInfo($route);
        foreach ($routeInfoArray as $key => $item) {
            foreach ($item->interval as $k => $interval) {
                if ($interval['type'] == 'in') {
                    $routeInfoArray[$key]->cityDistance += $info[$i]['distance'];
                    $routeInfoArray[$key]->cityTime += $info[$i]['time'];
                } else {
                    $routeInfoArray[$key]->outCityDistance += $info[$i]['distance'];
                    $routeInfoArray[$key]->outCityTime += $info[$i]['time'];
                }
                $routeInfoArray[$key]->interval[$k] += $info[$i++];
            }
        }

        return $routeInfoArray;
    }

    /**
     * Функция возвращает в какую парковку попала точка
     *
     * @param type $tenantId
     * @param type $cityId
     * @param type $lat
     * @param type $lon
     * @param type $jsonFormat
     * $return string|array $result
     */
    public function getParkingByCoords($tenantId, $cityId, $lat, $lon, $jsonFormat = false)
    {
        $coords = [
            'lat' => $lat,
            'lon' => $lon,
        ];
        $result = [
            'parking'      => null,
            'parking_name' => null,
            'inside'       => null,
            'error'        => null,
        ];

        //Получить все парковки в городе заказчика
        $arParking = $this->_dataBaseLayer->getParkingArrAll($tenantId, $cityId);

        if (empty($arParking)) {
            $result['error'] = 'Не получены парковки.';
        }
        //Форматирование
        $result['parking'] = $arParking;

        $polygon =  \Yii::$app->get('parkingService')->getPolygonByPoint([$lat, $lon]);

        if (isset($polygon['parking_id'])) {
            $result['inside'] = $polygon['parking_id'];
            foreach ($arParking as $park) {
                if ($park['parking_id'] == $polygon['parking_id']) {
                    $result['parking_name'] = $park['name'];
                }
            }
        } else {
            $result['error'] = 'Адрес вне парковок.';
        }
        if ($jsonFormat) {
            return json_encode($result);
        } else {
            return $result;
        }
    }

    /**
     * Анализатор маршрута
     *
     * @param int     $tenantId
     * @param array   $addressArray -массив Адресов
     * array(
     *   array(
     *       'city' => 'Ижевск',
     *       'street' => 'Советская',
     *       'house' => '12',
     *       'housing' => '',
     *       'porch' => '2',
     *   ),
     *   array(
     *       'city' => 'Ижевск',
     *       'street' => 'Ленина',
     *       'house' => '28',
     *       'housing' => '',
     *       'porch' => '',
     *   ),
     * )
     * @param array   $additional
     * @param int     $tariffId
     * @param string  $orderTime
     * @param string  $lang
     * @param string  $geocoderType
     *
     * @return array
     * @throws LimitAddressPoints
     */
    public function analyzeRoute(
        $tenantId,
        $cityId,
        $addressArray,
        $additional,
        $tariffId,
        $orderTime,
        $lang = "ru",
        $geocoderType = "ru"
    ) {
        $this->_dataBaseLayer->setTenantId($tenantId);
        $this->_dataBaseLayer->setCityId($cityId);
        // Массив координат адресов

        if (count($addressArray) > 26) {
            throw new LimitAddressPoints('Превышено количество допустимых точек адресов. ' . count($addressArray) . 'из 26');
        }
        $old_adressArray = $addressArray;
        $addressArray    = [];
        foreach ($old_adressArray as $item) {
            if (!isset($item['parking_id'])) {
                $parkings           = $this->getParkingByCoords($tenantId, $cityId, $item['lat'], $item['lon']);
                $item['parking_id'] = $parkings['inside'];

            }
            $addressArray[] = $item;
        }
        $addressCoords       = $this->findCoords($addressArray, $lang, $geocoderType);
        $this->addressCoords = $addressCoords;
        //Получаем тип тарифа по Вермени заказа (ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS'))
        $tariffType = $this->getTariffTypeByTime($tariffId, $orderTime);
        //Пишем в свойства с каким тарифом работаем
        $this->tariffType = $tariffType;
        // по городу
        $this->tariffDataCity = $this->getTariffData($tariffId, $this::TARIFF_TYPE_CITY, $this->tariffType);
        $this->isDay          = $this->isTariffDay($this->tariffDataCity, $orderTime); // по городу
        //за городом
        $this->tariffDataTrack = $this->getTariffData($tariffId, $this::TARIFF_TYPE_TRACK, $this->tariffType);
        if (isset($this->tariffDataCity['calculation_fix'])) {
            if ($this->tariffDataCity['calculation_fix'] == 1) {
                $this->needFixCost = 1;
            }
        }

        // ..можно ли расчитать по аэропорту
        $airportTariffs = $this->findFixTariffs($tariffId, $tariffType, self::TARIFF_TYPE_AIRPORT_OUT);
        // ..можно ли расчитать по ЖД
        $stationTariffs = $this->findFixTariffs($tariffId, $tariffType, self::TARIFF_TYPE_RAILWAY);

        //Получили все парковки
        $parkings        = $this->getParkings();
        $pointsInParking = [];
        foreach ($addressCoords as $coords) {
            $pointsInParking[] = $this->findPointLocationInParking($coords['lat'], $coords['lon'], $parkings);
        }

        $this->additional = $additional;

        // Получаем районный коэфицент
        $district1      = $addressArray[0]['parking_id'];
        $district2      = $addressArray[count($addressArray) - 1]['parking_id'];
        $this->district = $this->getAdditionalParking($district1, $district2);

        $isAirportOrStation = count($addressArray) > 1;
        $sectionData = [];
        for ($i = 0; $i < count($addressArray) - 1; $i++) {
            $currPoint             = $addressArray[$i];
            $pointParkingCurrentId = $this->findOneParking($pointsInParking[$i]);
            $section               = [
                'in_parking'         => empty($currPoint['parking_id']) ? null : (int)$currPoint['parking_id'],
                'startPointLocation' => $pointParkingCurrentId['type'],
                'fromLat'            => $currPoint['lat'],
                'fromLon'            => $currPoint['lon'],
            ];

            if (isset($addressArray[$i + 1])) {
                $nextPoint          = $addressArray[$i + 1];
                $pointParkingNextId = $this->findOneParking($pointsInParking[$i + 1]);

                $cost = $this->findAirpotrOrStationCost($currPoint['parking_id'], $nextPoint['parking_id'],
                    $airportTariffs, $stationTariffs);
                $isAirportOrStation = $isAirportOrStation && !empty($cost);
                $section = array_merge($section, [
                    'out_parking'         => empty($nextPoint['parking_id']) ? null : (int)$nextPoint['parking_id'],
                    'finishPointLocation' => $pointParkingNextId['type'],
                    'toLat'               => $nextPoint['lat'],
                    'toLon'               => $nextPoint['lon'],
                    'cost'                => $cost,
                ]);
            }


            $sectionData[] = $section;
        }
        if (isset($sectionsData[0]["startPointLocation"])) {
            $this->startPointLocation = $sectionsData[0]["startPointLocation"];
        } else {
            if (isset($pointsInParking[0])) {
                $pointParkingCurrentId    = $this->findOneParking($pointsInParking[0]);
                $this->startPointLocation = $pointParkingCurrentId['type'];
            }
        }
        $this->sectionLine = $this->analyzeSections($sectionData);

        $resultData = $this->calculateRoute();

        $resultData->isFix = $isAirportOrStation || $resultData->isFix;
        $resultData->routeLine = $this->routeLine;

        $resultData->test = $this->test; // Ловушка

        //        if ($this->error != self::SUCCESS) return ['error' => $this->error];
        //        return $resultData; // Вывод без форматирования
        return $this->formatRouteInfo($resultData);
    }

    /**
     * Получение стоимости парковок
     *
     * @param integer $district1 Начальная точка
     * @param integer $district2 Конечная точка
     *
     * @return string
     * */
    public function getAdditionalParking($district1, $district2)
    {
        $addParking = $this->_dataBaseLayer->getAdditionalParking($district1, $district2);

        return $this->tariffDataCity['enabled_parking_ratio'] == 1 ? $addParking : $this->district;
    }

    public function getAdditionalCostById($add, $tariffId, $tariffType)
    {
        $addCost = $this->_dataBaseLayer->getAdditionalCost($add, $tariffId, $tariffType);

        return $addCost;
    }

    /**
     * Получение доп. стоимости
     *
     * @param string $add Имя доп опции
     * @param int    $tariffGroupId ид тарифа
     *
     * @return string
     */
    public function getAdditionalCost($add, $tariffGroupId)
    {
        $addCost = $this->_dataBaseLayer->getAdditionalCost($add, $tariffGroupId);

        return !empty($addCost) ? $addCost : 0;
    }

    public function getFormattdAddress($city, $street, $house, $housing)
    {
        $address = $city . ',' . $street . ',' . $house . ',' . $housing;

        return $address;
    }

    /**
     * Получение координат по адресу
     *
     * @param string $city
     * @param string $street
     * @param string $house
     * @param string $housing
     * @param string $lang
     * @param string $geocoderType
     *
     * @return array
     */
    public function findCoordsByAddress($city, $street, $house, $housing, $lang = "ru", $geocoderType = "ru")
    {
        $result   = [
            'lat' => null,
            'lon' => null,
        ];
        $address  = $this->getFormattdAddress($city, $street, $house, $housing);
        $geoCoder = new TaxiGeocoder();
        $geoData  = $geoCoder->findCoordsByAddress($address, $lang, $geocoderType);
        if (!empty($geoData)) {
            $result = [
                'lat' => $geoData['lat'],
                'lon' => $geoData['lon'],
            ];
        }

        return $result;
    }

    /**
     * Получение кординат адресов
     *
     * @param array  $addressArray
     * @param string $lang
     * @param string $geocoderType
     *
     * @return array
     */
    public function findCoords($addressArray, $lang, $geocoderType)
    {
        $result = [];
        foreach ($addressArray as $address) {
            if (isset($address["lat"]) && !empty($address["lat"])) {
                $pointData = [
                    "lat" => $address["lat"],
                    "lon" => $address["lon"],
                ];
            } else {
                $pointData = $this->findCoordsByAddress($address['city'], $address['street'], $address ['house'],
                    $address['housing'], $lang, $geocoderType);
            }
            $result[] = $pointData;
        }

        return $result;
    }

    /**
     * Метод высчитывает цену, время и дистанцию отрезка
     *
     * @param type $route
     *
     * @return string
     * */
    public function calculateRoute()
    {
        $result = new TaxiRouteInfo();

        if ($this->isDay == 1) {
            $infoCity  = [
                'planting_price'        => empty($this->tariffDataCity['planting_price_day']) ? 0 : $this->tariffDataCity['planting_price_day'],
                'planting_include'      => empty($this->tariffDataCity['planting_include_day']) ? 0 : $this->tariffDataCity['planting_include_day'],
                'next_km_price'         => empty($this->tariffDataCity['next_km_price_day']) ? 0 : $this->tariffDataCity['next_km_price_day'],
                'rounding'              => empty($this->tariffDataCity['rounding_day']) ? self::DEFAULT_ROUNDING : $this->tariffDataCity['rounding_day'],
                'next_km_price_time'    => empty($this->tariffDataCity['next_km_price_day_time']) ? 0 : $this->tariffDataCity['next_km_price_day_time'],
                'planting_include_time' => empty($this->tariffDataCity['planting_include_day_time']) ? 0 : $this->tariffDataCity['planting_include_day_time'],
                'next_cost_unit'        => empty($this->tariffDataCity['next_cost_unit_day']) ? '1_minute' : $this->tariffDataCity['next_cost_unit_day'],
                'min_price'             => empty($this->tariffDataCity['min_price_day']) ? 0 : $this->tariffDataCity['min_price_day'],
                'second_min_price'      => empty($this->tariffDataCity['second_min_price_day']) ? 0 : $this->tariffDataCity['second_min_price_day'],
            ];
            $infoTrack = [
                'planting_price'        => empty($this->tariffDataTrack['planting_price_day']) ? 0 : $this->tariffDataTrack['planting_price_day'],
                'planting_include'      => empty($this->tariffDataTrack['planting_include_day']) ? 0 : $this->tariffDataTrack['planting_include_day'],
                'next_km_price'         => empty($this->tariffDataTrack['next_km_price_day']) ? 0 : $this->tariffDataTrack['next_km_price_day'],
                'rounding'              => empty($this->tariffDataTrack['rounding_day']) ? self::DEFAULT_ROUNDING : $this->tariffDataTrack['rounding_day'],
                'next_km_price_time'    => empty($this->tariffDataTrack['next_km_price_day_time']) ? 0 : $this->tariffDataTrack['next_km_price_day_time'],
                'planting_include_time' => empty($this->tariffDataTrack['planting_include_day_time']) ? 0 : $this->tariffDataTrack['planting_include_day_time'],
                'next_cost_unit'        => empty($this->tariffDataTrack['next_cost_unit_day']) ? '1_minute' : $this->tariffDataTrack['next_cost_unit_day'],
                'min_price'             => empty($this->tariffDataTrack['min_price_day']) ? 0 : $this->tariffDataTrack['min_price_day'],
                'second_min_price'      => empty($this->tariffDataTrack['second_min_price_day']) ? 0 : $this->tariffDataTrack['second_min_price_day'],

            ];
        } else {
            $infoCity  = [
                'planting_price'        => empty($this->tariffDataCity['planting_price_night']) ? 0 : $this->tariffDataCity['planting_price_night'],
                'planting_include'      => empty($this->tariffDataCity['planting_include_night']) ? 0 : $this->tariffDataCity['planting_include_night'],
                'next_km_price'         => empty($this->tariffDataCity['next_km_price_night']) ? 0 : $this->tariffDataCity['next_km_price_night'],
                'rounding'              => empty($this->tariffDataCity['rounding_night']) ? self::DEFAULT_ROUNDING : $this->tariffDataCity['rounding_night'],
                'next_km_price_time'    => empty($this->tariffDataCity['next_km_price_night_time']) ? 0 : $this->tariffDataCity['next_km_price_night_time'],
                'planting_include_time' => empty($this->tariffDataCity['planting_include_night_time']) ? 0 : $this->tariffDataCity['planting_include_night_time'],
                'next_cost_unit'        => empty($this->tariffDataCity['next_cost_unit_night']) ? '1_minute' : $this->tariffDataCity['next_cost_unit_night'],
                'min_price'             => empty($this->tariffDataCity['min_price_night']) ? 0 : $this->tariffDataCity['min_price_night'],
                'second_min_price'      => empty($this->tariffDataCity['second_min_price_night']) ? 0 : $this->tariffDataCity['second_min_price_night'],

            ];
            $infoTrack = [
                'planting_price'        => empty($this->tariffDataTrack['planting_price_night']) ? 0 : $this->tariffDataTrack['planting_price_night'],
                'planting_include'      => empty($this->tariffDataTrack['planting_include_night']) ? 0 : $this->tariffDataTrack['planting_include_night'],
                'next_km_price'         => empty($this->tariffDataTrack['next_km_price_night']) ? 0 : $this->tariffDataTrack['next_km_price_night'],
                'rounding'              => empty($this->tariffDataTrack['rounding_night']) ? self::DEFAULT_ROUNDING : $this->tariffDataTrack['rounding_night'],
                'next_km_price_time'    => empty($this->tariffDataTrack['next_km_price_night_time']) ? 0 : $this->tariffDataTrack['next_km_price_night_time'],
                'planting_include_time' => empty($this->tariffDataTrack['planting_include_night_time']) ? 0 : $this->tariffDataTrack['planting_include_night_time'],
                'next_cost_unit'        => empty($this->tariffDataTrack['next_cost_unit_night']) ? '1_minute' : $this->tariffDataTrack['next_cost_unit_night'],
                'min_price'             => empty($this->tariffDataTrack['min_price_night']) ? 0 : $this->tariffDataTrack['min_price_night'],
                'second_min_price'      => empty($this->tariffDataTrack['second_min_price_night']) ? 0 : $this->tariffDataTrack['second_min_price_night'],

            ];
        }


        switch ($this->tariffDataTrack['accrual']) {
            case self::TARIFF_TYPE_FIX :
                $resultTrack = $this->calculateFix($infoTrack, 'out');
                break;
            case self::TARIFF_TYPE_TIME :
                $resultTrack = $this->calculateTime($infoTrack, 'out');
                break;
            case self::TARIFF_TYPE_DISTANCE :
                $resultTrack = $this->calculateDistance($infoTrack, 'out');
                break;
            case self::TARIFF_TYPE_MIX :
                $resultTrack = $this->calculateMix($infoTrack, 'out');
                break;
            case self::TARIFF_TYPE_INTERVAL :
                $resultTrack = $this->calculateInterval($infoTrack, 'out');
        }

        switch ($this->tariffDataCity['accrual']) {
            case self::TARIFF_TYPE_FIX :
                $resultCity = $this->calculateFix($infoCity);
                break;
            case self::TARIFF_TYPE_TIME :
                $resultCity = $this->calculateTime($infoCity);
                break;
            case self::TARIFF_TYPE_DISTANCE :
                $resultCity = $this->calculateDistance($infoCity);
                break;
            case self::TARIFF_TYPE_MIX :
                $resultCity = $this->calculateMix($infoCity);
                break;
            case self::TARIFF_TYPE_INTERVAL :
                $resultCity = $this->calculateInterval($infoCity);
        }

        foreach ($this->sectionLine as $route) {
            $result->summaryCost += $route->summaryCost;
        }

        $result->cityTime     = $resultCity['time'];
        $result->cityDistance = $resultCity['distance'];
        $result->cityCost     = $resultCity['cost'];

        $result->outCityTime     = $resultTrack['time'];
        $result->outCityDistance = $resultTrack['distance'];
        $result->outCityCost     = $resultTrack['cost'];

        //Одна точка
        if (empty($this->sectionLine)) {
            if ($this->startPointLocation == 'in') {
                $result->cityCost += $infoCity['planting_price'];
            } else {
                if ($this->startPointLocation == 'out') {
                    $result->outCityCost += $infoTrack['planting_price'];
                }
            }
        }

        //Учитываем коэфиценты парковок. Проценты
//        $result->cityCost *= $this->district['PERCENT'] / 100;
//        $result->outCityCost *= $this->district['PERCENT'] / 100;

        //Округляем
//        $result->cityCost    = round($result->cityCost / $infoCity['rounding']) * $infoCity['rounding'];
//        $result->outCityCost = round($result->outCityCost / $infoTrack['rounding']) * $infoTrack['rounding'];

        //Учитываем коэфиценты парковок. Валюта
//        $result->summaryCost += $this->district['MONEY'];

        //Выводим общие время, дистанцию и цену
        $result->summaryTime     = $result->cityTime + $result->outCityTime;
        $result->summaryDistance = $result->cityDistance + $result->outCityDistance;
        $result->summaryCost += $result->cityCost + $result->outCityCost;

        //Доп.информация
        $result->startPointLocation = $this->startPointLocation;
        $result->finishPointLocation = ($this->finishPointLocation !== null) ? $this->finishPointLocation : $this->startPointLocation;
        $result->additionalParking = $this->district;
        $result->isFix             = $this->getIsFix($result->cityDistance, $result->outCityDistance);

        //Проверяем на минимальную стоимость
        //Величина минимальной стоимости берется из расчетов начальной точки
        $countCoords = count((array)$this->addressCoords) - 2;
        $countCoords = ($countCoords < 0) ? 0 : $countCoords;
        if ($result->finishPointLocation == 'in') {
            $rounding = $infoCity['rounding'];
            $const_min_price = $infoCity['min_price'];
            $second_min_price = ($this->tariffDataCity['calculation_fix'] === '1') ? $infoCity['second_min_price'] : 0;
        } else {
            $rounding = $infoTrack['rounding'];
            $const_min_price = $infoTrack['min_price'];
            $second_min_price = ($this->tariffDataTrack['calculation_fix'] === '1') ? $infoTrack['second_min_price'] : 0;
        }

        if (!$this->ifCounted && !$this->getIsAccrualFix($result->cityDistance, $result->outCityDistance)) {
            $result->summaryCost *= $this->district['PERCENT'] / 100;
            $result->summaryCost += $this->district['MONEY'];

        }

        $this->test = [
            $this->ifCounted,
            $this->getIsAccrualFix($result->cityDistance, $result->outCityDistance)
        ];

        //Добавляем доп.опции
        if (is_array($this->additional)) {
            foreach ($this->additional as $add) {
                $result->additionalCost += $this->getAdditionalCostById($add, $this->tariffDataCity['tariff_id'],$this->tariffDataCity['tariff_type']);
            }
            $result->summaryCost += $result->additionalCost;
        }

        $result->summaryCost    = round($result->summaryCost / $rounding) * $rounding;

        $result->min_price = $const_min_price + $second_min_price * $countCoords;
        $result->summaryCost = $result->summaryCost < $result->min_price ? $result->min_price : $result->summaryCost;

        $result->tariffInfo = [
            'tariffType'      => $this->tariffType,
            'tariffDataCity'  => $this->tariffDataCity,
            'tariffDataTrack' => $this->tariffDataTrack,
            'isDay'           => $this->isDay,
        ];

        return $result;
    }

    /**
     * Метод определяет стоит ли зафиксировать расчет
     *
     * @param type $cityDistance
     * @param type $trackDistance
     *
     * @return boolean
     */
    private function getIsFix($cityDistance, $trackDistance)
    {
        return $this->getIsAccrualFix($cityDistance, $trackDistance) || ($this->tariffDataTrack['calculation_fix'] == 1);
    }


    private function getIsAccrualFix($cityDistance, $trackDistance)
    {
        if (count($this->addressCoords) < 2) {
            return false;
        }

        if ($this->tariffDataCity['accrual'] == self::TARIFF_TYPE_FIX) {
            $cityIsFix = true;
        } else {
            $cityIsFix = false;
        }

        if ($this->tariffDataTrack['accrual'] == self::TARIFF_TYPE_FIX) {
            $trackIsFix = true;
        } else {
            $trackIsFix = false;
        }

        // Клиент может задать маршрут от А до А. Проверяем
        // Если не попали в город
        if($cityDistance === 0 && $this->startPointLocation !== 'in') {
            $cityIsFix = true;
        }
        // Если не попали в загород
        if($trackDistance === 0 && $this->startPointLocation === 'in') {
            $trackIsFix = true;
        }

        return $cityIsFix && $trackIsFix;
    }

    /**
     * Вспомогательный метод для расчета стоимости по интервалу. Высчитывает стоимость для одного интервала
     *
     * @param type $start С какого метра считаем
     * @param type $distance Длина пути
     *
     * @return type
     */
    private function calculateOneInterval($start, $distance, $rules)
    {
        // До какого метра считаем
        $finish = $start + $distance;
        $cost   = 0;
        // Перебираем какждый отрезок интервала.
        foreach ($rules as $rule) {
            $s = $rule['interval'][0] * 1000;
            $f = isset($rule['interval'][1]) ? $rule['interval'][1] * 1000 : $finish;
            // Отрезок лежит левее
            if ($start > $f) {
                continue;
            }
            // Отрезок лежит правее
            if ($finish < $s) {
                break;
            }
            // Если отрезок подходит частично
            if ($start > $s) {
                $s = $start;
            }
            // Если отрезок подходит частично
            if ($finish < $f) {
                $f = $finish;
            }
            $cost += ($f - $s) * $rule['price'] / 1000;
        }

        return $cost;
    }

    public function calculateInterval($tariffInfo, $type = 'in')
    {

        $rules = $tariffInfo['next_km_price'] == '0' ? [] : unserialize($tariffInfo['next_km_price']);
        if ($rules == []) {
            $this->error = self::ERROR_NO_INTERVAL;
        }
        $result = [
            'time'     => 0,
            'distance' => 0,
            'cost'     => 0,
        ];

        $other_distance = 0;

        foreach ($this->sectionLine as $route) {
            $result['time'] += $type == 'in' ? $route->cityTime : $route->outCityTime;

            $cost = 0;
            if ($route->typeSection == "start" && $route->startPointLocation == $type) {
                $cost += $tariffInfo['planting_price'];
                if (isset($rules[0])) {
                    $rule = [
                        'interval' => [
                            0,
                            $rules[0]['interval'][0],
                        ],
                        'price'    => 0,
                    ];
                    array_unshift($rules, $rule);
                }
            } else {
                if (isset($rules[0])) {
                    $rules[0]['interval'][0] = 0;
                }
            }

            foreach ($route->interval as $interval) {
                if ($type == $interval['type']) {
                    $cost += $this->calculateOneInterval($result['distance'] + $other_distance, $interval['distance'],
                        $rules);
                    $result['distance'] += $interval['distance'];
                } else {
                    $other_distance += $interval['distance'];
                }
            }

            if (!is_null($route->summaryCost)) {
                continue;
            }
            // Если точки а разных районах(город, загород). и тариф для загорода фиксированный.
            if ($route->startPointlocation != $route->finishPointLocation && $this->tariffDataTrack['accrual'] == self::TARIFF_TYPE_FIX) {
                continue;
            }
            $result['cost'] += $cost;
        }

        return $result;
    }

    public function calculateFix($tariffInfo, $type = 'in')
    {
        $result = [
            'time'     => 0,
            'distance' => 0,
            'cost'     => 0,
        ];

        $fixTariffs = $this->findFixTariffs($this->tariffDataCity['tariff_id'], $this->tariffType,
            $type == 'in' ? self::TARIFF_TYPE_CITY : self::TARIFF_TYPE_TRACK);


        foreach ($this->sectionLine as $key => $route) {
            $distance = $type == 'in' ? $route->cityDistance : $route->outCityDistance;
            $result['time'] += $type == 'in' ? $route->cityTime : $route->outCityTime;
            $result['distance'] += $distance;

            // Если стоимость была расчитана ранее
            if ($route->ifCounted) {
                continue;
            }
            // Если точка стартовая то учитываем стоимость посадки
            if ($route->typeSection == "start" && $route->startPointLocation == $type) {
                $result['cost'] += $tariffInfo['planting_price'];
                $include_time = $tariffInfo['planting_include'] * 60;
            }

            $start = null;
            $end   = null;

            if ($distance != 0) {
                if ($type == 'out') {
                    $start                              = $route->interval[0]['current_parking'];
                    $end                                = $route->interval[count($route->interval) - 1]['next_parking'];
                    $this->sectionLine[$key]->ifCounted = true;
                } else {
                    foreach ($route->interval as $interval) {
                        if ($interval['type'] == 'in') {
                            $end = $interval['next_parking'];
                            if (is_null($start)) {
                                $start = $interval['current_parking'];
                            }
                        }
                    }
                }
                $cost = $this->findFixCost($start, $end, $fixTariffs);
                if (is_null($cost)) {
                    $this->error = self::ERROR_NO_EXIST_PARKING;
                }
                $result['cost'] += $cost;
            }
        }

        return $result;
    }

    public function calculateTime($tariffInfo, $type = 'in')
    {
        $result = [
            'time'     => 0,
            'distance' => 0,
            'cost'     => 0,
        ];
        $time   = 0;
        switch ($tariffInfo['next_cost_unit']) {
            case '1_hour':
                $div = 60 * 60;
                break;
            case '30_minute':
                $div = 60 * 30;
                break;
            case '1_minute':
            default:
                $div = 60;
        }
        foreach ($this->sectionLine as $route) {
            $result['time'] += $type == 'in' ? $route->cityTime : $route->outCityTime;
            $result['distance'] += $type == 'in' ? $route->cityDistance : $route->outCityDistance;

            if ($route->ifCounted) {
                continue;
            }
            $time += $type == 'in' ? $route->cityTime : $route->outCityTime;
            // Если точка стартовая то учитываем стоимость посадки

            if ($route->typeSection == "start" && $route->startPointLocation == $type) {
                $result['cost'] += $tariffInfo['planting_price'];
                $time -= $tariffInfo['planting_include'] * 60;
            }
        }

        $time = $time < 0 ? 0 : $time;

        $result['cost'] += ceil($time / $div) * $tariffInfo['next_km_price'];

        return $result;
    }

    public function calculateDistance($tariffInfo, $type = 'in')
    {
        $result   = [
            'time'     => 0,
            'distance' => 0,
            'cost'     => 0,
        ];
        $distance = 0;
        foreach ($this->sectionLine as $route) {
            $result['time'] += $type == 'in' ? $route->cityTime : $route->outCityTime;
            $result['distance'] += $type == 'in' ? $route->cityDistance : $route->outCityDistance;

            if ($route->ifCounted) {
                continue;
            }
            $distance += $type == 'in' ? $route->cityDistance : $route->outCityDistance;
            // Если точка стартовая то учитываем стоимость посадки
            if ($route->typeSection == "start" && $route->startPointLocation == $type) {
                $result['cost'] += $tariffInfo['planting_price'];
                $distance -= $tariffInfo['planting_include'] * 1000;
            }
        }
        $distance = $distance < 0 ? 0 : $distance;

        $result['cost'] += $distance * $tariffInfo['next_km_price'] / 1000;

        return $result;
    }

    // Дважды считает стоимость посадка. Исправлю позже
    public function calculateMix($tariffInfo, $type = 'in')
    {
        $distance = $this->calculateDistance($tariffInfo, $type);

        $tariffInfo['next_km_price']    = $tariffInfo['next_km_price_time'];
        $tariffInfo['planting_include'] = $tariffInfo['planting_include_time'];
        $tariffInfo['planting_price']   = 0;
        $time                           = $this->calculateTime($tariffInfo, $type);

        $result = [
            'time'     => $distance['time'],
            "distance" => $time['distance'],
            'cost'     => $time['cost'] + $distance['cost'],
        ];

        return $result;
    }

    /**
     * Получение инфы для обычных тарифов
     *
     * @param type $tariffGroupId ид тарифа
     * @param type $zone (тип тарифа CITY/ TRACK)
     *
     * @return type
     */
    public function getTariffData($tariffGroupId, $zone, $tariffType)
    {
        $tariffData = $this->_dataBaseLayer->getTariffData($tariffGroupId, $zone, $tariffType);

        return $tariffData;
    }

    /**
     * Получение всех фиксированных тарифов для данного тарифа
     *
     * @param int $tariffId ид тарифа
     *
     * @return string $tariffType тип тарифа
     */
    public function findFixTariffs($tariffId, $tariffType, $type = null)
    {
        return $this->_dataBaseLayer->findFixTariffs($tariffId, $tariffType, $type);
    }

    /**
     * Получить тип тарифа по времени
     *
     * @param string $orderTime 2014.11.12 11:35:00
     *
     * @return string $tariffType тип тарифа  ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS')
     */
    public function getTariffTypeByTime($tariffGroupId, $orderTime)
    {
        $time           = new DateTime($orderTime);
        $dateFull       = $time->format('d.m.Y');
        $dateShort      = $time->format('d.m');
        $dayOfWeek      = date('l', strtotime($orderTime));
        $hoursAndMinute = $time->format('H:i');
        $toDay          = [$dayOfWeek, $dateShort, $dateFull];
        $type           = $this->_dataBaseLayer->getTariffTypeByTime($tariffGroupId, $toDay, $hoursAndMinute);

        return $type;
    }

    /**
     * Получить спосок доп. опций для тарифа
     *
     * @param integer $tariffId
     * @param string  $date
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getAddOptions($tariffId, $date)
    {
        /** @var TaxiTariffService $tariffService */
        $tariffService    = \Yii::$app->get('taxiTariffService');
        $tariffType       = $tariffService->getTypeModel($tariffId, $date);
        $additionalModels = $tariffService->findAdditionalModels($tariffType->type_id);

        return ArrayHelper::getColumn($additionalModels, 'additional_option_id');
    }

    public function isTariffDay($tariffData, $orderTime)
    {
        $allowDayNight = $tariffData['allow_day_night'];
        $startDay      = $tariffData['start_day'];
        $endDay        = $tariffData['end_day'];

        if ($allowDayNight != 1 || empty($startDay) || empty($endDay)) {
            return 1;
        }

        $orderDate = new DateTime($orderTime);
        $orderTime = (int)($orderDate->format('H') * 60 + $orderDate->format('i'));
        $startDay  = explode(':', $startDay);
        $startTime = (int)($startDay[0] * 60 + $startDay[1]);
        $endDay    = explode(':', $endDay);
        $endTime   = (int)($endDay[0] * 60 + $endDay[1]);

        if ($startTime < $endTime) {
            if ($startTime <= $orderTime && $orderTime < $endTime) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if ($endTime <= $orderTime && $orderTime < $startTime) {
                return 0;
            } else {
                return 1;
            }
        }

    }
}