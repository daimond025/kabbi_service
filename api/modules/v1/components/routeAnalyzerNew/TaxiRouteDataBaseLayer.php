<?php

namespace app\modules\v1\components\routeAnalyzerNew;

use Yii;
use api\common\models\Parking;
use api\common\models\OptionTariff;
use api\common\models\AdditionalOption;
use api\common\models\FixHasOption;
use api\common\models\FixTariff;
use api\common\models\OptionActiveDate;
use api\common\models\CarOption;

/**
 * Description of TaxiRouteDataBaseLayer
 *  Класс для работы с Базой данный через PDO
 * @author Сергей К.
 */
class TaxiRouteDataBaseLayer
{

    /**
     * Тип тарифа (по городу)
     */
    const TARIFF_TYPE_CITY = 'CITY';

    /**
     * Тип тарифа (за городом)
     */
    const TARIFF_TYPE_TRACK = 'TRACK';

    /**
     * Тип тарифа аэропорт загородний
     */
    const TARIFF_TYPE_AIRPORT_OUT = 'AIRPORT';

    /**
     * Тип тарифа вокзал
     */
    const TARIFF_TYPE_RAILWAY = "RAILWAY";
    
    /**
     * Тип тарифа фиксированный
     */
    const TARIFF_TYPE_FIX = "FIX";
    
    
    
    const CURRENT_TYPE = 'CURRENT';
    
    const HOLIDAYS_TYPE = 'HOLIDAYS';
    
    const EXCEPTIONS_TYPE = 'EXCEPTIONS';
    
    
    /**
     * ID арендатора
     * @var integer;
     */
    private $_tenantId;

    /**
     * ID города в котором совершается заказ
     * @var integer;
     */
    private $_cityId;

    public function setTenantId($tenantId)
    {
        $this->_tenantId = $tenantId;
    }

    public function getTenantId()
    {
        return $this->_tenantId;
    }

    public function setCityId($cityId)
    {
        $this->_cityId = $cityId;
    }

    public function getCityId()
    {
        return $this->_cityId;
    }

    /**
     * Получиь базовый полигон(город) для города
     * @return array
     */
    public function getBasePolygon()
    {
        $polygon = Parking::findBySql("SELECT polygon from " . Parking::tableName() . " where type='basePolygon' and tenant_id=:tenantId and city_id=:cityId", [':tenantId' => $this->getTenantId(), ':cityId' => $this->getCityId()])->asArray()->one();
        return $polygon['polygon'];
    }

    /**
     * Получить Парковки в городе
     * @return array
     */
    public function getParkings()
    {
        $parking = Parking::find()
            ->select(['parking_id', 'type', 'polygon'])
            ->where([
                'tenant_id' => $this->getTenantId(),
                'city_id' => $this->getCityId(),
            ])
            ->andWhere(['not', ['type' => 'reseptionArea']])
            ->asArray()
            ->all();

        return $parking;
    }

    /**
     * Получить тип парковки по Id
     * @param type $parkingId
     * @return type
     */
    public function getParkingTypeByid($parkingId)
    {
        return Parking::findBySql("SELECT type from " . Parking::tableName() . " where parking_id = :parkingId", [':parkingId' => $parkingId])->asArray()->one();
    }

    /**
     * Получение инфы по обычный тарифам
     * @param string $tariffGroupId
     * @param string $area
     * @return array
     */
    public function getTariffData($tariffGroupId, $area, $tariffType)
    {
        return OptionTariff::findBySql("SELECT * from " . OptionTariff::tableName() . " where tariff_id= :tariffGroupId and area = :area and tariff_type= :tariffType ", [':tariffGroupId' => $tariffGroupId, ':area' => $area, ':tariffType' => $tariffType])->asArray()->one();
    }

    /**
     * Получение инфы по фиксированным тарифам
     * @param string $tariffId
     * @return array
     */
    public function findFixTariffs($tariffId, $tariffType,$type = null)
    {
        $types = is_null($type) ? [self::TARIFF_TYPE_CITY,self::TARIFF_TYPE_TRACK] : [$type];
        //$option_ids = OptionTariff::findBySql("SELECT option_id from " . OptionTariff::tableName() . " where tariff_id= :tariffId and accrual = 'FIX' and tariff_type= :tariffType ", [':tariffId' => $tariffId, ':tariffType' => $tariffType])->asArray()->all();
        $option_ids = OptionTariff::find()
                ->select(['option_id'])
                ->where('tariff_id=:tariff_id',['tariff_id'=>$tariffId])
                ->andWhere('accrual=:fix',['fix'=>self::TARIFF_TYPE_FIX])
                ->andWhere('tariff_type=:tariff_type',['tariff_type'=>$tariffType])
                ->andWhere(['area' => $types])
                ->asArray()
                ->all();
        if (!empty($option_ids) && is_array($option_ids)) {
            $fixTariffs = array();
            foreach ($option_ids as $item) {
                $option_id = $item['option_id'];
                //$fix_ids = FixHasOption::findBySql("SELECT fix_id,option_id from " . FixHasOption::tableName() . " where option_id = :optionId", [':optionId' => $optionId])->asArray()->all();
                $fix_ids = FixHasOption::find()
//                        ->select(['fix_id','option_id'])
                        ->where('option_id=:option_id',['option_id'=>$option_id])
                        ->asArray()
                        ->all();
                if (!empty($fix_ids) && is_array($fix_ids)) {
                    $fixTariffs = array_merge($fixTariffs, $fix_ids);
                }
            }
            if (!empty($fixTariffs)) {
                $resultTarifffs = array();
                foreach ($fixTariffs as $item) {
                    $fix_id = $item['fix_id'];
//                    $data = FixTariff::findBySql("SELECT * from " . FixTariff::tableName() . " where fix_id = :id", [':id' => $id])->asArray()->all();
                    $data = FixTariff::find()
                            ->where('fix_id=:fix_id',['fix_id' => $fix_id])
                            ->asArray()
                            ->all();
                    $data[0]["option_id"] = $item["option_id"];
                    $data[0]['price'] =$item['price_to'];
                    if (!empty($data) && is_array($data)) {
                        $resultTarifffs = array_merge($resultTarifffs, $data);
                    }
                }
            }
        }
        return isset($resultTarifffs) ? $resultTarifffs : null;
    }
   
    public function getFixTarrifInfo($fixId, $optionId)
    {
//        $data = FixHasOption::findBySql("SELECT * from " . FixHasOption::tableName() . " where fix_id =:fixId and option_id =:optionId", [':fixId' => $fixId, ':optionId' => $optionId])->asArray()->one();
        return FixHasOption::find()
                ->where('fix_id=:fix_id',['fix_id'=>$fixId])
                ->andWhere('option_id=:option_id',['option_id'=>$optionId])
                ->asArray()
                ->one();
    }
    
    
    /**
     * Получение стоимости парковок
     * @param integer $district1 Начальная точка
     * @param integer $district2 Конечная точка
     * @return string
    **/
    public function getAdditionalParking($district1, $district2)
    {

        $district_out = Parking::findBySql("SELECT out_district,out_distr_coefficient_type from " . Parking::tableName() . " where parking_id = :parkingId", [':parkingId' => $district1])->one();
        $district_in = Parking::findBySql("SELECT in_district,in_distr_coefficient_type from " . Parking::tableName() . " where parking_id = :parkingId", [':parkingId' => $district2])->one();
        
        $array = array(
            'MONEY' => 0,
            'PERCENT' => 100,
        );
        if($district_out && !empty($district_out->out_distr_coefficient_type)) {
            $array[$district_out->out_distr_coefficient_type] += (int)$district_out->out_district;
        }
        if($district_in && !empty($district_in->in_distr_coefficient_type)) {
            $array[$district_in->in_distr_coefficient_type ] += (int)$district_in->in_district;
        }

        return $array;
    }

    /**
     * Получить добавочную стоимость для доп. опций
     * @param string $id ид доп опции
     * @param string $tariffId ид тарифа
     * @return string цена
     */
    public function getAdditionalCost($id, $tariffId, $tariffType)
    {
//        $price = AdditionalOption::findBySql("SELECT price from " . AdditionalOption::tableName() . " where additional_option_id= :id and tariff_id= :tariffId", [':id' => $id, ':tariffId' => $tariffId])->asArray()->one();

        $price = AdditionalOption::find()
            ->select('price')
            ->where([
                'additional_option_id' => $id,
                'tariff_id' => $tariffId,
                'tariff_type' => $tariffType,
            ])
            ->limit(1)
            ->scalar();
        return (int)$price;
        if (!empty($price) && is_array($price)) {
            return $price['price'];
        }
    }
    
    /**
     * Получить список всех допиков для тарифа
     * @param type $triff_id
     * @param type $type
     */
    public function addOptionList($tariff, $type)
    {
        $add_options = AdditionalOption::find()
                ->select(['additional_option_id'])
                ->where('tariff_id=:tariff_id',['tariff_id' => $tariff])
                ->andWhere('tariff_type=:tariff_type',['tariff_type' => $type])
                ->asArray()
                ->all();
        return array_map(function($item) {
            return $item['additional_option_id'];
        },$add_options);
        return $add_options;
    }

    /**
     * Получить тип тарифа ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS') по его Id и Текущему дню
     * Приоритеты: EXCEPTIONS -> HOLIDAYS -> CURRENT
     * @param int $tariffGroupId
     * @param array $toDay array('Monday','12.11.2014','12.11.')
     * @param string $hoursAndMinute '08:00'
     * @return string tariffType 'CURRENT'/'HOLIDAYS'/'EXCEPTIONS'
     */
    public function getTariffTypeByTime($tariffGroupId, $toDay, $hoursAndMinute)
    {
        $result = self::CURRENT_TYPE;
        
        $records = OptionActiveDate::find()
                ->where('tariff_id = :tariff_id', [':tariff_id' => $tariffGroupId])
                ->asArray()
                ->all();
        if (!empty($records)) {
            
            foreach ($records as $record) {
                
                $date = explode('|',$record['active_date']);

                // Игнорируем если день не совпал
                if(!in_array($date[0],$toDay)){
                    continue;
                }
                $interval = isset($date[1]) ? explode('-',$date[1]) : ['00:00','23:59'] ;

                // Игнорируем если не попали в часовой промежуток
                if($interval[0] > $hoursAndMinute || $interval[1] < $hoursAndMinute){
                    continue;
                }
                
                switch($record['tariff_type']) {
                    case self::EXCEPTIONS_TYPE :
                        if (self::checkTariffIsActive($tariffGroupId, self::EXCEPTIONS_TYPE)) {
                            return self::EXCEPTIONS_TYPE;
                        }
                        break;
                    case self::HOLIDAYS_TYPE :
                        if (self::checkTariffIsActive($tariffGroupId, self::HOLIDAYS_TYPE)) {
                            $result = self::HOLIDAYS_TYPE;
                        }
                        break;
                }
            }
        }
        return $result;
    }

    public static function checkTariffIsActive($tariffId, $tariffType)
    {
        $isActive = OptionTariff::find()
                ->where('tariff_id= :tariff_id',['tariff_id' => $tariffId])
                ->andWhere('tariff_type= :tariff_type',['tariff_type' => $tariffType])
                ->andWhere('active = 1 OR active IS NULL')
                ->asArray()
                ->one();
        if (!empty($isActive)) {
            return true;
        }
        return false;
    }

    public function getParkingArrAll($tenantId, $cityId)
    {
        return Parking::getParkingArrAll($tenantId, $cityId);
    }
    
    

}
