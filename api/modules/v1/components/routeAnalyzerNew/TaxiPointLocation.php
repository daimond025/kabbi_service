<?php

namespace app\modules\v1\components\routeAnalyzerNew;

/**
 * Description of TaxiPointLocation
 * Класс для работы с местоположением точек относительно полигонов
 * @author Сергей К.
 */
class TaxiPointLocation
{

    const TYPE_INSIDE = 'inside';
    const TYPE_OUTSIDE = 'outside';

    var $pointOnVertex = true;

    function pointLocation()
    {

    }

    /**
     *
     * @param type $point
     * @param type $point2
     * @return boolean
     */
    function pointInPoint($point, $point2)
    {

        $point = $this->pointStringToCoordinates($point);
        $point2 = $this->pointStringToCoordinates($point2[0]);
        $pointX = round($point[0], 2);
        $pointY = round($point[1], 2);
        $point2X = round($point2[0], 2);
        $point2Y = round($point2[1], 2);
        if (($pointX == $point2X ) && ( $pointY == $point2Y )) {
            return true;
        }
        return false;
    }

    /**
     * Метод определяет положение точки в полигоне
     * @param array $point
     * $point = [
     *      53.252449035645,
     *      53.252449035645
     * ]
     * @param array $polygon
     * @param boolean $pointOnVertex
     * @return string
     */
    function pointInPolygon($point, $polygon, $pointOnVertex = true)
    {

        $this->pointOnVertex = $pointOnVertex;

        $intersections = 0;
        $vertices_count = count($polygon);
        for ($i = 1; $i < $vertices_count; $i++) {
            $vertex1 = $polygon[$i - 1];
            $vertex2 = $polygon[$i];

            if ($point['1'] > min($vertex1['1'], $vertex2['1']) and $point['1'] <= max($vertex1['1'], $vertex2['1']) and $point['0'] <= max($vertex1['0'], $vertex2['0']) and $vertex1['1'] != $vertex2['1']) {
                $xinters = ($point['1'] - $vertex1['1']) * ($vertex2['0'] - $vertex1['0']) / ($vertex2['1'] - $vertex1['1']) + $vertex1['0'];

                if ($vertex1['0'] == $vertex2['0'] || $point['0'] <= $xinters) {
                    $intersections++;
                }
            }
        }
        if ($intersections % 2 != 0) {
            return self::TYPE_INSIDE;
        } else {
            return self::TYPE_OUTSIDE;
        }
    }

    /**
     *
     * @param type $point
     * @param type $vertices
     * @return boolean
     */
    function pointOnVertex($point, $vertices)
    {
        foreach ($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
    }

    /**
     * Метод превращает точку из строки в массив координат
     * @param string $pointString
     * @return array
     */
    function pointStringToCoordinates($pointString)
    {
        $coordinates = explode(" ", $pointString);
        return array("0" => $coordinates[0], "1" => $coordinates[1]);
    }

    /**
     * Метод превращает точку из массива в строку
     * @param array $point
     * @return string
     */
    function pointCoordsToString($point)
    {
        return $point['0'] . " " . $point['1'];
    }

}
