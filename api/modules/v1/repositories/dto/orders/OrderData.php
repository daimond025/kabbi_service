<?php

namespace app\modules\v1\repositories\dto\orders;

class OrderData
{
    public $id;
    public $number;
    public $cityId;
    public $address;
    public $positionId;
    public $worker;

    public function __construct($id, $number, $cityId, array $address, $positionId, OrderWorkerData $worker)
    {
        $this->id         = $id;
        $this->number     = $number;
        $this->cityId     = $cityId;
        $this->address    = $address;
        $this->positionId = $positionId;
        $this->worker     = $worker;
    }
}

