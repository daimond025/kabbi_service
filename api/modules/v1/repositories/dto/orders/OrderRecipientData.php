<?php

namespace app\modules\v1\repositories\dto\orders;

class OrderRecipientData
{
    public $phone;
    public $code;

    public function __construct($phone, $code)
    {
        $this->phone = $phone;
        $this->code  = $code;
    }
}
