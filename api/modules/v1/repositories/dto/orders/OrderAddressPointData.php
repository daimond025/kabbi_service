<?php

namespace app\modules\v1\repositories\dto\orders;

class OrderAddressPointData
{
    public $lat;
    public $lon;
    public $recipient;

    public function __construct($lat, $lon, OrderRecipientData $recipientData)
    {
        $this->lat       = $lat;
        $this->lon       = $lon;
        $this->recipient = $recipientData;
    }
}
