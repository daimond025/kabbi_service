<?php

namespace app\modules\v1\repositories\dto\orders;

class OrderWorkerData
{
    public $lastName;
    public $name;
    public $secondName;
    public $phone;

    public function __construct($lastName, $name, $secondName, $phone)
    {
        $this->lastName   = $lastName;
        $this->name       = $name;
        $this->secondName = $secondName;
        $this->phone      = $phone;
    }
}
