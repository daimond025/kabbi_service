<?php

namespace app\modules\v1\repositories\dto;

class RecipientTemplateData
{
    public $type;
    public $text;
    public $params = [];
    public $positionId;
    public $tenantId;
    public $cityId;

    public function __construct($type, $text, array $params, $positionId, $tenantId, $cityId)
    {
        $this->type       = $type;
        $this->text       = $text;
        $this->params     = $params;
        $this->positionId = $positionId;
        $this->tenantId   = $tenantId;
        $this->cityId     = $cityId;
    }
}
