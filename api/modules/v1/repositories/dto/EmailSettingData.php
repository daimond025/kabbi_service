<?php

namespace app\modules\v1\repositories\dto;

class EmailSettingData
{
    public $provider;
    public $sender;
    public $template;

    public function __construct(EmailProviderData $provider, EmailSenderData $sender, $template)
    {
        $this->provider = $provider;
        $this->sender   = $sender;
        $this->template = $template;
    }
}