<?php

namespace app\modules\v1\repositories;

use api\modules\v1\models\recipientTemplate\DefaultRecipientSmsTemplate;
use api\modules\v1\models\recipientTemplate\RecipientSmsTemplate;
use app\modules\v1\repositories\dto\RecipientTemplateData;

class RecipientRepository
{
    /**
     * @param $tenantId
     * @param $cityId
     * @param $positionId
     * @param $type
     *
     * @return RecipientTemplateData
     */
    public function getTemplate($tenantId, $cityId, $positionId, $type)
    {
        $defaultTemplate = $this->getDefaultTemplate($positionId, $type);

        /** @var RecipientSmsTemplate $template */
        $template = RecipientSmsTemplate::find()->where([
            'tenant_id'   => $tenantId,
            'city_id'     => $cityId,
            'position_id' => $positionId,
            'type'        => $type,
        ])->one();

        if (!$template) {
            throw new NotFoundException();
        }

        return new RecipientTemplateData(
            $type,
            $template->text,
            $defaultTemplate->params,
            $positionId,
            $tenantId,
            $template->city_id
        );
    }

    /**
     * @param $positionId
     * @param $type
     *
     * @return array|null|\yii\db\ActiveRecord|DefaultRecipientSmsTemplate
     */
    private function getDefaultTemplate($positionId, $type)
    {
        $template = DefaultRecipientSmsTemplate::find()->where(['type' => $type, 'position_id' => $positionId])->one();

        if (!$template) {
            throw new NotFoundException();
        }

        return $template;
    }
}
