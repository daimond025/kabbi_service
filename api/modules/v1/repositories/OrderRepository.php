<?php

namespace app\modules\v1\repositories;

use api\common\models\Order;
use app\modules\v1\repositories\dto\orders\OrderAddressPointData;
use app\modules\v1\repositories\dto\orders\OrderData;
use app\modules\v1\repositories\dto\orders\OrderRecipientData;
use app\modules\v1\repositories\dto\orders\OrderWorkerData;
use yii\helpers\ArrayHelper;

class OrderRepository
{
    private $orders = [];

    /**
     * @param $tenantId
     * @param $orderId
     *
     * @return OrderData
     * @throws \yii\base\InvalidConfigException
     */
    public function getOrder($tenantId, $orderId)
    {
        try {
            return $this->getOrderInRedis($tenantId, $orderId);
        } catch (NotFoundException $ignore) {
        }

        return $this->getOrderInMysql($tenantId, $orderId);
    }

    /**
     * @param $tenantId
     * @param $orderId
     *
     * @return OrderData
     * @throws \yii\base\InvalidConfigException
     */
    private function getOrderInRedis($tenantId, $orderId)
    {
        if (!isset($this->orders[$tenantId][$orderId])) {
            $order = \Yii::$app->get('redis_orders_active')->executeCommand('HGET', [$tenantId, $orderId]);
            $order = unserialize($order);

            if (!$order) {
                throw new NotFoundException();
            }

            $address     = unserialize(ArrayHelper::getValue($order, 'address'));
            $addressData = [];
            if (ArrayHelper::isTraversable($address)) {
                foreach ($address as $point) {
                    $addressData[] = new OrderAddressPointData(
                        ArrayHelper::getValue($point, 'lat'),
                        ArrayHelper::getValue($point, 'lon'),
                        new OrderRecipientData(
                            ArrayHelper::getValue($point, 'phone'),
                            ArrayHelper::getValue($point, 'confirmation_code')
                        )
                    );
                }
            }

            $this->orders[$tenantId][$orderId] = new OrderData(
                ArrayHelper::getValue($order, 'order_id'),
                ArrayHelper::getValue($order, 'order_number'),
                ArrayHelper::getValue($order, 'city_id'),
                $addressData,
                ArrayHelper::getValue($order, 'position_id'),
                new OrderWorkerData(
                    ArrayHelper::getValue($order, 'worker.last_name'),
                    ArrayHelper::getValue($order, 'worker.name'),
                    ArrayHelper::getValue($order, 'worker.second_name'),
                    ArrayHelper::getValue($order, 'worker.phone')
                )
            );

        }

        return $this->orders[$tenantId][$orderId];
    }

    /**
     * @param $tenantId
     * @param $orderId
     *
     * @return OrderData
     */
    private function getOrderInMysql($tenantId, $orderId)
    {
        if (!isset($this->orders[$tenantId][$orderId])) {
            $order = Order::find()
                ->alias('order')
                ->where(['order.tenant_id' => $tenantId, 'order.order_id' => $orderId])
                ->joinWith('worker')
                ->asArray()
                ->one();

            if (!$order) {
                throw new NotFoundException();
            }

            $address     = unserialize(ArrayHelper::getValue($order, 'address'));
            $addressData = [];
            if (ArrayHelper::isTraversable($address)) {
                foreach ($address as $point) {
                    $addressData[] = new OrderAddressPointData(
                        ArrayHelper::getValue($point, 'lat'),
                        ArrayHelper::getValue($point, 'lon'),
                        new OrderRecipientData(
                            ArrayHelper::getValue($point, 'phone'),
                            ArrayHelper::getValue($point, 'confirmation_code')
                        )
                    );
                }
            }

            $this->orders[$tenantId][$orderId] = new OrderData(
                ArrayHelper::getValue($order, 'order_id'),
                ArrayHelper::getValue($order, 'order_number'),
                ArrayHelper::getValue($order, 'city_id'),
                $addressData,
                ArrayHelper::getValue($order, 'position_id'),
                new OrderWorkerData(
                    ArrayHelper::getValue($order, 'worker.last_name'),
                    ArrayHelper::getValue($order, 'worker.name'),
                    ArrayHelper::getValue($order, 'worker.second_name'),
                    ArrayHelper::getValue($order, 'worker.phone')
                )
            );
        }

        return $this->orders[$tenantId][$orderId];
    }
}
