<?php

namespace app\modules\v1\repositories;

use api\common\models\Tenant;
use yii\helpers\ArrayHelper;

class TenantRepository
{
    private $tenantDomains = [];

    public function getTenantDomain($tenantId)
    {
        if (!ArrayHelper::keyExists($tenantId, $this->tenantDomains)){
            $model                          = $this->find($tenantId);
            $this->tenantDomains[$tenantId] = $model->domain;
        }

        return ArrayHelper::getValue($this->tenantDomains, $tenantId);
    }

    public function find($tenantId)
    {
        if (!$result = Tenant::findOne($tenantId)) {
            throw new \RuntimeException('Find model');
        }

        return $result;
    }
}