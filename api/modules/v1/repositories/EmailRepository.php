<?php

namespace app\modules\v1\repositories;

use api\modules\v1\models\email\DefaultEmailSetting;
use api\modules\v1\models\email\EmailTypes;
use api\modules\v1\models\email\TenantEmailSetting;
use app\modules\v1\repositories\dto\EmailProviderData;
use app\modules\v1\repositories\dto\EmailSenderData;
use app\modules\v1\repositories\dto\EmailSettingData;

class EmailRepository
{
    /**
     * @param $tenantId
     * @param $cityId
     *
     * @return EmailSettingData
     */
    public function findTenantSetting($tenantId, $cityId)
    {
        if (!$model = TenantEmailSetting::findOne(['tenant_id' => $tenantId, 'city_id' => $cityId, 'active' => 1])) {
            throw new NotFoundException();
        }

        return new EmailSettingData(
            new EmailProviderData($model->provider_server, $model->provider_port),
            new EmailSenderData($model->sender_name, $model->sender_email, $model->sender_password),
            $model->template
        );
    }

    /**
     * @return EmailSettingData
     */
    public function findDefaultSettingFromSystem()
    {
        /** @var DefaultEmailSetting $model */
        if (!$model = DefaultEmailSetting::findOne(['type' => DefaultEmailSetting::TYPE_SYSTEM])) {
            throw new NotFoundException();
        }

        return new EmailSettingData(
            new EmailProviderData($model->provider_server, $model->provider_port),
            new EmailSenderData($model->sender_name, $model->sender_email, $model->sender_password),
            $model->template
        );
    }
    /**
     * @return EmailSettingData
     */
    public function findDefaultSettingFromTenant()
    {
        /** @var DefaultEmailSetting $model */
        if (!$model = DefaultEmailSetting::findOne(['type' => DefaultEmailSetting::TYPE_TENANT])) {
            throw new NotFoundException();
        }

        return new EmailSettingData(
            new EmailProviderData($model->provider_server, $model->provider_port),
            new EmailSenderData($model->sender_name, $model->sender_email, $model->sender_password),
            $model->template
        );
    }

}