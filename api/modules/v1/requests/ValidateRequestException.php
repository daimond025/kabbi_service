<?php

namespace api\modules\v1\requests;

use api\modules\v1\exceptions\ErrorsException;

class ValidateRequestException extends ErrorsException
{
    public function __construct($errors)
    {
        parent::__construct($errors, 422);
    }

    public function getName()
    {
        return 'Validation Error';
    }
}