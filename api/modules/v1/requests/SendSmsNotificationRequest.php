<?php

namespace api\modules\v1\requests;

use api\common\models\Order;
use api\common\models\Tenant;
use yii\base\Model;

class SendSmsNotificationRequest extends Model
{
    public $tenant_id;
    public $order_id;
    public $status_id;

    public function rules()
    {
        return [
            ['tenant_id', 'required'],
            ['tenant_id', 'exist', 'targetClass' => Tenant::class],

            ['order_id', 'required'],
            ['order_id', 'exist', 'targetClass' => Order::class, 'targetAttribute' => ['order_id', 'tenant_id']],

            ['status_id', 'required'],
            ['status_id', 'string', 'max' => 255],
        ];
    }


    public function getErrorsMap()
    {
        $result = [];
        $errors = $this->errors;

        foreach ($errors as $errorName => $attributeErrors) {
            foreach ($attributeErrors as $error) {
                $result[] = [
                    'message'   => $error,
                    'attribute' => $errorName,
                ];
            }
        }

        return $result;
    }
}
