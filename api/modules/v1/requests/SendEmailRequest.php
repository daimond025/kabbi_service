<?php

namespace api\modules\v1\requests;

use api\common\models\Tenant;
use api\modules\v1\models\email\EmailTypes;
use app\models\tenant\TenantHasCity;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\validators\StringValidator;

class SendEmailRequest extends Model
{
    public $tenant_id;
    public $city_id;
    public $type;
    public $to;
    public $lang;
    public $params = [];

    public function rules()
    {
        return [
            ['tenant_id', 'string'],
            ['tenant_id', 'exist', 'targetClass' => Tenant::className()],
            
            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['city_id', 'tenant_id'],
            ],

            ['type', 'required'],
            ['type', 'in', 'range' => EmailTypes::getTypeList()],

            ['lang', 'string', 'max' => 10],

            ['to', 'required'],
            ['to', 'string'],
            ['to', 'filter', 'filter' => [$this, 'filterTo']],

            ['params', 'validateParams'],
            ['params', 'filter', 'filter' => [$this, 'filterParams']],
        ];
    }

    public function filterTo($value)
    {
        if (empty($value)) {
            return [];
        }

        return explode(',', $value);
    }

    public function filterParams($values)
    {
        if (!ArrayHelper::isTraversable($values)) {
            $values = [];
        }

        $typeParams = EmailTypes::getParamsByType($this->type);
        $params     = [];

        if (ArrayHelper::isTraversable($typeParams)) {
            foreach ($typeParams as $param) {
                $params[$param] = ArrayHelper::getValue($values, $param, '');
            }
        }

        return $params;
    }

    public function validateParams($attribute)
    {
        $attr = $this->$attribute;
        if (!ArrayHelper::isTraversable($attr)) {
            $this->addError($attribute, "$attribute должен быть массивом");

            return;
        }

        $stringValidator = new StringValidator();
        foreach ($attr as $paramName => $paramValue) {
            if (!$stringValidator->validate($paramValue)) {
                $this->addError($attribute, "$paramName must be a string.");
            }
        }
    }

    public function getErrorsMap()
    {
        $result = [];
        $errors = $this->errors;

        foreach ($errors as $errorName => $attributeErrors) {
            foreach ($attributeErrors as $error) {
                if ($errorName === 'params') {
                    $parseError = explode(' ', $error, 2);
                    $result[]   = [
                        'message'   => $this->generateAttributeLabel(ArrayHelper::getValue($parseError, 0)) . ' '
                            . ArrayHelper::getValue($parseError, 1),
                        'attribute' => ArrayHelper::getValue($parseError, 0),
                        'type'      => 'post',
                    ];
                } else {
                    $result[] = [
                        'message'   => $error,
                        'attribute' => $errorName,
                        'type'      => 'get',
                    ];
                }
            }
        }

        return $result;
    }
}