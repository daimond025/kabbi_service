<?php

namespace app\modules\v1\requests;

use api\common\models\Client;
use api\common\models\Tenant;
use app\models\client_tariff\TaxiTariff;
use app\models\tenant\TenantHasCity;
use yii\base\Model;

class AnalyzeRouteRequest extends Model
{
    public $tenant_id;
    public $city_id;
    public $address_array;
    public $tariff_id;
    public $order_time;
    public $lang;
    public $geocoder_type;
    public $client_id;
    public $additional;

    public function rules()
    {
        return [
            ['tenant_id', 'required'],
            ['tenant_id', 'exist', 'targetClass' => Tenant::className()],

            ['city_id', 'required'],
            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['tenant_id', 'city_id'],
            ],

            ['address_array', 'filter', 'filter' => [$this, 'filterAddressArray']],
            ['address_array', 'required'],

            ['tariff_id', 'required'],
            ['tariff_id', 'exist', 'targetClass' => TaxiTariff::className()],

            ['order_time', 'required'],
            ['order_time', 'filter', 'filter' => [$this, 'filterOrderTime']],

            [
                'client_id',
                'exist',
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id', 'tenant_id'],
            ],

            ['additional', 'filter', 'filter' => [$this, 'filterAdditional']],

            [['lang', 'geocoder_type'], 'default', 'value' => 'ru'],
        ];
    }

    public function filterAddressArray($value)
    {
        return array_values(json_decode(urldecode($value), true));
    }

    public function filterOrderTime($value)
    {
        return date("d.m.Y H:i", strtotime($value));
    }

    public function filterAdditional($value)
    {
        if (empty($value)) {
            return [];
        }

        $value = array_map(function ($item) {
            return trim($item);
        }, explode(',', $value));

        return array_unique(array_diff($value, ['']));
    }
}