<?php

namespace app\modules\v1\helpers;

class NotificationHelper
{
    const PARAM_ADDRESS = '#ADDRESS#';
    const PARAM_DATE = '#DATE#';
    const PARAM_TIME = '#TIME#';
    const PARAM_TARIFF = '#TARIFF#';
    const PARAM_PRICE = '#PRICE#';
    const PARAM_CURRENCY = '#CURRENCY#';
    const PARAM_WORKER_LAST_NAME = '#LAST_NAME#';
    const PARAM_WORKER_NAME = '#NAME#';
    const PARAM_WORKER_SECOND_NAME = '#SECOND_NAME#';
    const PARAM_WORKER_PHONE = '#PHONE#';
    const PARAM_CODE = '#CODE#';
}
