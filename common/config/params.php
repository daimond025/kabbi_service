<?php

return [
    'adminEmail'                    => 'api_service@gootax.pro',
    'supportEmail'                  => 'api_service@gootax.pro',
    'user.passwordResetTokenExpire' => 3600,
    'nodeApiUrl'                    => "http://192.168.81.16:8087"
];
